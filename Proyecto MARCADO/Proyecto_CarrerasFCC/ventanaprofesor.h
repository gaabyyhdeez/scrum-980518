#ifndef VENTANAPROFESOR_H
#define VENTANAPROFESOR_H

#include <QDialog>
#include "globals.h"

namespace Ui {
class ventanaProfesor;
}

class ventanaProfesor : public QDialog
{
    Q_OBJECT

public:
    explicit ventanaProfesor(QWidget *parent = 0, int idTutor=0);
    ~ventanaProfesor();

private slots:
    void on_Buscar_clicked();

    void on_lineNombre_textEdited(const QString &arg1);

    void on_pushButton_clicked();

private:
    Ui::ventanaProfesor *ui;

    //Mostrar tabla
    QSqlTableModel *miModeloTabla;

    //Autocompletado
    QCompleter * StringCompleterSeccion;

    int idTutor2=0;
};

#endif // VENTANAPROFESOR_H
