#include "ventanaalumnoicc.h"
#include "ui_ventanaalumnoicc.h"
#include <QMessageBox>

ventanaAlumnoICC::ventanaAlumnoICC(QWidget *parent, int matricula) :
    QDialog(parent),
    ui(new Ui::ventanaAlumnoICC)
{   
    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose,true);

    matricula2 = matricula;
    qDebug() << matricula2;

    QSqlQuery datosAlumno;
    datosAlumno.prepare("SELECT matricula, nombre FROM Alumno WHERE matricula="+QString::number(matricula2)+"");
    datosAlumno.exec();
    datosAlumno.next();

    qDebug() << datosAlumno.result();
    ui->label_NombreMatri->setText(datosAlumno.value(0).toString());
    ui->label_NombreAlm->setText(datosAlumno.value(1).toString());
}

ventanaAlumnoICC::~ventanaAlumnoICC()
{
    delete ui;
}


void ventanaAlumnoICC::on_buttRegresar_clicked()
{

}
