#ifndef VENTANAALUMNOLCC_H
#define VENTANAALUMNOLCC_H

#include <QDialog>
#include "globals.h"

namespace Ui {
class ventanaAlumnoLCC;
}

class ventanaAlumnoLCC : public QDialog
{
    Q_OBJECT

public:
    explicit ventanaAlumnoLCC(QWidget *parent = 0, int matricula = 0);
    ~ventanaAlumnoLCC();

private slots:


private:
    Ui::ventanaAlumnoLCC *ui;
    int matricula2;
};

#endif // VENTANAALUMNOLCC_H
