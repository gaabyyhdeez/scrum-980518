--------------------------------------------------------------
/*					CREACI�N DE LA BASE DE DATOS			*/
--------------------------------------------------------------

USE master
GO

CREATE DATABASE MapaCurricular
GO

DROP DATABASE MapaCurricular
GO

---------------------------------------------------------------
/*					CREACI�N DE LAS TABLAS					 */
---------------------------------------------------------------

USE MapaCurricular
GO

CREATE TABLE Tutor(idTutor nvarchar(20) not null,
nombre nvarchar(65) not null,
contrasegna nvarchar(45) not null,
verificado int not null,
primary key(idTutor)
)
GO

CREATE TABLE Seccion(idSeccion int not null,
nombre nvarchar(15) not null,
agno int not null,
idTutor nvarchar(20),
foreign key(idTutor) references Tutor(idTutor),
primary key(idSeccion)
)
GO

CREATE TABLE Carrera(idCarrera int not null,
nombreCarrera nvarchar(45) not null,
codigo nvarchar(15) not null,
primary key(idCarrera)
)
GO

CREATE TABLE Alumno(matricula int not null,
nombre nvarchar(60) not null,
contrasegna nvarchar(25) not null,
verificado int not null,
idCarrera int not null,
idSeccion int not null,
foreign key(idCarrera) references Carrera(idCarrera),
foreign key(idSeccion) references Seccion(idSeccion),
primary key(matricula)
)
GO

CREATE TABLE Area(idArea int not null,
nombreArea nvarchar(45) not null,
primary key(idArea)
)
GO

CREATE TABLE Nivel(idNivel int not null,
nombreNivel nvarchar(45) not null,
primary key(idNivel)
)
GO

CREATE TABLE Materia(idMateria int not null,
nombreMateria nvarchar(65) not null,
creditos int not null,
numSemestre int not null,
idCarrera int not null,
idArea int,
idNivel int not null,
foreign key(idCarrera) references Carrera(idCarrera),
foreign key(idArea) references Area(idArea),
foreign key(idNivel) references Nivel(idNivel),
primary key(idMateria)
)
GO

CREATE TABLE Avance(matricula int not null,
idMateria int not null,
foreign key(matricula) references Alumno(matricula),
foreign key(idMateria) references Materia(idMateria),
)
GO

CREATE TABLE Requisitos(prerequisito int not null,
subsecuente int not null,
foreign key(prerequisito) references Materia(idMateria),
foreign key(subsecuente) references Materia(idMateria),
)
GO