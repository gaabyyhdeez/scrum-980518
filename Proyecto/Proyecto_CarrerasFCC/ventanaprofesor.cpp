#include "ventanaprofesor.h"
#include "ui_ventanaprofesor.h"

ventanaProfesor::ventanaProfesor(QWidget *parent, int idTutor) :
    QDialog(parent),
    ui(new Ui::ventanaProfesor)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose,true);
    idTutor2 = idTutor;

    ///Tabla - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    //Tabla Tutorados
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery * query = new QSqlQuery();
    query->prepare("SELECT a.matricula, a.nombre FROM Alumno as a INNER JOIN Seccion as s ON a.idSeccion=s.idSeccion INNER JOIN Tutor as t ON t.idTutor=s.idTutor where t.idTutor='"+QString::number(idTutor2)+"'");
    query->exec();
    modal->setQuery(*query);
    ui->tablaTutorados->setModel(modal);
    ui->tablaTutorados->resizeColumnsToContents();


    ///Autocompletado - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


    QSqlQuery queryMat;
    QString matString = "", autMat = ui->lineNombre->text();
    QStringList CompletionList;


    queryMat.prepare("SELECT a.matricula, a.nombre FROM Alumno as a INNER JOIN Seccion as s ON a.idSeccion=s.idSeccion INNER JOIN Tutor as t ON t.idTutor=s.idTutor where t.idTutor='"+QString::number(idTutor2)+"'");
    if(queryMat.exec( )){
        while(queryMat.next()){
            matString = queryMat.value(0).toString();
            CompletionList << matString;
        }
    }

    StringCompleterSeccion = new QCompleter(CompletionList, this);
    StringCompleterSeccion->setCaseSensitivity(Qt::CaseInsensitive);
    ui->lineNombre->setCompleter(StringCompleterSeccion);

    QSqlQuery datosTutor;
    datosTutor.prepare("SELECT s.nombre, t.nombre, t.idTutor FROM Alumno as a INNER JOIN Seccion as s ON a.idSeccion=s.idSeccion "
                       "INNER JOIN Tutor as t ON t.idTutor=s.idTutor where t.idTutor='"+QString::number(idTutor2)+"'");

   datosTutor.exec();
   datosTutor.next();
   ui->label_NombreTutor->setText(datosTutor.value(1).toString());
   ui->labIDTutor->setText(datosTutor.value(2).toString());
   ui->labSeccion->setText(datosTutor.value(0).toString());

}

ventanaProfesor::~ventanaProfesor()
{
    delete ui;
}

void ventanaProfesor::on_Buscar_clicked()
{
    //Tabla Tutorados
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery * query = new QSqlQuery();
    QString autMat = ui->lineNombre->text();

    query->prepare("SELECT matricula as Matricula, nombre as 'Nombre del Alumno' FROM Alumno WHERE idSeccion = 1 AND matricula = '"+autMat +"'" );
    query->exec();
    modal->setQuery(*query);
    ui->tablaTutorados->setModel(modal);
    ui->tablaTutorados->resizeColumnsToContents();

}

void ventanaProfesor::on_lineNombre_textEdited(const QString &arg1)
{
    //Tabla Tutorados
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery * query = new QSqlQuery();
    query->prepare("SELECT matricula as Matricula, nombre as 'Nombre del Alumno' FROM Alumno WHERE idSeccion = 1");
    query->exec();
    modal->setQuery(*query);
    ui->tablaTutorados->setModel(modal);
    ui->tablaTutorados->resizeColumnsToContents();
}


void ventanaProfesor::on_pushButton_clicked()
{
    /*MainWindow * ventPrincipal = new MainWindow;
    ventPrincipal->show();


    close();*/
}
