#ifndef VENTANAALUMNOICC_H
#define VENTANAALUMNOICC_H

#include <QDialog>
#include "globals.h"

namespace Ui {
class ventanaAlumnoICC;
}

class ventanaAlumnoICC : public QDialog
{
    Q_OBJECT

public:
    explicit ventanaAlumnoICC(QWidget *parent = 0, int matricula=0);
    ~ventanaAlumnoICC();

private slots:


    void on_buttRegresar_clicked();

private:
    Ui::ventanaAlumnoICC *ui;
    int matricula2=0;
};

#endif // VENTANAALUMNOICC_H
