#include "ventanaadmin.h"
#include "ui_ventanaadmin.h"

ventanaAdmin::ventanaAdmin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ventanaAdmin)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose,true);

    ///Tablas - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    //Tabla Alumnos
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery * query = new QSqlQuery();
    query->prepare("SELECT TOP 100 matricula, nombre AS 'Nombre del Alumno' FROM Alumno");
    query->exec();
    modal->setQuery(*query);
    ui->tablaAlumnos->setModel(modal);
    ui->tablaAlumnos->resizeColumnsToContents();


    ///Autocompletado - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    //Alumnos

    QSqlQuery queryMatricula;
    QString matString = "";
    QStringList CompletionList;

    queryMatricula.prepare("SELECT matricula FROM Alumno");
    if(queryMatricula.exec( )){
        while(queryMatricula.next()){
            matString = queryMatricula.value(0).toString();
            CompletionList << matString;
        }
    }

    StringCompleter = new QCompleter(CompletionList, this);
    StringCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    ui->lineNuevaContra->setCompleter(StringCompleter);


}

ventanaAdmin::~ventanaAdmin()
{
    delete ui;
}

void ventanaAdmin::on_butRegresar_clicked()
{
    /*
    MainWindow * ventPrincipal = new MainWindow();
    ventPrincipal->show();

    close();
*/
}

void ventanaAdmin::on_pushButton_2_clicked()
{
    MainWindow * ventPrincipal = new MainWindow();
    ventPrincipal->show();

    close();

}

void ventanaAdmin::on_pushButton_3_clicked()
{
    //Tabla Tutorados
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery * query = new QSqlQuery();
    QString autMat = ui->lineNuevaContra->text();

    query->prepare("SELECT matricula as Matricula, nombre as 'Nombre del Alumno' FROM Alumno WHERE  matricula = '"+autMat +"'" );
    query->exec();
    modal->setQuery(*query);
    ui->tablaAlumnos->setModel(modal);
    ui->tablaAlumnos->resizeColumnsToContents();
}

void ventanaAdmin::on_pushButton_4_clicked()
{
    QMessageBox msgBox;
    msgBox.setText("Restablecer Contraseña.");
    msgBox.setInformativeText("¿Desea guardar los cambios?");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    msgBox.setButtonText(QMessageBox::Save,"Guardar");
    msgBox.setButtonText(QMessageBox::Discard,"Descartar");
    msgBox.setButtonText(QMessageBox::Cancel,"Cancelar");
    int ret = msgBox.exec();

    if (ret == QMessageBox::Save){

        QString matString = ui->lineNuevaContra->text();
        QSqlQuery cambioMMM;
        cambioMMM.prepare("UPDATE Alumno SET contrasegna = '" + ui->lineNuevaContra->text() + "', verificado=0 WHERE matricula="+ui->lineNuevaContra->text() +"");

        if(cambioMMM.exec()){

            QMessageBox msgBox;
            msgBox.setText("La contraseña se ha restablecido");
            msgBox.exec();
        }
    }

    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery * query = new QSqlQuery();
    query->prepare("SELECT TOP 100 matricula, nombre AS 'Nombre del Alumno' FROM Alumno");
    query->exec();
    modal->setQuery(*query);
    ui->tablaAlumnos->setModel(modal);
    ui->tablaAlumnos->resizeColumnsToContents();
    ui->lineNuevaContra->clear();

}


void ventanaAdmin::on_lineNuevaContra_textEdited(const QString &arg1)
{
    //Tabla alumnos
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery * query = new QSqlQuery();
    query->prepare("SELECT TOP 100 matricula, nombre AS 'Nombre del Alumno' FROM Alumno");
    query->exec();
    modal->setQuery(*query);
    ui->tablaAlumnos->setModel(modal);
    ui->tablaAlumnos->resizeColumnsToContents();

}
