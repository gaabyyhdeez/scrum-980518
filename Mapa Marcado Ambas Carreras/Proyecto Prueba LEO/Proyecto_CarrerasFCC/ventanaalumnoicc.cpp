#include "ventanaalumnoicc.h"
#include "ui_ventanaalumnoicc.h"
#include <QMessageBox>

ventanaAlumnoICC::ventanaAlumnoICC(QWidget *parent, int matricula) :
    QDialog(parent),
    ui(new Ui::ventanaAlumnoICC)
{
    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose,true);

    matricula2 = matricula;
    qDebug() << matricula2;

    QSqlQuery datosAlumno;
    datosAlumno.prepare("SELECT matricula, nombre FROM Alumno WHERE matricula="+QString::number(matricula2)+"");
    datosAlumno.exec();
    datosAlumno.next();

    qDebug() << datosAlumno.result();
    ui->label_NombreMatri->setText(datosAlumno.value(0).toString());
    ui->label_NombreAlm->setText(datosAlumno.value(1).toString());

    //primer semestre
    ui->Matematicas->setVisible(true);
    ui->Matematicas->setAccessibleName("1");
    ui->AlgebraSuperior->setVisible(true);
    ui->AlgebraSuperior->setAccessibleName("2");
    ui->MetodologiaDeLaProgramacion->setVisible(true);
    ui->MetodologiaDeLaProgramacion->setAccessibleName("3");
    ui->LenguaExtranjeraI->setVisible(true);
    ui->LenguaExtranjeraI->setAccessibleName("4");
    ui->FormacionHumanaySocial->setVisible(true);
    ui->FormacionHumanaySocial->setAccessibleName("5");
    //segundo semestre
    ui->CalculoDiferencial->setVisible(false);
    ui->CalculoDiferencial->setAccessibleName("6");
    ui->FisicaI->setVisible(false);
    ui->FisicaI->setAccessibleName("7");
    ui->AlgebraLinealConElementosDeGeometriaAnalitica->setVisible(false);
    ui->AlgebraLinealConElementosDeGeometriaAnalitica->setAccessibleName("8");
    ui->ProgramacionI->setVisible(false);
    ui->ProgramacionI->setAccessibleName("9");
    ui->LenguaExtranjeraII->setVisible(false);
    ui->LenguaExtranjeraII->setAccessibleName("10");
    ui->Dhpc->setVisible(true);
    ui->Dhpc->setAccessibleName("11");
    //tercer semestre
    ui->CalculoIntegral->setVisible(false);
    ui->CalculoIntegral->setAccessibleName("12");
    ui->FisicaII->setVisible(false);
    ui->FisicaII->setAccessibleName("13");
    ui->MatematicasDiscretas->setVisible(false);
    ui->MatematicasDiscretas->setAccessibleName("14");
    ui->ProgramacionII->setVisible(false);
    ui->ProgramacionII->setAccessibleName("15");
    ui->Ensamblador->setVisible(false);
    ui->Ensamblador->setAccessibleName("16");
    ui->LenguaExtranjeraIII->setVisible(false);
    ui->LenguaExtranjeraIII->setAccessibleName("17");
    //cuarto semestre
    ui->EcuacionesDiferenciales->setVisible(false);
    ui->EcuacionesDiferenciales->setAccessibleName("18");
    ui->CircuitosElectricos->setVisible(false);
    ui->CircuitosElectricos->setAccessibleName("19");
    ui->Graficacion->setVisible(false);
    ui->Graficacion->setAccessibleName("20");
    ui->EstructurasDeDatos->setVisible(false);
    ui->EstructurasDeDatos->setAccessibleName("21");
    ui->LenguaExtranjeraIV->setVisible(false);
    ui->LenguaExtranjeraIV->setAccessibleName("22");
    //quinto semestre
    ui->ProbabilidadyEstadistica->setVisible(false);
    ui->ProbabilidadyEstadistica->setAccessibleName("23");
    ui->CircuitosElectronicos->setVisible(false);
    ui->CircuitosElectronicos->setAccessibleName("24");
    ui->SistemasOperativosI->setVisible(false);
    ui->SistemasOperativosI->setAccessibleName("25");
    ui->AnalisisyDiseoDeAlgoritmos->setVisible(false);
    ui->AnalisisyDiseoDeAlgoritmos->setAccessibleName("26");
    ui->IngenieriaDeSoftware->setVisible(false);
    ui->IngenieriaDeSoftware->setAccessibleName("27");
    //sexto semestre
    ui->ModeloDeRedes->setVisible(false);
    ui->ModeloDeRedes->setAccessibleName("28");
    ui->DisenoDigital->setVisible(false);
    ui->DisenoDigital->setAccessibleName("29");
    ui->BaseDeDatosParaIngenieria->setVisible(false);
    ui->BaseDeDatosParaIngenieria->setAccessibleName("30");
    ui->SistemasOperativosII->setVisible(false);
    ui->SistemasOperativosII->setAccessibleName("31");
    ui->AdministracionDeProyectos->setVisible(true);
    ui->AdministracionDeProyectos->setAccessibleName("32");
    //septimo semestre
    ui->RedesInalambricas->setVisible(false);
    ui->RedesInalambricas->setAccessibleName("33");
    ui->MineriaDeDatos->setVisible(false);
    ui->MineriaDeDatos->setAccessibleName("34");
    ui->ArquitecturaDeComputo->setVisible(false);
    ui->ArquitecturaDeComputo->setAccessibleName("35");
    ui->ProgramacionConcurrenteyParalela->setVisible(false);
    ui->ProgramacionConcurrenteyParalela->setAccessibleName("36");
    ui->DesarrolloDeAplicacionesWeb->setVisible(false);
    ui->DesarrolloDeAplicacionesWeb->setAccessibleName("37");
    //octavo semestre
    ui->TeoriaDeControl->setVisible(false);
    ui->TeoriaDeControl->setAccessibleName("38");
    ui->AdministracionDeRedes->setVisible(false);
    ui->AdministracionDeRedes->setAccessibleName("39");
    ui->TecnicasDeInteligenciaArtificial->setVisible(false);
    ui->TecnicasDeInteligenciaArtificial->setAccessibleName("40");
    ui->ProgramacionDistribuidaAplicada->setVisible(false);
    ui->ProgramacionDistribuidaAplicada->setAccessibleName("41");
    ui->DesarrolloDeAplicacionesMoviles->setVisible(false);
    ui->DesarrolloDeAplicacionesMoviles->setAccessibleName("42");
    ui->OptativaI->setVisible(true);
    //ui->OptativaI->setAccessibleName("");
    //noveno semestre
    ui->IntercomunicacionySeguridadEnRedes->setVisible(false);
    ui->IntercomunicacionySeguridadEnRedes->setAccessibleName("55");
    ui->SistemasEmpotrados->setVisible(false);
    ui->SistemasEmpotrados->setAccessibleName("56");
    ui->ServicioSocial->setVisible(true);
    ui->ServicioSocial->setAccessibleName("57");
    ui->OptativaII->setVisible(true);
    //ui->OptativaII->setAccessibleName("");
    //decimo semestre
    ui->OptativaDesit->setVisible(true);
    //ui->OptativaDesit->setAccessibleName("");
    ui->PracticaProfesional->setVisible(true);
    ui->PracticaProfesional->setAccessibleName("69");
    ui->ProyectoIDI->setVisible(false);
    ui->ProyectoIDI->setAccessibleName("70");

    //Cargar avance-------------------------------------------------------------------
    int idmateria;

            QSqlQuery queryAvance;
            queryAvance.prepare("SELECT idMateria FROM Avance WHERE matricula = '"+QString::number(matricula2) +"' ");
            queryAvance.exec();


            while(queryAvance.next()){

                //Igualo la variable ID a cierto resultado del query para poder utilizar el switch.
                idmateria = queryAvance.value(0).toInt();

                switch (idmateria) {

                //idMateria de Matematicas es = 1
                case 1: {
                    //Marca la palomita correspondiente en el mapa curricular.
                    ui->Matematicas->setChecked(true);
                    //Habilita los demás botones de las materias consecuentes.
                    on_Matematicas_clicked(true);
                }
                    break;

                case 2: {
                    ui->AlgebraSuperior->setChecked(true);
                    on_AlgebraSuperior_clicked(true);
                    qDebug() << "2" ;
                }
                    break;

                case 3: {
                    ui->MetodologiaDeLaProgramacion->setChecked(true);
                    on_MetodologiaDeLaProgramacion_clicked(true);
                    qDebug() << "3" ;
                }
                    break;

                case 4: {
                    ui->LenguaExtranjeraI->setChecked(true);
                    on_LenguaExtranjeraI_clicked(true);
                    qDebug() << "4" ;
                }
                    break;

                case 5: {
                    ui->FormacionHumanaySocial->setChecked(true);
                    on_FormacionHumanaySocial_clicked(true);
                    qDebug() << "5" ;
                }
                    break;

                case 6: {
                    ui->CalculoDiferencial->setChecked(true);
                    on_CalculoDiferencial_clicked(true);
                }
                    break;

                case 7: {
                    ui->FisicaI->setChecked(true);
                    on_FisicaI_clicked(true);
                }
                    break;

                case 8: {
                    ui->AlgebraLinealConElementosDeGeometriaAnalitica->setChecked(true);
                    on_AlgebraLinealConElementosDeGeometriaAnalitica_clicked(true);
                }
                    break;

                case 9: {
                    ui->ProgramacionI->setChecked(true);
                    on_ProgramacionI_clicked(true);
                }
                    break;

                case 10:{
                    ui->LenguaExtranjeraII->setChecked(true);
                    on_LenguaExtranjeraII_clicked(true);
                }
                    break;

                case 11:{
                    ui->Dhpc->setChecked(true);
                    on_Dhpc_clicked(true);
                }
                    break;

                case 12:{
                    ui->CalculoIntegral->setChecked(true);
                    on_CalculoIntegral_clicked(true);
                }
                    break;

                case 13:{
                    ui->FisicaII->setChecked(true);
                    on_FisicaII_clicked(true);
                }
                    break;

                case 14:{
                    ui->MatematicasDiscretas->setChecked(true);
                    on_MatematicasDiscretas_clicked(true);
                }
                    break;

                case 15:{
                    ui->ProgramacionII->setChecked(true);
                    on_ProgramacionII_clicked(true);
                }
                    break;

                case 16:{
                    ui->Ensamblador->setChecked(true);
                    on_Ensamblador_clicked(true);
                }
                    break;

                case 17:{
                    ui->LenguaExtranjeraIII->setChecked(true);
                    on_LenguaExtranjeraIII_clicked(true);
                }
                    break;

                case 18:{
                    ui->EcuacionesDiferenciales->setChecked(true);
                    on_EcuacionesDiferenciales_clicked(true);
                }
                    break;

                case 19:{
                    ui->CircuitosElectricos->setChecked(true);
                    on_CircuitosElectricos_clicked(true);
                }
                    break;

                case 20:{
                    ui->Graficacion->setChecked(true);
                    on_Graficacion_clicked(true);
                }
                    break;

                case 21:{
                    ui->EstructurasDeDatos->setChecked(true);
                    on_EstructurasDeDatos_clicked(true);
                }
                    break;

                case 22:{
                    ui->LenguaExtranjeraIV->setChecked(true);
                    on_LenguaExtranjeraIV_clicked(true);
                }
                    break;

                case 23:{
                    ui->ProbabilidadyEstadistica->setChecked(true);
                    on_ProbabilidadyEstadistica_clicked(true);
                }
                    break;

                case 24:{
                    ui->CircuitosElectronicos->setChecked(true);
                    on_CircuitosElectronicos_clicked(true);
                }
                    break;

                case 25:{
                    ui->SistemasOperativosI->setChecked(true);
                    on_SistemasOperativosI_clicked(true);
                }
                    break;

                case 26:{
                    ui->AnalisisyDiseoDeAlgoritmos->setChecked(true);
                    on_AnalisisyDiseoDeAlgoritmos_clicked(true);
                }
                    break;

                case 27:{
                    ui->IngenieriaDeSoftware->setChecked(true);
                    on_IngenieriaDeSoftware_clicked(true);
                }
                    break;

                case 28:{
                    ui->ModeloDeRedes->setChecked(true);
                    on_IngenieriaDeSoftware_clicked(true);
                }
                    break;

                case 29:{
                    ui->DisenoDigital->setChecked(true);
                    on_DisenoDigital_clicked(true);
                }
                    break;

                case 30:{
                    ui->BaseDeDatosParaIngenieria->setChecked(true);
                    on_BaseDeDatosParaIngenieria_clicked(true);
                }
                    break;

                case 31:{
                    ui->SistemasOperativosII->setChecked(true);
                    on_SistemasOperativosII_clicked(true);
                }
                    break;

                case 32:{
                    ui->AdministracionDeProyectos->setChecked(true);
                    on_AdministracionDeProyectos_clicked(true);
                }
                    break;

                case 33:{
                    ui->RedesInalambricas->setChecked(true);
                    on_RedesInalambricas_clicked(true);
                }
                    break;

                case 34:{
                    ui->MineriaDeDatos->setChecked(true);
                    on_MineriaDeDatos_clicked(true);
                }
                    break;

                case 35:{
                    ui->ArquitecturaDeComputo->setChecked(true);
                    on_ArquitecturaDeComputo_clicked(true);
                }
                    break;

                case 36:{
                    ui->ProgramacionConcurrenteyParalela->setChecked(true);
                    on_ProgramacionConcurrenteyParalela_clicked(true);
                }
                    break;

                case 37:{
                    ui->DesarrolloDeAplicacionesWeb->setChecked(true);
                    on_DesarrolloDeAplicacionesWeb_clicked(true);
                }
                    break;

                case 38:{
                    ui->TeoriaDeControl->setChecked(true);
                    on_TeoriaDeControl_clicked(true);
                }
                    break;

                case 39:{
                    ui->AdministracionDeRedes->setChecked(true);
                    on_AdministracionDeRedes_clicked(true);
                }
                    break;

                case 40:{
                    ui->TecnicasDeInteligenciaArtificial->setChecked(true);
                    on_TecnicasDeInteligenciaArtificial_clicked(true);
                }
                    break;

                case 41:{
                    ui->ProgramacionDistribuidaAplicada->setChecked(true);
                    on_ProgramacionDistribuidaAplicada_clicked(true);
                }
                    break;

                case 42:{
                    ui->DesarrolloDeAplicacionesMoviles->setChecked(true);
                    on_DesarrolloDeAplicacionesMoviles_clicked(true);
                }
                    break;

                    /* OPTATIVAS
                case 43:{
                    ui->opt
                }
                    break;

                case 44:
                    break;

                case 45:
                    break;

                case 46:
                    break;

                case 47:
                    break;

                case 48:
                    break;

                case 49:
                    break;

                case 50:
                    break;

                case 51:
                    break;

                case 52:
                    break;

                case 53:
                    break;


                case 54:
                    break;
                Terminan optativas octavo semestre*/

                case 55:{
                    ui->IntercomunicacionySeguridadEnRedes->setChecked(true);
                    on_IntercomunicacionySeguridadEnRedes_clicked(true);
                }
                    break;

                case 56:{
                    ui->SistemasEmpotrados->setChecked(true);
                    on_SistemasEmpotrados_clicked(true);
                }
                    break;

                case 57:{
                    ui->ServicioSocial->setChecked(true);
                    on_ServicioSocial_clicked(true);
                }
                    break;

                    /*OPTATIVAS NOVENO
                case 58:
                    break;

                case 59:
                    break;

                case 60:
                    break;

                case 61:
                    break;

                case 62:
                    break;

                case 63:
                    break;

                case 64:
                    break;

                case 65:
                    break;

                case 66:
                    break;

                case 67:
                    break;

                case 68:
                    break;
                    TERMINAN OPTATIVAS NOVENO*/

                case 69:{
                    ui->PracticaProfesional->setChecked(true);
                    on_PracticaProfesional_clicked(true);
                }
                    break;

                case 70:{
                    ui->ProyectoIDI->setChecked(true);
                    on_ProyectoIDI_clicked(true);
                }
                    break;

                    /*OPTATIVAS DESIT
                case 71:
                    break;

                case 72:
                    break;

                case 73:
                    break;
                   TERMINAN OPTATIVAS DESIT*/

                default:
                    qDebug() << "Default" ;
                    break;
                }


            }

            qDebug() << queryAvance.result();

    //Porcentaje
    ui->labPorcentaje->setText(calculaPorcentaje().append("%"));
}

ventanaAlumnoICC::~ventanaAlumnoICC()
{
    delete ui;
}


void ventanaAlumnoICC::on_buttRegresar_clicked()
{

}

//Primer semestre
void ventanaAlumnoICC::on_Matematicas_clicked(bool checked)
{
    ui->CalculoDiferencial->setVisible(checked);
    ui->FisicaI->setVisible(checked);

    if(!checked){
        ui->CalculoDiferencial->setChecked(checked);
        ui->FisicaI->setChecked(checked);

        on_CalculoDiferencial_clicked(checked);
        on_FisicaI_clicked(checked);
    }
}

void ventanaAlumnoICC::on_AlgebraSuperior_clicked(bool checked)
{
    ui->AlgebraLinealConElementosDeGeometriaAnalitica->setVisible(checked);
    if(!checked){
        ui->AlgebraLinealConElementosDeGeometriaAnalitica->setChecked(checked);
        on_AlgebraLinealConElementosDeGeometriaAnalitica_clicked(checked);
    }
}

void ventanaAlumnoICC::on_MetodologiaDeLaProgramacion_clicked(bool checked)
{
    ui->ProgramacionI->setVisible(checked);
    if(!checked){
        ui->ProgramacionI->setChecked(checked);
        on_ProgramacionI_clicked(checked);
    }
}

void ventanaAlumnoICC::on_LenguaExtranjeraI_clicked(bool checked)
{
    ui->LenguaExtranjeraII->setVisible(checked);
    if(!checked){
        ui->LenguaExtranjeraII->setChecked(checked);
        on_LenguaExtranjeraII_clicked(checked);
    }
}

void ventanaAlumnoICC::on_FormacionHumanaySocial_clicked(bool checked)
{
    //No tiene materia consecuente.
}

//Segundo Semestre

void ventanaAlumnoICC::on_CalculoDiferencial_clicked(bool checked)
{
    ui->CalculoIntegral->setVisible(checked);
    if(!checked){
        ui->CalculoIntegral->setChecked(checked);
        on_CalculoIntegral_clicked(checked);
    }
}

void ventanaAlumnoICC::on_FisicaI_clicked(bool checked)
{
    ui->FisicaII->setVisible(checked);
    if(!checked){
        ui->FisicaII->setChecked(checked);
        on_FisicaII_clicked(checked);
    }
}

void ventanaAlumnoICC::on_AlgebraLinealConElementosDeGeometriaAnalitica_clicked(bool checked)
{
    if(ui->ProgramacionII->isChecked())
        ui->Graficacion->setVisible(checked);

    ui->MatematicasDiscretas->setVisible(checked);

    if (!checked){
        ui->MatematicasDiscretas->setChecked(checked);
        ui->Graficacion->setChecked(checked);

        //on_MatematicasDiscretas_clicked(checked);
        //on_Graficacion_clicked(checked);
    }
}

void ventanaAlumnoICC::on_ProgramacionI_clicked(bool checked)
{
    ui->ProgramacionII->setVisible(checked);
    ui->Ensamblador->setVisible(checked);

    if(!checked){
        ui->ProgramacionII->setChecked(checked);
        ui->Ensamblador->setChecked(checked);

        on_ProgramacionII_clicked(checked);
        on_Ensamblador_clicked(checked);
    }
}

void ventanaAlumnoICC::on_LenguaExtranjeraII_clicked(bool checked)
{
    ui->LenguaExtranjeraIII->setVisible(checked);
    if(!checked){
        ui->LenguaExtranjeraIII->setChecked(checked);
        on_LenguaExtranjeraIII_clicked(checked);
    }
}

void ventanaAlumnoICC::on_Dhpc_clicked(bool checked)
{
    //No tiene materia consecuente.
}

//Tercer Semestre

void ventanaAlumnoICC::on_CalculoIntegral_clicked(bool checked)
{
    ui->EcuacionesDiferenciales->setVisible(checked);
    ui->ProbabilidadyEstadistica->setVisible(checked);

    if(!checked){
        ui->EcuacionesDiferenciales->setChecked(checked);
        ui->ProbabilidadyEstadistica->setChecked(checked);

        on_EcuacionesDiferenciales_clicked(checked);
        on_ProbabilidadyEstadistica_clicked(checked);
    }
}

void ventanaAlumnoICC::on_FisicaII_clicked(bool checked)
{
    ui->CircuitosElectricos->setVisible(checked);
    if(!checked){
        ui->CircuitosElectricos->setChecked(checked);
        on_CircuitosElectricos_clicked(checked);
    }
}

void ventanaAlumnoICC::on_MatematicasDiscretas_clicked(bool checked)
{
    //No tiene materia consecuente.
}

void ventanaAlumnoICC::on_ProgramacionII_clicked(bool checked)
{
    if(ui->AlgebraLinealConElementosDeGeometriaAnalitica->isChecked())
        ui->Graficacion->setVisible(checked);

    ui->EstructurasDeDatos->setVisible(checked);
    ui->IngenieriaDeSoftware->setVisible(checked);

    if(!checked){

        ui->Graficacion->setChecked(checked);
        ui->EstructurasDeDatos->setChecked(checked);
        ui->IngenieriaDeSoftware->setChecked(checked);

        on_EstructurasDeDatos_clicked(checked);
    }
}

void ventanaAlumnoICC::on_Ensamblador_clicked(bool checked)
{
    if(ui->EstructurasDeDatos->isChecked())
        ui->SistemasOperativosI->setVisible(checked);
    if(!checked){
        ui->SistemasOperativosI->setChecked(checked);
        on_SistemasOperativosI_clicked(checked);
    }
}

void ventanaAlumnoICC::on_LenguaExtranjeraIII_clicked(bool checked)
{
    ui->LenguaExtranjeraIV->setVisible(checked);
    if(!checked)
        ui->LenguaExtranjeraIV->setChecked(checked);
}

//Cuarto Semestre

void ventanaAlumnoICC::on_EcuacionesDiferenciales_clicked(bool checked)
{
    ui->TeoriaDeControl->setVisible(checked);
    if(!checked)
        ui->TeoriaDeControl->setChecked(checked);
}

void ventanaAlumnoICC::on_CircuitosElectricos_clicked(bool checked)
{
    ui->CircuitosElectronicos->setVisible(checked);
    if(!checked){
        ui->CircuitosElectronicos->setChecked(checked);
        on_CircuitosElectronicos_clicked(checked);
    }
}

void ventanaAlumnoICC::on_Graficacion_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_EstructurasDeDatos_clicked(bool checked)
{
    if(ui->Ensamblador->isChecked())
        ui->SistemasOperativosI->setVisible(checked);

    ui->AnalisisyDiseoDeAlgoritmos->setVisible(checked);
    ui->BaseDeDatosParaIngenieria->setVisible(checked);

    if(!checked){
        ui->AnalisisyDiseoDeAlgoritmos->setChecked(checked);
        ui->SistemasOperativosI->setChecked(checked);
        ui->BaseDeDatosParaIngenieria->setChecked(checked);

        on_AnalisisyDiseoDeAlgoritmos_clicked(checked);
        on_SistemasOperativosI_clicked(checked);
    }
}

void ventanaAlumnoICC::on_LenguaExtranjeraIV_clicked(bool checked)
{
    //No tiene materia consecuente.
}

//Quinto Semestre

void ventanaAlumnoICC::on_ProbabilidadyEstadistica_clicked(bool checked)
{
    ui->ModeloDeRedes->setVisible(checked);
    ui->MineriaDeDatos->setVisible(checked);
    if(!checked){
        ui->ModeloDeRedes->setChecked(checked);
        ui->MineriaDeDatos->setChecked(checked);
        on_ModeloDeRedes_clicked(checked);
    }
}

void ventanaAlumnoICC::on_CircuitosElectronicos_clicked(bool checked)
{
    ui->DisenoDigital->setVisible(checked);
    if(!checked){
        ui->DisenoDigital->setChecked(checked);
        on_DisenoDigital_clicked(checked);
    }
}

void ventanaAlumnoICC::on_SistemasOperativosI_clicked(bool checked)
{
    ui->SistemasOperativosII->setVisible(checked);
    if(!checked)
        ui->SistemasOperativosII->setChecked(checked);
}

void ventanaAlumnoICC::on_AnalisisyDiseoDeAlgoritmos_clicked(bool checked)
{
    ui->ProgramacionConcurrenteyParalela->setVisible(checked);
    ui->TecnicasDeInteligenciaArtificial->setVisible(checked);
    if(!checked){
        ui->ProgramacionConcurrenteyParalela->setChecked(checked);
        ui->TecnicasDeInteligenciaArtificial->setChecked(checked);
        on_ProgramacionConcurrenteyParalela_clicked(checked);
    }
}

void ventanaAlumnoICC::on_IngenieriaDeSoftware_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

//Sexto Semestre

void ventanaAlumnoICC::on_ModeloDeRedes_clicked(bool checked)
{
    ui->RedesInalambricas->setVisible(checked);
    if(!checked){
        ui->RedesInalambricas->setChecked(checked);
        on_RedesInalambricas_clicked(checked);
    }
}

void ventanaAlumnoICC::on_DisenoDigital_clicked(bool checked)
{
    ui->ArquitecturaDeComputo->setVisible(checked);
    if (!checked){
        ui->ArquitecturaDeComputo->setChecked(checked);
        on_ArquitecturaDeComputo_clicked(checked);
    }
}

void ventanaAlumnoICC::on_BaseDeDatosParaIngenieria_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_SistemasOperativosII_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_AdministracionDeProyectos_clicked(bool checked)
{
    if(ui->OptativaII->isChecked())
        ui->ProyectoIDI->setVisible(checked);

    ui->DesarrolloDeAplicacionesWeb->setVisible(checked);
    if(!checked){
        ui->DesarrolloDeAplicacionesWeb->setChecked(checked);
        ui->ProyectoIDI->setChecked(checked);
        on_DesarrolloDeAplicacionesWeb_clicked(checked);
    }

}

/// ------------- Séptimo Semestre.

void ventanaAlumnoICC::on_RedesInalambricas_clicked(bool checked)
{
    ui->AdministracionDeRedes->setVisible(checked);
    if (!checked){
        ui->AdministracionDeRedes->setChecked(checked);
        on_AdministracionDeRedes_clicked(checked);
    }
}

void ventanaAlumnoICC::on_MineriaDeDatos_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_ArquitecturaDeComputo_clicked(bool checked)
{
    ui->SistemasEmpotrados->setVisible(checked);
    if(!checked)
        ui->SistemasEmpotrados->setChecked(false);
}

void ventanaAlumnoICC::on_ProgramacionConcurrenteyParalela_clicked(bool checked)
{
    ui->ProgramacionDistribuidaAplicada->setVisible(checked);
    if(!checked)
        ui->ProgramacionDistribuidaAplicada->setChecked(checked);
}

void ventanaAlumnoICC::on_DesarrolloDeAplicacionesWeb_clicked(bool checked)
{
    ui->DesarrolloDeAplicacionesMoviles->setVisible(checked);
    if(!checked)
        ui->DesarrolloDeAplicacionesMoviles->setChecked(checked);
}


/// ------------- Octavo Semestre.


void ventanaAlumnoICC::on_TeoriaDeControl_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_AdministracionDeRedes_clicked(bool checked)
{
    ui->IntercomunicacionySeguridadEnRedes->setVisible(checked);
    if (!checked)
        ui->IntercomunicacionySeguridadEnRedes->setChecked(checked);
}

void ventanaAlumnoICC::on_TecnicasDeInteligenciaArtificial_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_ProgramacionDistribuidaAplicada_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_DesarrolloDeAplicacionesMoviles_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_OptativaI_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

/// -------------  Noveno Semestre.

void ventanaAlumnoICC::on_IntercomunicacionySeguridadEnRedes_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_SistemasEmpotrados_clicked(bool checked)
{
    //No tiene materias consecuentes.
}


void ventanaAlumnoICC::on_ServicioSocial_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_OptativaII_clicked(bool checked)
{
    if(ui->AdministracionDeProyectos->isChecked())
        ui->ProyectoIDI->setVisible(checked);
    if (!checked)
        ui->ProyectoIDI->setChecked(checked);
}


/// -------------  Décimo Semestre.


void ventanaAlumnoICC::on_OptativaDesit_clicked(bool checked)
{
    //No tiene materias consecuentes.
}


void ventanaAlumnoICC::on_PracticaProfesional_clicked(bool checked)
{
    //No tiene materias consecuentes.
}


void ventanaAlumnoICC::on_ProyectoIDI_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_buttGuardar_clicked()
{
    QMessageBox mBoxConfirmar(QMessageBox::Question, "Guardar", "¿Desea guardar los cambios?", QMessageBox::Yes | QMessageBox::No);

    mBoxConfirmar.setButtonText(QMessageBox::Yes, "Sí");
    mBoxConfirmar.setButtonText(QMessageBox::No, "No");


    if(mBoxConfirmar.exec() == QMessageBox::Yes)
    {
        QSqlQuery eliminar, guardar;
            QString idMateria;

            eliminar.prepare("DELETE FROM avance WHERE matricula = " + QString::number(matricula2) + "");
            eliminar.exec();

            qDebug() << eliminar.lastError().text();

            //primer semestre
            if(ui->Matematicas->isChecked())
            {
                idMateria = ui->Matematicas->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->AlgebraSuperior->isChecked())
            {
                idMateria = ui->AlgebraSuperior->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->MetodologiaDeLaProgramacion->isChecked())
            {
                idMateria = ui->MetodologiaDeLaProgramacion->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->LenguaExtranjeraI->isChecked())
            {
                idMateria = ui->LenguaExtranjeraI->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->FormacionHumanaySocial->isChecked())
            {
                idMateria = ui->FormacionHumanaySocial->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            //segundo semestre
            if(ui->CalculoDiferencial->isChecked())
            {
                idMateria = ui->CalculoDiferencial->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->FisicaI->isChecked())
            {
                idMateria = ui->FisicaI->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->AlgebraLinealConElementosDeGeometriaAnalitica->isChecked())
            {
                idMateria = ui->AlgebraLinealConElementosDeGeometriaAnalitica->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->ProgramacionI->isChecked())
            {
                idMateria = ui->ProgramacionI->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->LenguaExtranjeraII->isChecked())
            {
                idMateria = ui->LenguaExtranjeraII->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->Dhpc->isChecked())
            {
                idMateria = ui->Dhpc->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            //tercer semestre
            if(ui->CalculoIntegral->isChecked())
            {
                idMateria = ui->CalculoIntegral->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->FisicaII->isChecked())
            {
                idMateria = ui->FisicaII->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->MatematicasDiscretas->isChecked())
            {
                idMateria = ui->MatematicasDiscretas->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->ProgramacionII->isChecked())
            {
                idMateria = ui->ProgramacionII->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->Ensamblador->isChecked())
            {
                idMateria = ui->Ensamblador->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->LenguaExtranjeraIII->isChecked())
            {
                idMateria = ui->LenguaExtranjeraIII->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            //cuarto semestre
            if(ui->EcuacionesDiferenciales->isChecked())
            {
                idMateria = ui->EcuacionesDiferenciales->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->CircuitosElectricos->isChecked())
            {
                idMateria = ui->CircuitosElectricos->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->Graficacion->isChecked())
            {
                idMateria = ui->Graficacion->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->EstructurasDeDatos->isChecked())
            {
                idMateria = ui->EstructurasDeDatos->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->LenguaExtranjeraIV->isChecked())
            {
                idMateria = ui->LenguaExtranjeraIV->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            //quinto semestre
            if(ui->ProbabilidadyEstadistica->isChecked())
            {
                idMateria = ui->ProbabilidadyEstadistica->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->CircuitosElectronicos->isChecked())
            {
                idMateria = ui->CircuitosElectronicos->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->SistemasOperativosI->isChecked())
            {
                idMateria = ui->SistemasOperativosI->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->AnalisisyDiseoDeAlgoritmos->isChecked())
            {
                idMateria = ui->AnalisisyDiseoDeAlgoritmos->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->IngenieriaDeSoftware->isChecked())
            {
                idMateria = ui->IngenieriaDeSoftware->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            //sexto semestre
            if(ui->ModeloDeRedes->isChecked())
            {
                idMateria = ui->ModeloDeRedes->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->DisenoDigital->isChecked())
            {
                idMateria = ui->DisenoDigital->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->BaseDeDatosParaIngenieria->isChecked())
            {
                idMateria = ui->BaseDeDatosParaIngenieria->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->SistemasOperativosII->isChecked())
            {
                idMateria = ui->SistemasOperativosII->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->AdministracionDeProyectos->isChecked())
            {
                idMateria = ui->AdministracionDeProyectos->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            //septimo semestre
            if(ui->RedesInalambricas->isChecked())
            {
                idMateria = ui->RedesInalambricas->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->MineriaDeDatos->isChecked())
            {
                idMateria = ui->MineriaDeDatos->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->ArquitecturaDeComputo->isChecked())
            {
                idMateria = ui->ArquitecturaDeComputo->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->ProgramacionConcurrenteyParalela->isChecked())
            {
                idMateria = ui->ProgramacionConcurrenteyParalela->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->DesarrolloDeAplicacionesWeb->isChecked())
            {
                idMateria = ui->DesarrolloDeAplicacionesWeb->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            //octavo semestre
            if(ui->TeoriaDeControl->isChecked())
            {
                idMateria = ui->TeoriaDeControl->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->AdministracionDeRedes->isChecked())
            {
                idMateria = ui->AdministracionDeRedes->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->TecnicasDeInteligenciaArtificial->isChecked())
            {
                idMateria = ui->TecnicasDeInteligenciaArtificial->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->ProgramacionDistribuidaAplicada->isChecked())
            {
                idMateria = ui->ProgramacionDistribuidaAplicada->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->DesarrolloDeAplicacionesMoviles->isChecked())
            {
                idMateria = ui->DesarrolloDeAplicacionesMoviles->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            //noveno semestre
            if(ui->IntercomunicacionySeguridadEnRedes->isChecked())
            {
                idMateria = ui->IntercomunicacionySeguridadEnRedes->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->SistemasEmpotrados->isChecked())
            {
                idMateria = ui->SistemasEmpotrados->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->ServicioSocial->isChecked())
            {
                idMateria = ui->ServicioSocial->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            //decimo semestre
            if(ui->PracticaProfesional->isChecked())
            {
                idMateria = ui->PracticaProfesional->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }
            if(ui->ProyectoIDI->isChecked())
            {
                idMateria = ui->ProyectoIDI->accessibleName();

                guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" + QString::number(matricula2) + ", " + idMateria + ")");
                guardar.exec();

                qDebug() << guardar.lastError().text();
            }

            //Porcentaje
            ui->labPorcentaje->setText(calculaPorcentaje().append("%"));
    }
}

//Funcion para calcular el porcentaje
QString ventanaAlumnoICC::calculaPorcentaje()
{
    QSqlQuery queryPorcentaje;

    queryPorcentaje.prepare("SELECT (((SUM(M.creditos))*100)/421) as Av FROM Avance AS A "
                            "INNER JOIN Materia as M ON A.idMateria=M.idMateria "
                            "INNER JOIN Alumno as Alm ON Alm.matricula=A.matricula "
                            "WHERE A.matricula="+QString::number(matricula2)+" GROUP BY A.matricula");

    queryPorcentaje.exec();
    queryPorcentaje.next();
    qDebug() << queryPorcentaje.result() << endl;

    QString porcentaje = queryPorcentaje.value(0).toString();

    return porcentaje;
}
