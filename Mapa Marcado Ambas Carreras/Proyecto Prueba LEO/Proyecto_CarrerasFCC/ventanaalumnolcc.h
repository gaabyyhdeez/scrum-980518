#ifndef VENTANAALUMNOLCC_H
#define VENTANAALUMNOLCC_H

#include <QDialog>
#include "globals.h"

namespace Ui {
class ventanaAlumnoLCC;
}

class ventanaAlumnoLCC : public QDialog
{
    Q_OBJECT

public:
    explicit ventanaAlumnoLCC(QWidget *parent = 0, int matricula = 0);
    ~ventanaAlumnoLCC();

private slots:


    void on_MatematicasElementales_clicked(bool checked);

    void on_AlgebraSuperior_clicked(bool checked);

    void on_MetodologiaDeLaProgramacion_clicked(bool checked);

    void on_ProgramacionI_clicked(bool checked);

    void on_FormacionHumanaySocial_clicked(bool checked);

    void on_LenguaExtranjeraI_clicked(bool checked);

    void on_CalculoDiferencial_clicked(bool checked);

    void on_AlgebraLineal_clicked(bool checked);

    void on_LenguaExtranjeraII_clicked(bool checked);

    void on_CalculoIntegral_clicked(bool checked);

    void on_ProgramacionII_clicked(bool checked);

    void on_Dhpc_clicked(bool checked);

    void on_Ensamblador_clicked(bool checked);

    void on_EstructurasDiscretas_clicked(bool checked);

    void on_LenguaExtranjeraIII_clicked(bool checked);


    void on_CircuitosElectricos_clicked(bool checked);

    void on_EstructuraDeDatos_clicked(bool checked);

    void on_LogicaMatematica_clicked(bool checked);

    void on_LenguajesFormalesyAutomatas_clicked(bool checked);

    void on_ProbabilidadyEstadistica_clicked(bool checked);

    void on_SistemasOperativosI_clicked(bool checked);

    void on_AnalisisyDisenoDeAlgoritmos_clicked(bool checked);

    void on_BasesDeDatos_clicked(bool checked);

    void on_IngenieriaDeSoftware_clicked(bool checked);

    void on_FundamentosDeLenguajesDeProgramacion_clicked(bool checked);

    void on_ProgramacionConcurrenteyParalela_clicked(bool checked);

    void on_Graficacion_clicked(bool checked);

    void on_RedesDeComputadoras_clicked(bool checked);

    void on_AdministracionDeProyectos_clicked(bool checked);

    void on_CircuitosLogicos_clicked(bool checked);

    void on_SistemasOperativosII_clicked(bool checked);

    void on_ProgramacionDistribuida_clicked(bool checked);


    void on_InteligenciaArtificial_clicked(bool checked);

    void on_SeguridadEnRedes_clicked(bool checked);


    void on_Computabilidad_clicked(bool checked);

    void on_RecuperacionDeLaInformacion_clicked(bool checked);

    void on_ServicioSocial_clicked(bool checked);

    void on_Optativa1_clicked(bool checked);

    void on_Optativa2_clicked(bool checked);

    void on_ArquitecturaFuncionalDeComputadoras_clicked(bool checked);

    void on_Optativa1Desit_clicked(bool checked);

    void on_ProyectoIDI_clicked(bool checked);

    void on_Optativa3_clicked(bool checked);

    void on_PracticaProfesional_clicked(bool checked);

    void on_Optativa4_clicked(bool checked);

    void on_Optativa5_clicked(bool checked);

    void on_LenguaExtranjeraIV_clicked(bool checked);

    void on_buttGuardar_clicked();

    //Declaracion de funcion para calcular el porcentaje
    QString calculaPorcentaje();

private:
    Ui::ventanaAlumnoLCC *ui;
    int matricula2;
};

#endif // VENTANAALUMNOLCC_H
