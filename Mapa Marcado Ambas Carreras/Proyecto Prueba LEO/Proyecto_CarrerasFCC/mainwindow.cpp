#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);  
    //QMainWindow::showFullScreen();
    setAttribute(Qt::WA_DeleteOnClose,true);

    //Conexión Rebeca
    QSqlDatabase sDataBase = QSqlDatabase::addDatabase("QODBC");
                sDataBase.setConnectOptions();
               QString dsn = QString("DRIVER = {SQL Server};SERVER=%1;DATABASE=%2;Trusted_Connection=yes;").arg("REBECARQ\\MSSQLSERVEREBE").arg("MapaCurricular");
                sDataBase.setDatabaseName("mydsn");

    /*/Conexión Gabi
    QSqlDatabase sDataBase = QSqlDatabase::addDatabase("QODBC");
    sDataBase.setConnectOptions();
    QString dsn = QString("DRIVER={SQL Server};SERVER=%1;DATABASE=%2;Trusted_Connection=yes;").arg("GABY-PC").arg("MapaCurricular");
    sDataBase.setDatabaseName(dsn);*/

    //Conexión Dana
    //sDataBase = QSqlDatabase::addDatabase("QODBC");
    //sDataBase.setDatabaseName("DRIVER={SQL SERVER};SERVER=DESKTOP-U62I4AI;DATABASE=MapaCurricular;UID=danae;PWD=;Trusted_Connection=yes;");

    //Conexión Luis
    //sDataBase = QSqlDatabase::addDatabase("QODBC");
    //sDataBase.setDatabaseName("DRIVER={SQL SERVER};SERVER=LUIS-ANGEL;DATABASE=MapaCurricular;UID=luis;PWD=;Trusted_Connection=yes;");

    //Conexión Leo
    //sDataBase = QSqlDatabase::addDatabase("QODBC");
    //sDataBase.setDatabaseName("DRIVER={SQL SERVER};SERVER=DESKTOP-3Q3S6AP;DATABASE=MapaCurricular;UID=Luis Leonardo;PWD=;Trusted_Connection=yes;");

    if(sDataBase.open())
    {
        qDebug() << "Base CONECTADA, POR FIN! :'D";
    }
    else
    {
        qDebug() << "No Conectado D':";
    }


    /*mModel = new QSqlTableModel(this);
    mModel->setTable("persona");
    mModel->select();
    ui->tableView->setModel(mModel);*/

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::modificaContrasenia()
{
    if(ui->rBut_Alumno->isChecked())
    {
        ventanaModificaContrasenaAlm * modifContraAlm = new ventanaModificaContrasenaAlm(this);
        modifContraAlm->setModal(true);
        modifContraAlm->exec();
    }
}

void MainWindow::on_butIngresar_clicked()
{
    if((!ui->lineUsuario->text().isEmpty()) && (!ui->lineContrasenia->text().isEmpty()))
    {
        if(ui->rBut_Alumno->isChecked())
        {
            /*ventanaAlumnoICC * ventAlmICC = new ventanaAlumnoICC(this);
            ventAlmICC->setModal(true);
            ventAlmICC->exec();*/
            QSqlQuery checaAlm;
            checaAlm.exec("SELECT matricula, idCarrera, verificado FROM Alumno WHERE matricula='"+ui->lineUsuario->text()+"' and contrasegna='"+ui->lineContrasenia->text()+"'");
            if(checaAlm.next())
            {
                int verificado = checaAlm.value(2).toInt();

                if(verificado==0)
                {
                     ui->lineContrasenia->clear();
                   matricula = checaAlm.value(0).toInt();
                   ventanaModificaContrasenaAlm * ventMod = new ventanaModificaContrasenaAlm(this, matricula);
                   ventMod->setModal(true);
                   ventMod->exec();
                }else{
                    ui->lineUsuario->clear();
                    ui->lineContrasenia->clear();

                    matricula = checaAlm.value(0).toInt();
                    if(checaAlm.value(1).toInt()==1)
                    {
                        qDebug() << "LCC= " << matricula << endl;
                        ventanaAlumnoLCC * ventLCC = new ventanaAlumnoLCC(this, matricula);
                        ventLCC->setModal(true);
                        ventLCC->exec();
                        //this->close();
                    }else{
                        qDebug() << "ICC= " << matricula << endl;
                        ventanaAlumnoICC * ventICC = new ventanaAlumnoICC(this, matricula);
                        ventICC->setModal(true);
                        ventICC->exec();
                        //this->close();
                    }
                }
            }else{
                QMessageBox::warning(this,"ERROR","Usuario o contraseña incorrectos");
            }

        }else{
            if(ui->rBut_Profesor->isChecked())
            {
                QSqlQuery checaProf;
                checaProf.exec("SELECT idTutor, verificado FROM Tutor WHERE idTutor='"+ui->lineUsuario->text()+"' and contrasegna='"+ui->lineContrasenia->text()+"'");
                if(checaProf.next())
                {
                    int verificado = checaProf.value(1).toInt();

                    if(verificado==1)
                    {
                        ui->lineUsuario->clear();
                        ui->lineContrasenia->clear();
                       idTutor = checaProf.value(0).toInt();
                       ventanaProfesor * ventProf = new ventanaProfesor(this, idTutor);
                       ventProf->setModal(true);
                       ventProf->exec();
                       //this->close();
                    }else{
                        QMessageBox::warning(this,"ERROR","No eres Tutor");
                    }
                }else{
                    QMessageBox::warning(this,"ERROR","Usuario o contraseña incorrectos");
                }
                /*ventanaProfesor * ventProf = new ventanaProfesor(this);
                ventProf->setModal(true);
                ventProf->exec();*/
            }else{
                if(ui->rBut_Coordinador->isChecked())
                {
                    if(ui->lineContrasenia->text() == "coordinador")
                    {
                        ui->lineUsuario->clear();
                        ui->lineContrasenia->clear();

                        ventanaCoordinador * ventAlmICC = new ventanaCoordinador(this);
                        ventAlmICC->setModal(true);
                        ventAlmICC->exec();

                        //this->close();
                    }else{
                        QMessageBox::warning(this,"ERROR","Contraseña incorrecta");
                    }

                }else{
                    if(ui->rBut_Admin->isChecked())
                    {
                        if(ui->lineContrasenia->text() == "admin")
                        {
                            ui->lineUsuario->clear();
                            ui->lineContrasenia->clear();
                            ventanaAdmin * ventAdm = new ventanaAdmin(this);
                            ventAdm->setModal(true);
                            ventAdm->exec();
                            //this->close();
                        }else{
                            QMessageBox::warning(this,"ERROR","Contraseña incorrecta");
                        }
                    }
                }
            }
        }
    }else{
        QMessageBox::warning(this,"Datos incompletos","Faltan campos por llenar");
    }
}

void MainWindow::on_rBut_Coordinador_clicked()
{
    ui->lineUsuario->setText("Coordinador");
    ui->lineUsuario->setEnabled(false);
    ui->lineContrasenia->clear();
}

void MainWindow::on_rBut_Admin_clicked()
{
      ui->lineUsuario->setText("Administrador");
       ui->lineUsuario->setEnabled(false);
       ui->lineContrasenia->clear();
}

void MainWindow::on_rBut_Profesor_clicked()
{
    ui->lineUsuario->clear();
    ui->lineUsuario->setEnabled(true);
    ui->lineContrasenia->clear();

}

void MainWindow::on_rBut_Alumno_clicked()
{
    ui->lineUsuario->clear();
    ui->lineUsuario->setEnabled(true);
    ui->lineContrasenia->clear();
}

void MainWindow::on_Salir_clicked()
{
    QApplication::quit();
}
