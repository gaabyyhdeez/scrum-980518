#-------------------------------------------------
#
# Project created by QtCreator 2018-10-14T16:16:06
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Proyecto_CarrerasFCC
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ventanaprofesor.cpp \
    ventanacoordinador.cpp \
    ventanaadmin.cpp \
    ventanaalumnolcc.cpp \
    ventanaalumnoicc.cpp \
    ventanamodificacontrasenaalm.cpp \
    globals.cpp

HEADERS  += mainwindow.h \
    ventanaprofesor.h \
    ventanacoordinador.h \
    ventanaadmin.h \
    ventanaalumnolcc.h \
    ventanaalumnoicc.h \
    ventanamodificacontrasenaalm.h \
    globals.h

FORMS    += mainwindow.ui \
    ventanaprofesor.ui \
    ventanacoordinador.ui \
    ventanaadmin.ui \
    ventanaalumnolcc.ui \
    ventanaalumnoicc.ui \
    ventanamodificacontrasenaalm.ui

RESOURCES += \
    Im�genes/imagenes.qrc

CONFIG += resources_big

QMAKE_CXXFLAGS += -std=gnu++14
