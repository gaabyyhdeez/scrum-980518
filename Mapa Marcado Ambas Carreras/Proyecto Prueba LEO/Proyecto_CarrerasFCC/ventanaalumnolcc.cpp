#include "ventanaalumnolcc.h"
#include "ui_ventanaalumnolcc.h"

ventanaAlumnoLCC::ventanaAlumnoLCC(QWidget *parent, int matricula) :
    QDialog(parent),
    ui(new Ui::ventanaAlumnoLCC)
{
    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose,true);

    matricula2 = matricula;
    qDebug() << matricula2;

    QSqlQuery datosAlumno;
    datosAlumno.prepare("SELECT matricula, nombre FROM Alumno WHERE matricula="+QString::number(matricula2)+"");
    datosAlumno.exec();
    datosAlumno.next();

    qDebug() << datosAlumno.result();
    ui->label_NombreMatri->setText(datosAlumno.value(0).toString());
    ui->label_NombreAlm->setText(datosAlumno.value(1).toString());

    // Botones ---------------------------------------------
    // Semestre I
    ui->MatematicasElementales->setAccessibleName("74");
    ui->AlgebraSuperior->setAccessibleName("75");
    ui->MetodologiaDeLaProgramacion->setAccessibleName("76");
    ui->FormacionHumanaySocial->setAccessibleName("77");
    ui->LenguaExtranjeraI->setAccessibleName("78");

    // Semestre II
    ui->CalculoDiferencial->setVisible(false);
    ui->CalculoDiferencial->setAccessibleName("79");
    ui->AlgebraLineal->setVisible(false);
    ui->AlgebraLineal->setAccessibleName("80");
    ui->ProgramacionI->setVisible(false);
    ui->ProgramacionI->setAccessibleName("81");
    ui->Dhpc->setVisible(false);
    ui->Dhpc->setAccessibleName("82");
    ui->LenguaExtranjeraII->setVisible(false);
    ui->LenguaExtranjeraII->setAccessibleName("83");

    // Semestre III
    ui->CalculoIntegral->setVisible(false);
    ui->CalculoIntegral->setAccessibleName("84");
    ui->ProgramacionII->setVisible(false);
    ui->ProgramacionII->setAccessibleName("85");
    ui->Ensamblador->setVisible(false);
    ui->Ensamblador->setAccessibleName("86");
    ui->EstructurasDiscretas->setVisible(false);
    ui->EstructurasDiscretas->setAccessibleName("87");
    ui->LenguaExtranjeraIII->setVisible(false);
    ui->LenguaExtranjeraIII->setAccessibleName("88");

    // Semestre IV
    ui->CircuitosElectricos->setVisible(false);
    ui->CircuitosElectricos->setAccessibleName("89");
    ui->EstructuraDeDatos->setVisible(false);
    ui->EstructuraDeDatos->setAccessibleName("90");
    ui->LogicaMatematica->setVisible(false);
    ui->LogicaMatematica->setAccessibleName("91");
    ui->LenguajesFormalesyAutomatas->setVisible(false);
    ui->LenguajesFormalesyAutomatas->setAccessibleName("92");
    ui->LenguaExtranjeraIV->setVisible(false);
    ui->LenguaExtranjeraIV->setAccessibleName("93");

    // Sesmtre V
    ui->ProbabilidadyEstadistica->setVisible(false);
    ui->ProbabilidadyEstadistica->setAccessibleName("94");
    ui->SistemasOperativosI->setVisible(false);
    ui->SistemasOperativosI->setAccessibleName("95");
    ui->AnalisisyDisenoDeAlgoritmos->setVisible(false);
    ui->AnalisisyDisenoDeAlgoritmos->setAccessibleName("96");
    ui->BasesDeDatos->setVisible(false);
    ui->BasesDeDatos->setAccessibleName("97");
    ui->IngenieriaDeSoftware->setVisible(false);
    ui->IngenieriaDeSoftware->setAccessibleName("98");

    // Semestre VI
    ui->FundamentosDeLenguajesDeProgramacion->setVisible(false);
    ui->FundamentosDeLenguajesDeProgramacion->setAccessibleName("99");
    ui->ProgramacionConcurrenteyParalela->setVisible(false);
    ui->ProgramacionConcurrenteyParalela->setAccessibleName("100");
    ui->Graficacion->setVisible(false);
    ui->Graficacion->setAccessibleName("101");
    ui->RedesDeComputadoras->setVisible(false);
    ui->RedesDeComputadoras->setAccessibleName("102");
    ui->AdministracionDeProyectos->setVisible(true);
    ui->AdministracionDeProyectos->setAccessibleName("103");

    // Semestre VII
    ui->CircuitosLogicos->setVisible(false);
    ui->CircuitosLogicos->setAccessibleName("104");
    ui->SistemasOperativosII->setVisible(false);
    ui->SistemasOperativosII->setAccessibleName("105");
    ui->ProgramacionDistribuida->setVisible(false);
    ui->ProgramacionDistribuida->setAccessibleName("106");
    ui->InteligenciaArtificial->setVisible(false);
    ui->InteligenciaArtificial->setAccessibleName("107");
    ui->SeguridadEnRedes->setVisible(false);
    ui->SeguridadEnRedes->setAccessibleName("108");

    // Semestre VIII
    ui->Computabilidad->setVisible(false);
    ui->Computabilidad->setAccessibleName("109");
    ui->RecuperacionDeLaInformacion->setVisible(false);
    ui->RecuperacionDeLaInformacion->setAccessibleName("110");
    ui->ServicioSocial->setVisible(true);
    ui->ServicioSocial->setAccessibleName("111");
    ui->Optativa1->setVisible(true);
    ui->Optativa2->setVisible(true);


    //Semestre IX
    ui->ArquitecturaFuncionalDeComputadoras->setVisible(false);
    ui->ArquitecturaFuncionalDeComputadoras->setAccessibleName("132");
    ui->Optativa1Desit->setVisible(true);
    ui->ProyectoIDI->setVisible(false);
    ui->ProyectoIDI->setAccessibleName("133");
    ui->Optativa3->setVisible(true);

    //Semestre X
    ui->PracticaProfesional->setVisible(true);
    ui->PracticaProfesional->setAccessibleName("135");
    ui->Optativa4->setVisible(true);
    ui->Optativa5->setVisible(true);
    // -----------------------------------------------------


    //Cargar avance -------------------------------------------
    int idmateria;
    QSqlQuery queryAvance;
    queryAvance.prepare("SELECT idMateria FROM avance WHERE matricula = '"+QString::number(matricula2) +"' ");
    queryAvance.exec();

    while(queryAvance.next())
    {
        //Igualar variable id a resultado del query para utilizar switch
        idmateria = queryAvance.value(0).toInt();

        switch(idmateria)
        {
            case 74:
                ui->MatematicasElementales->setChecked(true);
                on_MatematicasElementales_clicked(true);
                qDebug() << "74.Matematicas elementales";
            break;

            case 75:
                ui->AlgebraSuperior->setChecked(true);
                on_AlgebraSuperior_clicked(true);
                qDebug() << "75.Algebra superiror";
            break;

            case 76:
                ui->MetodologiaDeLaProgramacion->setChecked(true);
                on_MetodologiaDeLaProgramacion_clicked(true);
                qDebug() << "76.Metodología de la programación";
            break;

            case 77:
                ui->FormacionHumanaySocial->setChecked(true);
                on_FormacionHumanaySocial_clicked(true);
                qDebug() << "77.Formación Humana y Social";
            break;

            case 78:
                ui->LenguaExtranjeraI->setChecked(true);
                on_LenguaExtranjeraI_clicked(true);
                qDebug() << "78.Lengua extranjera I";
            break;

            case 79:
                ui->CalculoDiferencial->setChecked(true);
                on_CalculoDiferencial_clicked(true);
                qDebug() << "79.CalculoDiferencial";
            break;

            case 80:
                ui->AlgebraLineal->setChecked(true);
                on_AlgebraLineal_clicked(true);
                qDebug() << "80.Algebra lineal";
            break;

            case 81:
                ui->ProgramacionI->setChecked(true);
                on_ProgramacionI_clicked(true);
                qDebug() << "81.Programación I";
            break;

            case 82:
                ui->Dhpc->setChecked(true);
                qDebug() << "82.Dhpc";
            break;

            case 83:
                ui->LenguaExtranjeraII->setChecked(true);
                on_LenguaExtranjeraII_clicked(true);
                qDebug() << "83.LenguaExtranjera II";
            break;

            case 84:
                ui->CalculoIntegral->setChecked(true);
                on_CalculoIntegral_clicked(true);
                qDebug() << "84.Cálculo integral";
            break;

            case 85:
                ui->ProgramacionII->setChecked(true);
                on_ProgramacionII_clicked(true);
                qDebug() << "85.Programación II";
            break;

            case 86:
                ui->Ensamblador->setChecked(true);
                on_Ensamblador_clicked(true);
                qDebug() <<"86.Ensamblador";
            break;

            case 87:
                ui->EstructurasDiscretas->setChecked(true);
                on_EstructurasDiscretas_clicked(true);
                qDebug() <<"87.Estructuras Discretas";
            break;

            case 88:
                ui->LenguaExtranjeraIII->setChecked(true);
                on_LenguaExtranjeraIII_clicked(true);
                qDebug() << "88. Lengua extranjera III";
            break;

            case 89:
                ui->CircuitosElectricos->setChecked(true);
                on_CircuitosElectricos_clicked(true);
                qDebug() << "89.Circuitos electricos";
            break;

            case 90:
                ui->EstructuraDeDatos->setChecked(true);
                on_EstructuraDeDatos_clicked(true);
                qDebug() <<"90.Estructura de datos";
            break;

            case 91:
                ui->LogicaMatematica->setChecked(true);
                on_LogicaMatematica_clicked(true);
                qDebug() <<"91. Logica matematica";
            break;

            case 92:
                ui->LenguajesFormalesyAutomatas->setChecked(true);
                on_LenguajesFormalesyAutomatas_clicked(true);
                qDebug() <<"92. Lenguahes Formales y automatas";
            break;

            case 93:
                ui->LenguaExtranjeraIV->setChecked(true);
                qDebug() <<"93.Lengua Extranjera IV";
            break;

            case 94:
                ui->ProbabilidadyEstadistica->setChecked(true);
                on_ProbabilidadyEstadistica_clicked(true);
                qDebug() <<"94. Probabilidad y estadistica";
            break;

            case 95:
                ui->SistemasOperativosI->setChecked(true);
                on_SistemasOperativosI_clicked(true);
                qDebug() <<"95. Sistemas Operativos I";
            break;

            case 96:
                ui->AnalisisyDisenoDeAlgoritmos->setChecked(true);
                qDebug() <<"96.Analisis y diseño de algoritmos";
            break;

            case 97:
                ui->BasesDeDatos->setChecked(true);
                on_BasesDeDatos_clicked(true);
                qDebug() <<"97.Bases de datos";
            break;

            case 98:
                ui->IngenieriaDeSoftware->setChecked(true);
                qDebug() <<"98.Ingeniería de software";
            break;

            case 99:
                ui->FundamentosDeLenguajesDeProgramacion->setChecked(true);
                qDebug()<<"99.Fundamentos de lenguajes de programacion";
            break;

            case 100:
                ui->ProgramacionConcurrenteyParalela->setChecked(true);
                on_ProgramacionConcurrenteyParalela_clicked(true);
                qDebug()<<"100.Programación concurrete y pararalela";
            break;

            case 101:
                ui->Graficacion->setChecked(true);
                qDebug()<<"101.Graficación";
            break;

            case 102:
                ui->RedesDeComputadoras->setChecked(true);
                on_RedesDeComputadoras_clicked(true);
                qDebug()<<"102.Redes de computadoras";
            break;

            case 103:
                ui->AdministracionDeProyectos->setChecked(true);
                on_AdministracionDeProyectos_clicked(true);
                qDebug()<<"103.Administracion de proyectos";
            break;

            case 104:
                ui->CircuitosLogicos->setChecked(true);
                on_CircuitosLogicos_clicked(true);
                qDebug()<<"104.CircuitosLogicos";
            break;

            case 105:
                ui->SistemasOperativosII->setChecked(true);
                qDebug()<<"105.Sistemas operativos II";
            break;

            case 106:
                ui->ProgramacionDistribuida->setChecked(true);
                qDebug()<<"106.Programación distribuida";
            break;

            case 107:
                ui->InteligenciaArtificial->setChecked(true);
                qDebug()<<"107. Inteligencia artificial";
            break;

            case 108:
                ui->SeguridadEnRedes->setChecked(true);
                qDebug()<<"108.Seguridad en redes";
            break;

            case 109:
                ui->Computabilidad->setChecked(true);
                qDebug()<<"109.Computabilidad";
            break;

            case 110:
                ui->RecuperacionDeLaInformacion->setChecked(true);
                qDebug()<<"110.Recuperación de la información";
            break;

            case 111:
                ui->ServicioSocial->setChecked(true);
                qDebug()<<"111.Servicio social";
            break;

            case 112:
                ui->ServicioSocial->setChecked(true);
                qDebug()<<"112.Servicio social";
            break;

            case 132:
                ui->ArquitecturaFuncionalDeComputadoras->setChecked(true);
                qDebug()<<"132. Arquitectura funcional de computadoras";
            break;

            case 133:
                ui->ProyectoIDI->setChecked(true);
                qDebug()<<"133.ProyectoIDI";
            break;

            case 135:
                ui->PracticaProfesional->setChecked(true);
                qDebug()<<"135.Practica profesional";
            break;

            default:
                qDebug() << "No hay una materia registrada con ese ID";
        }
    }

    //Porcentaje
    ui->labPorcentaje->setText(calculaPorcentaje().append("%"));
}

ventanaAlumnoLCC::~ventanaAlumnoLCC()
{
    delete ui;
}

// SEMESTRES I
void ventanaAlumnoLCC::on_MatematicasElementales_clicked(bool checked)
{
    ui->CalculoDiferencial->setVisible(checked);

    if(!checked)
    {
        ui->CalculoDiferencial->setChecked(checked);

        on_CalculoDiferencial_clicked(checked);
    }
}

void ventanaAlumnoLCC::on_AlgebraSuperior_clicked(bool checked)
{
    ui->AlgebraLineal->setVisible(checked);
    ui->EstructurasDiscretas->setVisible(checked);

    if(!checked)
    {
        ui->AlgebraLineal->setChecked(checked);
        ui->EstructurasDiscretas->setChecked(checked);

        on_AlgebraLineal_clicked(checked);
        on_EstructurasDiscretas_clicked(checked);
    }
}

void ventanaAlumnoLCC::on_MetodologiaDeLaProgramacion_clicked(bool checked)
{
    if(ui->ProgramacionI->isChecked())
        ui->Ensamblador->setVisible(checked);

    ui->ProgramacionI->setVisible(checked);

    if(!checked)
    {
        ui->ProgramacionI->setChecked(checked);
        ui->Ensamblador->setChecked(checked);

        on_Ensamblador_clicked(checked);
        on_ProgramacionI_clicked(checked);
    }
}

void ventanaAlumnoLCC::on_FormacionHumanaySocial_clicked(bool checked)
{
    ui->Dhpc->setVisible(checked);

    if(!checked)
        ui->Dhpc->setChecked(checked);
}

void ventanaAlumnoLCC::on_LenguaExtranjeraI_clicked(bool checked)
{
    ui->LenguaExtranjeraII->setVisible(checked);

    if(!checked)
    {
        ui->LenguaExtranjeraII->setChecked(checked);

        on_LenguaExtranjeraII_clicked(checked);
    }
}


// SEMESTRE II
void ventanaAlumnoLCC::on_CalculoDiferencial_clicked(bool checked)
{
    ui->CalculoIntegral->setVisible(checked);
    ui->CircuitosElectricos->setVisible(checked);

    if(!checked)
    {
        ui->CalculoIntegral->setChecked(checked);
        ui->CircuitosElectricos->setChecked(checked);

        on_CalculoIntegral_clicked(checked);
        on_CircuitosElectricos_clicked(checked);
    }

}

void ventanaAlumnoLCC::on_AlgebraLineal_clicked(bool checked)
{
    ui->Graficacion->setVisible(checked);

    if(!checked)
        ui->Graficacion->setChecked(checked);
}

void ventanaAlumnoLCC::on_ProgramacionI_clicked(bool checked)
{
    if(ui->MetodologiaDeLaProgramacion->isChecked())
        ui->Ensamblador->setVisible(checked);

    ui->ProgramacionII->setVisible(checked);


    if(!checked)
    {
        ui->ProgramacionII->setChecked(checked);
        ui->Ensamblador->setChecked(checked);

        on_ProgramacionII_clicked(checked);
        on_Ensamblador_clicked(checked);
    }
}

void ventanaAlumnoLCC::on_Dhpc_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_LenguaExtranjeraII_clicked(bool checked)
{
    ui->LenguaExtranjeraIII->setVisible(checked);

    if(!checked)
    {
        ui->LenguaExtranjeraIII->setChecked(checked);

        on_LenguaExtranjeraIII_clicked(checked);
    }
}

// SEMESTRE III
void ventanaAlumnoLCC::on_CalculoIntegral_clicked(bool checked)
{
    ui->ProbabilidadyEstadistica->setVisible(checked);

    if(!checked)
    {
        ui->ProbabilidadyEstadistica->setChecked(checked);

        on_ProbabilidadyEstadistica_clicked(checked);
    }
}

void ventanaAlumnoLCC::on_ProgramacionII_clicked(bool checked)
{
    ui->EstructuraDeDatos->setVisible(checked);
    ui->BasesDeDatos->setVisible(checked);
    ui->IngenieriaDeSoftware->setVisible(checked);

    if(!checked)
    {
        ui->EstructuraDeDatos->setChecked(checked);
        ui->BasesDeDatos->setChecked(checked);
        ui->IngenieriaDeSoftware->setChecked(checked);

        on_EstructuraDeDatos_clicked(checked);
        on_BasesDeDatos_clicked(checked);
    }

}

void ventanaAlumnoLCC::on_Ensamblador_clicked(bool checked)
{
    if(ui->EstructuraDeDatos->isChecked())
        ui->SistemasOperativosI->setVisible(checked);

    if(!checked)
    {
        ui->SistemasOperativosI->setChecked(checked);
        on_SistemasOperativosI_clicked(checked);
    }
}

void ventanaAlumnoLCC::on_EstructurasDiscretas_clicked(bool checked)
{
    ui->LogicaMatematica->setVisible(checked);
    ui->LenguajesFormalesyAutomatas->setVisible(checked);

    if(!checked)
    {
        ui->LogicaMatematica->setChecked(checked);
        ui->LenguajesFormalesyAutomatas->setChecked(checked);

        on_LogicaMatematica_clicked(checked);
        on_LenguajesFormalesyAutomatas_clicked(checked);
    }
}

void ventanaAlumnoLCC::on_LenguaExtranjeraIII_clicked(bool checked)
{
    ui->LenguaExtranjeraIV->setVisible(checked);

    if(!checked)
        ui->LenguaExtranjeraIV->setChecked(checked);
}

// SEMESTRE IV
void ventanaAlumnoLCC::on_CircuitosElectricos_clicked(bool checked)
{
    ui->CircuitosLogicos->setVisible(checked);

    if(!checked)
        ui->CircuitosElectricos->setChecked(checked);
}

void ventanaAlumnoLCC::on_EstructuraDeDatos_clicked(bool checked)
{
    if(ui->Ensamblador->isChecked())
        ui->SistemasOperativosI->setVisible(checked);

    ui->AnalisisyDisenoDeAlgoritmos->setVisible(checked);
    ui->ProgramacionConcurrenteyParalela->setVisible(checked);

    if(!checked)
    {
        ui->SistemasOperativosI->setChecked(checked);
        ui->AnalisisyDisenoDeAlgoritmos->setChecked(checked);
        ui->ProgramacionConcurrenteyParalela->setChecked(checked);

        on_SistemasOperativosI_clicked(checked);
        on_ProgramacionConcurrenteyParalela_clicked(checked);
    }
}

void ventanaAlumnoLCC::on_LogicaMatematica_clicked(bool checked)
{
    ui->InteligenciaArtificial->setVisible(checked);

    if(!checked)
        ui->InteligenciaArtificial->setChecked(checked);
}

void ventanaAlumnoLCC::on_LenguajesFormalesyAutomatas_clicked(bool checked)
{
    ui->FundamentosDeLenguajesDeProgramacion->setVisible(checked);
    ui->Computabilidad->setVisible(checked);

    if(!checked)
    {
        ui->FundamentosDeLenguajesDeProgramacion->setChecked(checked);
        ui->Computabilidad->setChecked(checked);
    }
}

void ventanaAlumnoLCC::on_LenguaExtranjeraIV_clicked(bool checked)
{
    //no tiene consecuente
}

// SEMESTRE V
void ventanaAlumnoLCC::on_ProbabilidadyEstadistica_clicked(bool checked)
{
    if(ui->BasesDeDatos->isChecked())
        ui->RecuperacionDeLaInformacion->setVisible(checked);

    ui->RedesDeComputadoras->setVisible(checked);

    if(!checked)
    {
        ui->RedesDeComputadoras->setChecked(checked);
        ui->RecuperacionDeLaInformacion->setChecked(checked);

        on_RedesDeComputadoras_clicked(checked);
    }
}

void ventanaAlumnoLCC::on_SistemasOperativosI_clicked(bool checked)
{
    ui->SistemasOperativosII->setVisible(checked);

    if(!checked)
        ui->SistemasOperativosII->setChecked(checked);
}

void ventanaAlumnoLCC::on_AnalisisyDisenoDeAlgoritmos_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_BasesDeDatos_clicked(bool checked)
{
    if(ui->ProbabilidadyEstadistica->isChecked())
        ui->RecuperacionDeLaInformacion->setVisible(checked);

     if(!checked)
            ui->RecuperacionDeLaInformacion->setChecked(checked);
}

void ventanaAlumnoLCC::on_IngenieriaDeSoftware_clicked(bool checked)
{
    //no tiene consecuente
}


// SEMETRE VI
void ventanaAlumnoLCC::on_FundamentosDeLenguajesDeProgramacion_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_ProgramacionConcurrenteyParalela_clicked(bool checked)
{
    ui->ProgramacionDistribuida->setVisible(checked);

    if(!checked)
        ui->ProgramacionDistribuida->setChecked(checked);
}


void ventanaAlumnoLCC::on_Graficacion_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_RedesDeComputadoras_clicked(bool checked)
{
    ui->SeguridadEnRedes->setVisible(checked);

    if(!checked)
        ui->SeguridadEnRedes->setChecked(checked);
}

void ventanaAlumnoLCC::on_AdministracionDeProyectos_clicked(bool checked)
{
    ui->ProyectoIDI->setVisible(checked);

    if(!checked)
        ui->ProyectoIDI->setChecked(checked);
}

// SEMESTRE VII
void ventanaAlumnoLCC::on_CircuitosLogicos_clicked(bool checked)
{
    ui->ArquitecturaFuncionalDeComputadoras->setVisible(checked);

    if(!checked)
        ui->ArquitecturaFuncionalDeComputadoras->setChecked(checked);
}

void ventanaAlumnoLCC::on_SistemasOperativosII_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_ProgramacionDistribuida_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_InteligenciaArtificial_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_SeguridadEnRedes_clicked(bool checked)
{
    //no tiene consecuente
}

// SEMESTRE VIII
void ventanaAlumnoLCC::on_Computabilidad_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_RecuperacionDeLaInformacion_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_ServicioSocial_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_Optativa1_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_Optativa2_clicked(bool checked)
{
    //no tiene consecuente
}

// SEMETRE IX
void ventanaAlumnoLCC::on_ArquitecturaFuncionalDeComputadoras_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_Optativa1Desit_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_ProyectoIDI_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_Optativa3_clicked(bool checked)
{
    //no tiene consecuente
}

// SEMESTRE X
void ventanaAlumnoLCC::on_PracticaProfesional_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_Optativa4_clicked(bool checked)
{
    //no tiene consecuente
}

void ventanaAlumnoLCC::on_Optativa5_clicked(bool checked)
{
    //No tiene consecuente
}

void ventanaAlumnoLCC::on_buttGuardar_clicked()
{
    QMessageBox mBoxConfirmar(QMessageBox::Question, "Guardar", "¿Desea guardar los cambios?", QMessageBox::Yes | QMessageBox::No);

    mBoxConfirmar.setButtonText(QMessageBox::Yes, "Sí");
    mBoxConfirmar.setButtonText(QMessageBox::No, "No");


    if(mBoxConfirmar.exec() == QMessageBox::Yes)
    {
        QSqlQuery eliminar, guardar;
        QString idMateria;

        eliminar.prepare("DELETE FROM avance WHERE matricula = " + QString::number(matricula2)+ "");
        eliminar.exec();

        qDebug() << eliminar.lastError().text();

        //Semestre I
        if(ui->MatematicasElementales->isChecked())
        {
            idMateria = ui->MatematicasElementales->accessibleName();

            guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" +QString::number(matricula2) + ", "+ idMateria + ")");
            guardar.exec();

            qDebug() << guardar.lastError().text();
        }

        if(ui->AlgebraSuperior->isChecked())
        {
            idMateria = ui->AlgebraSuperior->accessibleName();

            guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" +QString::number(matricula2) + ", "+ idMateria + ")");
            guardar.exec();

            qDebug() << guardar.lastError().text();
        }

        if(ui->MetodologiaDeLaProgramacion->isChecked())
        {
            idMateria = ui->MetodologiaDeLaProgramacion->accessibleName();

            guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" +QString::number(matricula2) + ", "+ idMateria + ")");
            guardar.exec();

            qDebug() << guardar.lastError().text();
        }

        if(ui->FormacionHumanaySocial->isChecked())
        {
            idMateria = ui->FormacionHumanaySocial->accessibleName();

            guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" +QString::number(matricula2) + ", "+ idMateria + ")");
            guardar.exec();

            qDebug() << guardar.lastError().text();
        }

        if(ui->LenguaExtranjeraI->isChecked())
        {
            idMateria = ui->LenguaExtranjeraI->accessibleName();

            guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" +QString::number(matricula2) + ", "+ idMateria + ")");
            guardar.exec();

            qDebug() << guardar.lastError().text();
        }

        //Semestre II
        if(ui->CalculoDiferencial->isChecked())
        {
            idMateria = ui->CalculoDiferencial->accessibleName();

            guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" +QString::number(matricula2) + ", "+ idMateria + ")");
            guardar.exec();

            qDebug() << guardar.lastError().text();
        }

        if(ui->AlgebraLineal->isChecked())
        {
            idMateria = ui->AlgebraLineal->accessibleName();

            guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" +QString::number(matricula2) + ", "+ idMateria + ")");
            guardar.exec();

            qDebug() << guardar.lastError().text();
        }

        if(ui->ProgramacionI->isChecked())
        {
            idMateria = ui->ProgramacionI->accessibleName();

            guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" +QString::number(matricula2) + ", "+ idMateria + ")");
            guardar.exec();

            qDebug() << guardar.lastError().text();
        }

        if(ui->Dhpc->isChecked())
        {
            idMateria = ui->Dhpc->accessibleName();

            guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" +QString::number(matricula2) + ", "+ idMateria + ")");
            guardar.exec();

            qDebug() << guardar.lastError().text();
        }

        if(ui->LenguaExtranjeraII->isChecked())
        {
            idMateria = ui->LenguaExtranjeraII->accessibleName();

            guardar.prepare("INSERT INTO avance(matricula, idMateria) VALUES(" +QString::number(matricula2) + ", "+ idMateria + ")");
            guardar.exec();

            qDebug() << guardar.lastError().text();
        }

        //Semestre III

        if(ui->CalculoIntegral->isChecked())
            {
                idMateria = ui->CalculoIntegral->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->ProgramacionII->isChecked())
            {
                idMateria = ui->ProgramacionII->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->Ensamblador->isChecked())
            {
                idMateria = ui->Ensamblador->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->EstructurasDiscretas->isChecked())
            {
                idMateria = ui->EstructurasDiscretas->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->LenguaExtranjeraIII->isChecked())
            {
                idMateria = ui->LenguaExtranjeraIII->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            //Semestre IV
            if(ui->CircuitosElectricos->isChecked())
            {
                idMateria = ui->CircuitosElectricos->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->EstructuraDeDatos->isChecked())
            {
                idMateria = ui->EstructuraDeDatos->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->LogicaMatematica->isChecked())
            {
                idMateria = ui->LogicaMatematica->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->LenguajesFormalesyAutomatas->isChecked())
            {
                idMateria = ui->LenguajesFormalesyAutomatas->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->LenguaExtranjeraIV->isChecked())
            {
                idMateria = ui->LenguaExtranjeraIV->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            //Semestre V
            if(ui->ProbabilidadyEstadistica->isChecked())
            {
                idMateria = ui->ProbabilidadyEstadistica->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->SistemasOperativosI->isChecked())
            {
                idMateria = ui->SistemasOperativosI->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->AnalisisyDisenoDeAlgoritmos->isChecked())
            {
                idMateria = ui->AnalisisyDisenoDeAlgoritmos->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->BasesDeDatos->isChecked())
            {
                idMateria = ui->BasesDeDatos->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->IngenieriaDeSoftware->isChecked())
            {
                idMateria = ui->IngenieriaDeSoftware->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            //Semestre VI
            if(ui->FundamentosDeLenguajesDeProgramacion->isChecked())
            {
                idMateria = ui->FundamentosDeLenguajesDeProgramacion->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->ProgramacionConcurrenteyParalela->isChecked())
            {
                idMateria = ui->ProgramacionConcurrenteyParalela->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->Graficacion->isChecked())
            {
                idMateria = ui->Graficacion->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->RedesDeComputadoras->isChecked())
            {
                idMateria = ui->RedesDeComputadoras->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->AdministracionDeProyectos->isChecked())
            {
                idMateria = ui->AdministracionDeProyectos->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            //Semestre VII
            if(ui->CircuitosLogicos->isChecked())
            {
                idMateria = ui->SistemasOperativosII->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->SistemasOperativosII->isChecked())
            {
                idMateria = ui->SistemasOperativosII->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->ProgramacionDistribuida->isChecked())
            {
                idMateria = ui->ProgramacionDistribuida->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->InteligenciaArtificial->isChecked())
            {
                idMateria = ui->InteligenciaArtificial->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->SeguridadEnRedes->isChecked())
            {
                idMateria = ui->SeguridadEnRedes->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            //Semestre VIII
            if(ui->Computabilidad->isChecked())
            {
                idMateria = ui->Computabilidad->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->RecuperacionDeLaInformacion->isChecked())
            {
                idMateria = ui->RecuperacionDeLaInformacion->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->ServicioSocial->isChecked())
            {
                idMateria = ui->ServicioSocial->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            //Semestre IX
            if(ui->ArquitecturaFuncionalDeComputadoras->isChecked())
            {
                idMateria = ui->ArquitecturaFuncionalDeComputadoras->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            if(ui->ProyectoIDI->isChecked())
            {
                idMateria = ui->ProyectoIDI->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }


            //Semestre X
            if(ui->PracticaProfesional->isChecked())
            {
                idMateria = ui->PracticaProfesional->accessibleName();

                guardar.prepare("INSERT INTO Avance(matricula, idMateria) VALUES("+QString::number(matricula2)+", "+idMateria+")");
                guardar.exec();

                qDebug() << guardar.lastError().text() << endl;
            }

            //Porcentaje
            ui->labPorcentaje->setText(calculaPorcentaje().append("%"));
    }
}

//Funcion para calcular el porcentaje
QString ventanaAlumnoLCC::calculaPorcentaje()
{
    QSqlQuery queryPorcentaje;

    queryPorcentaje.prepare("SELECT (((SUM(M.creditos))*100)/364) as Av FROM Avance AS A "
                            "INNER JOIN Materia as M ON A.idMateria=M.idMateria "
                            "INNER JOIN Alumno as Alm ON Alm.matricula=A.matricula "
                            "WHERE A.matricula="+QString::number(matricula2)+" GROUP BY A.matricula");

    queryPorcentaje.exec();
    queryPorcentaje.next();
    qDebug() << queryPorcentaje.result() << endl;

    QString porcentaje = queryPorcentaje.value(0).toString();

    return porcentaje;
}
