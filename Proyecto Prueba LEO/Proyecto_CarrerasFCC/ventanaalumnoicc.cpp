#include "ventanaalumnoicc.h"
#include "ui_ventanaalumnoicc.h"
#include <QMessageBox>

ventanaAlumnoICC::ventanaAlumnoICC(QWidget *parent, int matricula) :
    QDialog(parent),
    ui(new Ui::ventanaAlumnoICC)
{   
    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose,true);

    matricula2 = matricula;
    qDebug() << matricula2;

    QSqlQuery datosAlumno;
    datosAlumno.prepare("SELECT matricula, nombre FROM Alumno WHERE matricula="+QString::number(matricula2)+"");
    datosAlumno.exec();
    datosAlumno.next();

    qDebug() << datosAlumno.result();
    ui->label_NombreMatri->setText(datosAlumno.value(0).toString());
    ui->label_NombreAlm->setText(datosAlumno.value(1).toString());

    //primer semestre
    ui->Matematicas->setVisible(true);
    ui->AlgebraSuperior->setVisible(true);
    ui->MetodologiaDeLaProgramacion->setVisible(true);
    ui->LenguaExtranjeraI->setVisible(true);
    ui->FormacionHumanaySocial->setVisible(true);
    //segundo semestre
    ui->CalculoDiferencial->setVisible(false);
    ui->FisicaI->setVisible(false);
    ui->AlgebraLinealConElementosDeGeometriaAnalitica->setVisible(false);
    ui->ProgramacionI->setVisible(false);
    ui->LenguaExtranjeraII->setVisible(false);
    ui->Dhpc->setVisible(true);
    //tercer semestre
    ui->CalculoIntegral->setVisible(false);
    ui->FisicaII->setVisible(false);
    ui->MatematicasDiscretas->setVisible(false);
    ui->ProgramacionII->setVisible(false);
    ui->Ensamblador->setVisible(false);
    ui->LenguaExtranjeraIII->setVisible(false);
    //cuarto semestre
    ui->EcuacionesDiferenciales->setVisible(false);
    ui->CircuitosElectricos->setVisible(false);
    ui->Graficacion->setVisible(false);
    ui->EstructurasDeDatos->setVisible(false);
    ui->LenguaExtranjeraIV->setVisible(false);
    //quinto semestre
    ui->ProbabilidadyEstadistica->setVisible(false);
    ui->CircuitosElectronicos->setVisible(false);
    ui->SistemasOperativosI->setVisible(false);
    ui->AnalisisyDiseoDeAlgoritmos->setVisible(false);
    ui->IngenieriaDeSoftware->setVisible(false);
    //sexto semestre
    ui->ModeloDeRedes->setVisible(false);
    ui->DisenoDigital->setVisible(false);
    ui->BaseDeDatosParaIngenieria->setVisible(false);
    ui->SistemasOperativosII->setVisible(false);
    ui->AdministracionDeProyectos->setVisible(true);
    //septimo semestre
    ui->RedesInalambricas->setVisible(false);
    ui->MineriaDeDatos->setVisible(false);
    ui->ArquitecturaDeComputo->setVisible(false);
    ui->ProgramacionConcurrenteyParalela->setVisible(false);
    ui->DesarrolloDeAplicacionesWeb->setVisible(false);
    //octavo semestre
    ui->TeoriaDeControl->setVisible(false);
    ui->AdministracionDeRedes->setVisible(false);
    ui->TecnicasDeInteligenciaArtificial->setVisible(false);
    ui->ProgramacionDistribuidaAplicada->setVisible(false);
    ui->DesarrolloDeAplicacionesMoviles->setVisible(false);
    ui->OptativaI->setVisible(false);
    //noveno semestre
    ui->IntercomunicacionySeguridadEnRedes->setVisible(false);
    ui->SistemasEmpotrados->setVisible(false);
    ui->ServicioSocial->setVisible(false);
    ui->OptativaII->setVisible(false);
    //decimo semestre
    ui->OptativaDesit->setVisible(false);
    ui->PracticaProfesional->setVisible(false);
    ui->ProyectoIDI->setVisible(false);
}

ventanaAlumnoICC::~ventanaAlumnoICC()
{
    delete ui;
}


void ventanaAlumnoICC::on_buttRegresar_clicked()
{

}

void ventanaAlumnoICC::on_Matematicas_clicked(bool checked)
{
    ui->CalculoDiferencial->setVisible(true);
    ui->FisicaI->setVisible(true);
}

void ventanaAlumnoICC::on_AlgebraSuperior_clicked(bool checked)
{
    ui->AlgebraLinealConElementosDeGeometriaAnalitica->setVisible(true);
}

void ventanaAlumnoICC::on_MetodologiaDeLaProgramacion_clicked(bool checked)
{
    ui->ProgramacionI->setVisible(true);
}

void ventanaAlumnoICC::on_LenguaExtranjeraI_clicked(bool checked)
{
    ui->LenguaExtranjeraII->setVisible(true);
}

void ventanaAlumnoICC::on_FormacionHumanaySocial_clicked(bool checked)
{

}

//Segundo Semestre

void ventanaAlumnoICC::on_CalculoDiferencial_clicked(bool checked)
{
    if(!(ui->Matematicas->isChecked()))
        ui->CalculoDiferencial->setVisible(false);
    else
        ui->CalculoIntegral->setVisible(true);
}

void ventanaAlumnoICC::on_FisicaI_clicked(bool checked)
{
    if(!(ui->Matematicas->isChecked()))
        ui->FisicaI->setVisible(false);
    else
        ui->FisicaII->setVisible(true);
}

void ventanaAlumnoICC::on_AlgebraLinealConElementosDeGeometriaAnalitica_clicked(bool checked)
{
    if(!(ui->AlgebraSuperior->isChecked()))
        ui->AlgebraLinealConElementosDeGeometriaAnalitica->setVisible(false);
    else
    {
        ui->MatematicasDiscretas->setVisible(true);

        if(ui->ProgramacionII->isChecked())
            ui->Graficacion->setVisible(true);
    }
}

void ventanaAlumnoICC::on_ProgramacionI_clicked(bool checked)
{
    if(!(ui->MetodologiaDeLaProgramacion->isChecked()))
        ui->ProgramacionI->setVisible(false);

    else
    {
        ui->ProgramacionII->setVisible(true);
        ui->Ensamblador->setVisible(true);
    }
}

void ventanaAlumnoICC::on_LenguaExtranjeraII_clicked(bool checked)
{
    if(!(ui->LenguaExtranjeraI->isChecked()))
        ui->LenguaExtranjeraII->setVisible(false);
    else
        ui->LenguaExtranjeraIII->setVisible(true);
}

void ventanaAlumnoICC::on_Dhpc_clicked(bool checked)
{

}

//Tercer Semestre

void ventanaAlumnoICC::on_CalculoIntegral_clicked(bool checked)
{
    if(!(ui->CalculoDiferencial->isChecked()))
        ui->CalculoIntegral->setVisible(false);
    else
    {
        ui->EcuacionesDiferenciales->setVisible(true);
        ui->ProbabilidadyEstadistica->setVisible(true);
    }
}

void ventanaAlumnoICC::on_FisicaII_clicked(bool checked)
{
    if(!(ui->FisicaI->isChecked()))
        ui->FisicaII->setVisible(false);
    else
        ui->CircuitosElectricos->setVisible(true);
}

void ventanaAlumnoICC::on_MatematicasDiscretas_clicked(bool checked)
{
    if(!(ui->AlgebraLinealConElementosDeGeometriaAnalitica->isChecked()))
        ui->MatematicasDiscretas->setVisible(false);
}

void ventanaAlumnoICC::on_ProgramacionII_clicked(bool checked)
{
    if(!(ui->ProgramacionI->isChecked()))
        ui->ProgramacionII->setVisible(false);
    else
    {
        ui->EstructurasDeDatos->setVisible(true);
        ui->IngenieriaDeSoftware->setVisible(true);

        if(ui->AlgebraLinealConElementosDeGeometriaAnalitica->isChecked())
            ui->Graficacion->setVisible(true);
    }
}

void ventanaAlumnoICC::on_Ensamblador_clicked(bool checked)
{
    if(!(ui->ProgramacionI->isChecked()))
        ui->Ensamblador->setVisible(false);
    else
    {
        if(ui->EstructurasDeDatos->isChecked())
            ui->SistemasOperativosI->setVisible(true);
    }
}

void ventanaAlumnoICC::on_LenguaExtranjeraIII_clicked(bool checked)
{
    if(!(ui->LenguaExtranjeraII->isChecked()))
        ui->LenguaExtranjeraIII->setVisible(false);
    else
        ui->LenguaExtranjeraIV->setVisible(true);
}

//Cuarto Semestre

void ventanaAlumnoICC::on_EcuacionesDiferenciales_clicked(bool checked)
{
    if(!(ui->CalculoIntegral->isChecked()))
        ui->EcuacionesDiferenciales->setVisible(false);
    else
        ui->TeoriaDeControl->setVisible(true);
}

void ventanaAlumnoICC::on_CircuitosElectricos_clicked(bool checked)
{
    if(!(ui->FisicaII->isChecked()))
        ui->CircuitosElectricos->setVisible(false);
    else
        ui->CircuitosElectronicos->setVisible(true);
}

void ventanaAlumnoICC::on_Graficacion_clicked(bool checked)
{
    if((!(ui->AlgebraLinealConElementosDeGeometriaAnalitica->isChecked()))||(!(ui->ProgramacionII->isChecked())))
        ui->Graficacion->setVisible(false);
}

void ventanaAlumnoICC::on_EstructurasDeDatos_clicked(bool checked)
{
    if(!(ui->ProgramacionII->isChecked()))
        ui->EstructurasDeDatos->setVisible(false);
    else
    {
        ui->AnalisisyDiseoDeAlgoritmos->setVisible(true);
        ui->BaseDeDatosParaIngenieria->setVisible(true);

        if(ui->Ensamblador->isChecked())
            ui->SistemasOperativosI->setVisible(true);
    }
}

void ventanaAlumnoICC::on_LenguaExtranjeraIV_clicked(bool checked)
{
    if(!(ui->LenguaExtranjeraIII->isChecked()))
        ui->LenguaExtranjeraIV->setVisible(false);
}

//Quinto Semestre

void ventanaAlumnoICC::on_ProbabilidadyEstadistica_clicked(bool checked)
{
    if(!(ui->CalculoIntegral->isChecked()))
        ui->ProbabilidadyEstadistica->setVisible(false);
    else
        ui->MineriaDeDatos->setVisible(true);
}

void ventanaAlumnoICC::on_CircuitosElectronicos_clicked(bool checked)
{
    if(!(ui->CircuitosElectricos->isChecked()))
        ui->CircuitosElectronicos->setVisible(false);
    else
        ui->DisenoDigital->setVisible(true);
}

void ventanaAlumnoICC::on_SistemasOperativosI_clicked(bool checked)
{
    if((!(ui->EstructurasDeDatos->isChecked()))||(!(ui->Ensamblador->isChecked())))
        ui->SistemasOperativosI->setVisible(false);
    else
        ui->SistemasOperativosII->setVisible(true);
}

void ventanaAlumnoICC::on_AnalisisyDiseoDeAlgoritmos_clicked(bool checked)
{
    if(!(ui->EstructurasDeDatos->isChecked()))
        ui->AnalisisyDiseoDeAlgoritmos->setVisible(false);
    else
        ui->ProgramacionConcurrenteyParalela->setVisible(true);
}

void ventanaAlumnoICC::on_IngenieriaDeSoftware_clicked(bool checked)
{
    if(!(ui->ProgramacionII->isChecked()))
        ui->IngenieriaDeSoftware->setVisible(false);
}

//Sexto Semestre

void ventanaAlumnoICC::on_ModeloDeRedes_clicked(bool checked)
{
    if(!(ui->ProbabilidadyEstadistica->isChecked()))
        ui->ModeloDeRedes->setVisible(false);
    else
        ui->RedesInalambricas->setVisible(true);
}

void ventanaAlumnoICC::on_DisenoDigital_clicked(bool checked)
{
    if(!(ui->CircuitosElectronicos->isChecked()))
        ui->DisenoDigital->setVisible(false);
    else
        ui->ArquitecturaDeComputo->setVisible(true);
}

void ventanaAlumnoICC::on_BaseDeDatosParaIngenieria_clicked(bool checked)
{
    if(!(ui->EstructurasDeDatos->isChecked()))
        ui->BaseDeDatosParaIngenieria->setVisible(false);
}

void ventanaAlumnoICC::on_SistemasOperativosII_clicked(bool checked)
{
    if(!(ui->SistemasOperativosI->isChecked()))
        ui->SistemasOperativosII->setVisible(false);
}

void ventanaAlumnoICC::on_AdministracionDeProyectos_clicked(bool checked)
{
    ui->DesarrolloDeAplicacionesWeb->setVisible(true);

    if(ui->OptativaII->isChecked())
        ui->ProyectoIDI->setVisible(true);
}
