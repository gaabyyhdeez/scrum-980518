#ifndef VENTANAMODIFICACONTRASENAALM_H
#define VENTANAMODIFICACONTRASENAALM_H

#include <QDialog>
#include "globals.h"

namespace Ui {
class ventanaModificaContrasenaAlm;
}

class ventanaModificaContrasenaAlm : public QDialog
{
    Q_OBJECT

public:
    explicit ventanaModificaContrasenaAlm(QWidget *parent = 0, int matricula=0);
    ~ventanaModificaContrasenaAlm();

private slots:
    void on_buttGuardar_clicked();

private:
    Ui::ventanaModificaContrasenaAlm *ui;
    int matricula2=0;
};

#endif // VENTANAMODIFICACONTRASENAALM_H
