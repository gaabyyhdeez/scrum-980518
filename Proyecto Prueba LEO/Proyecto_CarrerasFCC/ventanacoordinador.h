#ifndef VENTANACOORDINADOR_H
#define VENTANACOORDINADOR_H

#include <QDialog>
#include "globals.h"

namespace Ui {
class ventanaCoordinador;
}

class ventanaCoordinador : public QDialog
{
    Q_OBJECT

public:
    explicit ventanaCoordinador(QWidget *parent = 0);
    ~ventanaCoordinador();

    //Mostrar tabla
    QSqlTableModel *miModeloTabla;

    //Autocompletado
    QCompleter * StringCompleter;
    QCompleter * StringCompleterGrupo;

private slots:
    void on_buttBuscarGrupo_clicked();

    void on_buttBuscar2_clicked();

    void on_lineIDGrupo_textEdited(const QString &arg1);

    void on_lineIDProfesor_textEdited(const QString &arg1);

    void on_pushButton_clicked();

    void on_buttAsignar_clicked();

private:
    Ui::ventanaCoordinador *ui;
};

#endif // VENTANACOORDINADOR_H
