/********************************************************************************
** Form generated from reading UI file 'ventanaalumnolcc.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VENTANAALUMNOLCC_H
#define UI_VENTANAALUMNOLCC_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ventanaAlumnoLCC
{
public:
    QLabel *label;
    QLabel *label_2;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_NombreMatri;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_NombreAlm;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QPushButton *buttRegresar;
    QLabel *AUXIMA;
    QPushButton *MatematicasElementales;
    QPushButton *CalculoDiferencial;
    QPushButton *CalculoIntegral;
    QPushButton *ProbabilidadyEstadistica;
    QPushButton *LenguaExtranjeraI;
    QPushButton *LenguaExtranjeraII;
    QPushButton *LenguaExtranjeraIII;
    QPushButton *LenguaExtranjeraIV;
    QPushButton *FormacionHumanaySocial;
    QPushButton *Dhpc;
    QPushButton *IngenieriaDeSoftware;
    QPushButton *AdministracionDeProyectos;
    QPushButton *ProyectoIDI;
    QPushButton *PracticaProfesional;
    QPushButton *Optativa1Desit;
    QPushButton *SeguridadEnRedes;
    QPushButton *RedesDeComputadoras;
    QPushButton *BasesDeDatos;
    QPushButton *ServicioSocial;
    QPushButton *InteligenciaArtificial;
    QPushButton *Graficacion;
    QPushButton *AnalisisyDisenoDeAlgoritmos;
    QPushButton *LenguajesFormalesyAutomatas;
    QPushButton *EstructurasDiscretas;
    QPushButton *MetodologiaDeLaProgramacion;
    QPushButton *ProgramacionI;
    QPushButton *Ensamblador;
    QPushButton *LogicaMatematica;
    QPushButton *ProgramacionConcurrenteyParalela;
    QPushButton *RecuperacionDeLaInformacion;
    QPushButton *ProgramacionDistribuida;
    QPushButton *ArquitecturaFuncionalDeComputadoras;
    QPushButton *Computabilidad;
    QPushButton *SistemasOperativosII;
    QPushButton *FundamentosDeLenguajesDeProgramacion;
    QPushButton *SistemasOperativosI;
    QPushButton *EstructuraDeDatos;
    QPushButton *ProgramacionII;
    QPushButton *Optativa2;
    QPushButton *Optativa3;
    QPushButton *Optativa5;
    QPushButton *Optativa4;
    QPushButton *Optativa1;
    QPushButton *CircuitosLogicos;
    QPushButton *CircuitosElectricos;
    QPushButton *AlgebraLineal;
    QPushButton *AlgebraSuperior;

    void setupUi(QDialog *ventanaAlumnoLCC)
    {
        if (ventanaAlumnoLCC->objectName().isEmpty())
            ventanaAlumnoLCC->setObjectName(QStringLiteral("ventanaAlumnoLCC"));
        ventanaAlumnoLCC->resize(1366, 748);
        ventanaAlumnoLCC->setStyleSheet(QLatin1String("QDialog#ventanaAlumnoLCC\n"
"{\n"
"                background-color: rgb(255, 255, 255);\n"
"}\n"
"\n"
" QPushButton:pressed\n"
"{\n"
"     background-color: rgb(167, 191, 255);\n"
" }\n"
"QPushButton\n"
"\n"
"{\n"
"        border-width: 1.5px;\n"
"        border-color: rgb(82, 150, 170);\n"
"        border-radius:5px;\n"
"        padding: 0 8px;\n"
"\n"
"/*background-color: rgb(255, 255, 255);*/\n"
"        font: 10pt \"Yu Gothic\";\n"
"\n"
"        color:rgb(82, 150, 170)\n"
"}"));
        label = new QLabel(ventanaAlumnoLCC);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(-20, 30, 981, 671));
        label->setPixmap(QPixmap(QString::fromUtf8(":/Mapa_Carrera_LCC-001.jpg")));
        label->setScaledContents(true);
        label_2 = new QLabel(ventanaAlumnoLCC);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(1070, 50, 181, 181));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/1200px-Escudobuappositivo2.png")));
        label_2->setScaledContents(true);
        pushButton = new QPushButton(ventanaAlumnoLCC);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(1080, 540, 151, 41));
        QFont font;
        font.setFamily(QStringLiteral("Yu Gothic"));
        font.setPointSize(10);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        pushButton->setFont(font);
        pushButton->setStyleSheet(QLatin1String("background-color: rgb(0, 59, 92);\n"
"border-radius: 10px;\n"
"color: white;"));
        pushButton_2 = new QPushButton(ventanaAlumnoLCC);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(1080, 610, 151, 41));
        pushButton_2->setFont(font);
        pushButton_2->setStyleSheet(QLatin1String("background-color: rgb(0, 59, 92);\n"
"border-radius: 10px;\n"
"color: white;"));
        gridLayoutWidget = new QWidget(ventanaAlumnoLCC);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(980, 250, 351, 251));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_NombreMatri = new QLabel(gridLayoutWidget);
        label_NombreMatri->setObjectName(QStringLiteral("label_NombreMatri"));
        QFont font1;
        font1.setFamily(QStringLiteral("Segoe UI"));
        font1.setPointSize(13);
        font1.setBold(false);
        font1.setItalic(false);
        font1.setWeight(50);
        label_NombreMatri->setFont(font1);
        label_NombreMatri->setStyleSheet(QStringLiteral(""));
        label_NombreMatri->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_NombreMatri, 1, 0, 1, 1);

        label_4 = new QLabel(gridLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        QFont font2;
        font2.setFamily(QStringLiteral("Segoe UI"));
        font2.setPointSize(11);
        font2.setBold(true);
        font2.setItalic(false);
        font2.setWeight(75);
        label_4->setFont(font2);
        label_4->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        gridLayout->addWidget(label_4, 0, 0, 1, 1);

        label_5 = new QLabel(gridLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        QFont font3;
        font3.setFamily(QStringLiteral("Segoe UI"));
        font3.setPointSize(11);
        font3.setBold(true);
        font3.setWeight(75);
        label_5->setFont(font3);
        label_5->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        gridLayout->addWidget(label_5, 2, 0, 1, 1);

        label_NombreAlm = new QLabel(gridLayoutWidget);
        label_NombreAlm->setObjectName(QStringLiteral("label_NombreAlm"));
        QFont font4;
        font4.setFamily(QStringLiteral("Segoe UI"));
        font4.setPointSize(13);
        font4.setBold(false);
        font4.setWeight(50);
        label_NombreAlm->setFont(font4);
        label_NombreAlm->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_NombreAlm, 3, 0, 1, 1);

        label_6 = new QLabel(gridLayoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        QFont font5;
        font5.setPointSize(11);
        font5.setBold(true);
        font5.setWeight(75);
        label_6->setFont(font5);
        label_6->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        gridLayout->addWidget(label_6, 4, 0, 1, 1);

        label_7 = new QLabel(gridLayoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setFont(font4);
        label_7->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_7, 5, 0, 1, 1);

        label_8 = new QLabel(ventanaAlumnoLCC);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(970, 20, 371, 671));
        label_8->setStyleSheet(QLatin1String("background-color: rgb(235, 235, 235);\n"
"border-radius: 10px;\n"
"color: white;alternate-background-color: rgb(222, 222, 222);"));
        label_9 = new QLabel(ventanaAlumnoLCC);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(970, 20, 371, 671));
        label_9->setStyleSheet(QLatin1String("border-radius: 10px;\n"
"border: 2px solid #003b5c;"));
        buttRegresar = new QPushButton(ventanaAlumnoLCC);
        buttRegresar->setObjectName(QStringLiteral("buttRegresar"));
        buttRegresar->setGeometry(QRect(10, 10, 41, 23));
        buttRegresar->setStyleSheet(QLatin1String("background-color: none;\n"
"border-image:url(:/west-arrow (1).png);"));
        AUXIMA = new QLabel(ventanaAlumnoLCC);
        AUXIMA->setObjectName(QStringLiteral("AUXIMA"));
        AUXIMA->setGeometry(QRect(-20, 30, 981, 671));
        AUXIMA->setPixmap(QPixmap(QString::fromUtf8("Im\303\241genes/Mapa_Carrera_LCC-001.jpg")));
        AUXIMA->setScaledContents(true);
        MatematicasElementales = new QPushButton(ventanaAlumnoLCC);
        MatematicasElementales->setObjectName(QStringLiteral("MatematicasElementales"));
        MatematicasElementales->setGeometry(QRect(140, 150, 61, 37));
        MatematicasElementales->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        QIcon icon;
        icon.addFile(QStringLiteral("../marcado/transparente.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon.addFile(QStringLiteral("../marcado/PALOMA.png"), QSize(), QIcon::Normal, QIcon::On);
        icon.addFile(QStringLiteral("../marcado/transparente.png"), QSize(), QIcon::Disabled, QIcon::Off);
        icon.addFile(QStringLiteral("../marcado/transparente.png"), QSize(), QIcon::Active, QIcon::Off);
        MatematicasElementales->setIcon(icon);
        MatematicasElementales->setIconSize(QSize(61, 37));
        MatematicasElementales->setCheckable(true);
        MatematicasElementales->setChecked(false);
        CalculoDiferencial = new QPushButton(ventanaAlumnoLCC);
        CalculoDiferencial->setObjectName(QStringLiteral("CalculoDiferencial"));
        CalculoDiferencial->setGeometry(QRect(220, 150, 61, 37));
        CalculoDiferencial->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        CalculoDiferencial->setIcon(icon);
        CalculoDiferencial->setIconSize(QSize(61, 37));
        CalculoDiferencial->setCheckable(true);
        CalculoDiferencial->setChecked(false);
        CalculoIntegral = new QPushButton(ventanaAlumnoLCC);
        CalculoIntegral->setObjectName(QStringLiteral("CalculoIntegral"));
        CalculoIntegral->setGeometry(QRect(300, 150, 61, 37));
        CalculoIntegral->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        CalculoIntegral->setIcon(icon);
        CalculoIntegral->setIconSize(QSize(61, 37));
        CalculoIntegral->setCheckable(true);
        CalculoIntegral->setChecked(false);
        ProbabilidadyEstadistica = new QPushButton(ventanaAlumnoLCC);
        ProbabilidadyEstadistica->setObjectName(QStringLiteral("ProbabilidadyEstadistica"));
        ProbabilidadyEstadistica->setGeometry(QRect(470, 150, 61, 37));
        ProbabilidadyEstadistica->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        ProbabilidadyEstadistica->setIcon(icon);
        ProbabilidadyEstadistica->setIconSize(QSize(61, 37));
        ProbabilidadyEstadistica->setCheckable(true);
        ProbabilidadyEstadistica->setChecked(false);
        LenguaExtranjeraI = new QPushButton(ventanaAlumnoLCC);
        LenguaExtranjeraI->setObjectName(QStringLiteral("LenguaExtranjeraI"));
        LenguaExtranjeraI->setGeometry(QRect(140, 640, 61, 37));
        LenguaExtranjeraI->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        LenguaExtranjeraI->setIcon(icon);
        LenguaExtranjeraI->setIconSize(QSize(61, 37));
        LenguaExtranjeraI->setCheckable(true);
        LenguaExtranjeraI->setChecked(false);
        LenguaExtranjeraII = new QPushButton(ventanaAlumnoLCC);
        LenguaExtranjeraII->setObjectName(QStringLiteral("LenguaExtranjeraII"));
        LenguaExtranjeraII->setGeometry(QRect(220, 640, 61, 37));
        LenguaExtranjeraII->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        LenguaExtranjeraII->setIcon(icon);
        LenguaExtranjeraII->setIconSize(QSize(61, 37));
        LenguaExtranjeraII->setCheckable(true);
        LenguaExtranjeraII->setChecked(false);
        LenguaExtranjeraIII = new QPushButton(ventanaAlumnoLCC);
        LenguaExtranjeraIII->setObjectName(QStringLiteral("LenguaExtranjeraIII"));
        LenguaExtranjeraIII->setGeometry(QRect(300, 640, 61, 37));
        LenguaExtranjeraIII->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        LenguaExtranjeraIII->setIcon(icon);
        LenguaExtranjeraIII->setIconSize(QSize(61, 37));
        LenguaExtranjeraIII->setCheckable(true);
        LenguaExtranjeraIII->setChecked(false);
        LenguaExtranjeraIV = new QPushButton(ventanaAlumnoLCC);
        LenguaExtranjeraIV->setObjectName(QStringLiteral("LenguaExtranjeraIV"));
        LenguaExtranjeraIV->setGeometry(QRect(380, 640, 61, 37));
        LenguaExtranjeraIV->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        LenguaExtranjeraIV->setIcon(icon);
        LenguaExtranjeraIV->setIconSize(QSize(61, 37));
        LenguaExtranjeraIV->setCheckable(true);
        LenguaExtranjeraIV->setChecked(false);
        FormacionHumanaySocial = new QPushButton(ventanaAlumnoLCC);
        FormacionHumanaySocial->setObjectName(QStringLiteral("FormacionHumanaySocial"));
        FormacionHumanaySocial->setGeometry(QRect(140, 580, 61, 37));
        FormacionHumanaySocial->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        FormacionHumanaySocial->setIcon(icon);
        FormacionHumanaySocial->setIconSize(QSize(61, 37));
        FormacionHumanaySocial->setCheckable(true);
        FormacionHumanaySocial->setChecked(false);
        Dhpc = new QPushButton(ventanaAlumnoLCC);
        Dhpc->setObjectName(QStringLiteral("Dhpc"));
        Dhpc->setGeometry(QRect(220, 580, 61, 37));
        Dhpc->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        Dhpc->setIcon(icon);
        Dhpc->setIconSize(QSize(61, 37));
        Dhpc->setCheckable(true);
        Dhpc->setChecked(false);
        IngenieriaDeSoftware = new QPushButton(ventanaAlumnoLCC);
        IngenieriaDeSoftware->setObjectName(QStringLiteral("IngenieriaDeSoftware"));
        IngenieriaDeSoftware->setGeometry(QRect(470, 570, 61, 37));
        IngenieriaDeSoftware->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        IngenieriaDeSoftware->setIcon(icon);
        IngenieriaDeSoftware->setIconSize(QSize(61, 37));
        IngenieriaDeSoftware->setCheckable(true);
        IngenieriaDeSoftware->setChecked(false);
        AdministracionDeProyectos = new QPushButton(ventanaAlumnoLCC);
        AdministracionDeProyectos->setObjectName(QStringLiteral("AdministracionDeProyectos"));
        AdministracionDeProyectos->setGeometry(QRect(550, 570, 61, 37));
        AdministracionDeProyectos->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        AdministracionDeProyectos->setIcon(icon);
        AdministracionDeProyectos->setIconSize(QSize(61, 37));
        AdministracionDeProyectos->setCheckable(true);
        AdministracionDeProyectos->setChecked(false);
        ProyectoIDI = new QPushButton(ventanaAlumnoLCC);
        ProyectoIDI->setObjectName(QStringLiteral("ProyectoIDI"));
        ProyectoIDI->setGeometry(QRect(790, 570, 61, 37));
        ProyectoIDI->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        ProyectoIDI->setIcon(icon);
        ProyectoIDI->setIconSize(QSize(61, 37));
        ProyectoIDI->setCheckable(true);
        ProyectoIDI->setChecked(false);
        PracticaProfesional = new QPushButton(ventanaAlumnoLCC);
        PracticaProfesional->setObjectName(QStringLiteral("PracticaProfesional"));
        PracticaProfesional->setGeometry(QRect(870, 500, 61, 37));
        PracticaProfesional->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        PracticaProfesional->setIcon(icon);
        PracticaProfesional->setIconSize(QSize(61, 37));
        PracticaProfesional->setCheckable(true);
        PracticaProfesional->setChecked(false);
        Optativa1Desit = new QPushButton(ventanaAlumnoLCC);
        Optativa1Desit->setObjectName(QStringLiteral("Optativa1Desit"));
        Optativa1Desit->setGeometry(QRect(790, 500, 61, 37));
        Optativa1Desit->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        Optativa1Desit->setIcon(icon);
        Optativa1Desit->setIconSize(QSize(61, 37));
        Optativa1Desit->setCheckable(true);
        Optativa1Desit->setChecked(false);
        SeguridadEnRedes = new QPushButton(ventanaAlumnoLCC);
        SeguridadEnRedes->setObjectName(QStringLiteral("SeguridadEnRedes"));
        SeguridadEnRedes->setGeometry(QRect(640, 500, 61, 37));
        SeguridadEnRedes->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        SeguridadEnRedes->setIcon(icon);
        SeguridadEnRedes->setIconSize(QSize(61, 37));
        SeguridadEnRedes->setCheckable(true);
        SeguridadEnRedes->setChecked(false);
        RedesDeComputadoras = new QPushButton(ventanaAlumnoLCC);
        RedesDeComputadoras->setObjectName(QStringLiteral("RedesDeComputadoras"));
        RedesDeComputadoras->setGeometry(QRect(550, 500, 61, 37));
        RedesDeComputadoras->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        RedesDeComputadoras->setIcon(icon);
        RedesDeComputadoras->setIconSize(QSize(61, 37));
        RedesDeComputadoras->setCheckable(true);
        RedesDeComputadoras->setChecked(false);
        BasesDeDatos = new QPushButton(ventanaAlumnoLCC);
        BasesDeDatos->setObjectName(QStringLiteral("BasesDeDatos"));
        BasesDeDatos->setGeometry(QRect(460, 500, 61, 37));
        BasesDeDatos->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        BasesDeDatos->setIcon(icon);
        BasesDeDatos->setIconSize(QSize(61, 37));
        BasesDeDatos->setCheckable(true);
        BasesDeDatos->setChecked(false);
        ServicioSocial = new QPushButton(ventanaAlumnoLCC);
        ServicioSocial->setObjectName(QStringLiteral("ServicioSocial"));
        ServicioSocial->setGeometry(QRect(710, 440, 61, 37));
        ServicioSocial->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        ServicioSocial->setIcon(icon);
        ServicioSocial->setIconSize(QSize(61, 37));
        ServicioSocial->setCheckable(true);
        ServicioSocial->setChecked(false);
        InteligenciaArtificial = new QPushButton(ventanaAlumnoLCC);
        InteligenciaArtificial->setObjectName(QStringLiteral("InteligenciaArtificial"));
        InteligenciaArtificial->setGeometry(QRect(630, 440, 61, 37));
        InteligenciaArtificial->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        InteligenciaArtificial->setIcon(icon);
        InteligenciaArtificial->setIconSize(QSize(61, 37));
        InteligenciaArtificial->setCheckable(true);
        InteligenciaArtificial->setChecked(false);
        Graficacion = new QPushButton(ventanaAlumnoLCC);
        Graficacion->setObjectName(QStringLiteral("Graficacion"));
        Graficacion->setGeometry(QRect(550, 440, 61, 37));
        Graficacion->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        Graficacion->setIcon(icon);
        Graficacion->setIconSize(QSize(61, 37));
        Graficacion->setCheckable(true);
        Graficacion->setChecked(false);
        AnalisisyDisenoDeAlgoritmos = new QPushButton(ventanaAlumnoLCC);
        AnalisisyDisenoDeAlgoritmos->setObjectName(QStringLiteral("AnalisisyDisenoDeAlgoritmos"));
        AnalisisyDisenoDeAlgoritmos->setGeometry(QRect(470, 440, 61, 37));
        AnalisisyDisenoDeAlgoritmos->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        AnalisisyDisenoDeAlgoritmos->setIcon(icon);
        AnalisisyDisenoDeAlgoritmos->setIconSize(QSize(61, 37));
        AnalisisyDisenoDeAlgoritmos->setCheckable(true);
        AnalisisyDisenoDeAlgoritmos->setChecked(false);
        LenguajesFormalesyAutomatas = new QPushButton(ventanaAlumnoLCC);
        LenguajesFormalesyAutomatas->setObjectName(QStringLiteral("LenguajesFormalesyAutomatas"));
        LenguajesFormalesyAutomatas->setGeometry(QRect(390, 440, 61, 37));
        LenguajesFormalesyAutomatas->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        LenguajesFormalesyAutomatas->setIcon(icon);
        LenguajesFormalesyAutomatas->setIconSize(QSize(61, 37));
        LenguajesFormalesyAutomatas->setCheckable(true);
        LenguajesFormalesyAutomatas->setChecked(false);
        EstructurasDiscretas = new QPushButton(ventanaAlumnoLCC);
        EstructurasDiscretas->setObjectName(QStringLiteral("EstructurasDiscretas"));
        EstructurasDiscretas->setGeometry(QRect(300, 440, 61, 37));
        EstructurasDiscretas->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        EstructurasDiscretas->setIcon(icon);
        EstructurasDiscretas->setIconSize(QSize(61, 37));
        EstructurasDiscretas->setCheckable(true);
        EstructurasDiscretas->setChecked(false);
        MetodologiaDeLaProgramacion = new QPushButton(ventanaAlumnoLCC);
        MetodologiaDeLaProgramacion->setObjectName(QStringLiteral("MetodologiaDeLaProgramacion"));
        MetodologiaDeLaProgramacion->setGeometry(QRect(140, 300, 61, 37));
        MetodologiaDeLaProgramacion->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        MetodologiaDeLaProgramacion->setIcon(icon);
        MetodologiaDeLaProgramacion->setIconSize(QSize(61, 37));
        MetodologiaDeLaProgramacion->setCheckable(true);
        MetodologiaDeLaProgramacion->setChecked(false);
        ProgramacionI = new QPushButton(ventanaAlumnoLCC);
        ProgramacionI->setObjectName(QStringLiteral("ProgramacionI"));
        ProgramacionI->setGeometry(QRect(220, 300, 61, 37));
        ProgramacionI->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        ProgramacionI->setIcon(icon);
        ProgramacionI->setIconSize(QSize(61, 37));
        ProgramacionI->setCheckable(true);
        ProgramacionI->setChecked(false);
        Ensamblador = new QPushButton(ventanaAlumnoLCC);
        Ensamblador->setObjectName(QStringLiteral("Ensamblador"));
        Ensamblador->setGeometry(QRect(300, 370, 61, 37));
        Ensamblador->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        Ensamblador->setIcon(icon);
        Ensamblador->setIconSize(QSize(61, 37));
        Ensamblador->setCheckable(true);
        Ensamblador->setChecked(false);
        LogicaMatematica = new QPushButton(ventanaAlumnoLCC);
        LogicaMatematica->setObjectName(QStringLiteral("LogicaMatematica"));
        LogicaMatematica->setGeometry(QRect(390, 370, 61, 37));
        LogicaMatematica->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        LogicaMatematica->setIcon(icon);
        LogicaMatematica->setIconSize(QSize(61, 37));
        LogicaMatematica->setCheckable(true);
        LogicaMatematica->setChecked(false);
        ProgramacionConcurrenteyParalela = new QPushButton(ventanaAlumnoLCC);
        ProgramacionConcurrenteyParalela->setObjectName(QStringLiteral("ProgramacionConcurrenteyParalela"));
        ProgramacionConcurrenteyParalela->setGeometry(QRect(550, 370, 61, 37));
        ProgramacionConcurrenteyParalela->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        ProgramacionConcurrenteyParalela->setIcon(icon);
        ProgramacionConcurrenteyParalela->setIconSize(QSize(61, 37));
        ProgramacionConcurrenteyParalela->setCheckable(true);
        ProgramacionConcurrenteyParalela->setChecked(false);
        RecuperacionDeLaInformacion = new QPushButton(ventanaAlumnoLCC);
        RecuperacionDeLaInformacion->setObjectName(QStringLiteral("RecuperacionDeLaInformacion"));
        RecuperacionDeLaInformacion->setGeometry(QRect(710, 370, 61, 37));
        RecuperacionDeLaInformacion->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        RecuperacionDeLaInformacion->setIcon(icon);
        RecuperacionDeLaInformacion->setIconSize(QSize(61, 37));
        RecuperacionDeLaInformacion->setCheckable(true);
        RecuperacionDeLaInformacion->setChecked(false);
        ProgramacionDistribuida = new QPushButton(ventanaAlumnoLCC);
        ProgramacionDistribuida->setObjectName(QStringLiteral("ProgramacionDistribuida"));
        ProgramacionDistribuida->setGeometry(QRect(630, 370, 61, 37));
        ProgramacionDistribuida->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        ProgramacionDistribuida->setIcon(icon);
        ProgramacionDistribuida->setIconSize(QSize(61, 37));
        ProgramacionDistribuida->setCheckable(true);
        ProgramacionDistribuida->setChecked(false);
        ArquitecturaFuncionalDeComputadoras = new QPushButton(ventanaAlumnoLCC);
        ArquitecturaFuncionalDeComputadoras->setObjectName(QStringLiteral("ArquitecturaFuncionalDeComputadoras"));
        ArquitecturaFuncionalDeComputadoras->setGeometry(QRect(790, 300, 61, 37));
        ArquitecturaFuncionalDeComputadoras->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        ArquitecturaFuncionalDeComputadoras->setIcon(icon);
        ArquitecturaFuncionalDeComputadoras->setIconSize(QSize(61, 37));
        ArquitecturaFuncionalDeComputadoras->setCheckable(true);
        ArquitecturaFuncionalDeComputadoras->setChecked(false);
        Computabilidad = new QPushButton(ventanaAlumnoLCC);
        Computabilidad->setObjectName(QStringLiteral("Computabilidad"));
        Computabilidad->setGeometry(QRect(710, 300, 61, 37));
        Computabilidad->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        Computabilidad->setIcon(icon);
        Computabilidad->setIconSize(QSize(61, 37));
        Computabilidad->setCheckable(true);
        Computabilidad->setChecked(false);
        SistemasOperativosII = new QPushButton(ventanaAlumnoLCC);
        SistemasOperativosII->setObjectName(QStringLiteral("SistemasOperativosII"));
        SistemasOperativosII->setGeometry(QRect(630, 300, 61, 37));
        SistemasOperativosII->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        SistemasOperativosII->setIcon(icon);
        SistemasOperativosII->setIconSize(QSize(61, 37));
        SistemasOperativosII->setCheckable(true);
        SistemasOperativosII->setChecked(false);
        FundamentosDeLenguajesDeProgramacion = new QPushButton(ventanaAlumnoLCC);
        FundamentosDeLenguajesDeProgramacion->setObjectName(QStringLiteral("FundamentosDeLenguajesDeProgramacion"));
        FundamentosDeLenguajesDeProgramacion->setGeometry(QRect(550, 300, 61, 37));
        FundamentosDeLenguajesDeProgramacion->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        FundamentosDeLenguajesDeProgramacion->setIcon(icon);
        FundamentosDeLenguajesDeProgramacion->setIconSize(QSize(61, 37));
        FundamentosDeLenguajesDeProgramacion->setCheckable(true);
        FundamentosDeLenguajesDeProgramacion->setChecked(false);
        SistemasOperativosI = new QPushButton(ventanaAlumnoLCC);
        SistemasOperativosI->setObjectName(QStringLiteral("SistemasOperativosI"));
        SistemasOperativosI->setGeometry(QRect(470, 300, 61, 37));
        SistemasOperativosI->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        SistemasOperativosI->setIcon(icon);
        SistemasOperativosI->setIconSize(QSize(61, 37));
        SistemasOperativosI->setCheckable(true);
        SistemasOperativosI->setChecked(false);
        EstructuraDeDatos = new QPushButton(ventanaAlumnoLCC);
        EstructuraDeDatos->setObjectName(QStringLiteral("EstructuraDeDatos"));
        EstructuraDeDatos->setGeometry(QRect(390, 300, 61, 37));
        EstructuraDeDatos->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        EstructuraDeDatos->setIcon(icon);
        EstructuraDeDatos->setIconSize(QSize(61, 37));
        EstructuraDeDatos->setCheckable(true);
        EstructuraDeDatos->setChecked(false);
        ProgramacionII = new QPushButton(ventanaAlumnoLCC);
        ProgramacionII->setObjectName(QStringLiteral("ProgramacionII"));
        ProgramacionII->setGeometry(QRect(300, 300, 61, 37));
        ProgramacionII->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        ProgramacionII->setIcon(icon);
        ProgramacionII->setIconSize(QSize(61, 37));
        ProgramacionII->setCheckable(true);
        ProgramacionII->setChecked(false);
        Optativa2 = new QPushButton(ventanaAlumnoLCC);
        Optativa2->setObjectName(QStringLiteral("Optativa2"));
        Optativa2->setGeometry(QRect(710, 640, 61, 37));
        Optativa2->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        Optativa2->setIcon(icon);
        Optativa2->setIconSize(QSize(61, 37));
        Optativa2->setCheckable(true);
        Optativa2->setChecked(false);
        Optativa3 = new QPushButton(ventanaAlumnoLCC);
        Optativa3->setObjectName(QStringLiteral("Optativa3"));
        Optativa3->setGeometry(QRect(790, 640, 61, 37));
        Optativa3->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        Optativa3->setIcon(icon);
        Optativa3->setIconSize(QSize(61, 37));
        Optativa3->setCheckable(true);
        Optativa3->setChecked(false);
        Optativa5 = new QPushButton(ventanaAlumnoLCC);
        Optativa5->setObjectName(QStringLiteral("Optativa5"));
        Optativa5->setGeometry(QRect(870, 640, 61, 37));
        Optativa5->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        Optativa5->setIcon(icon);
        Optativa5->setIconSize(QSize(61, 37));
        Optativa5->setCheckable(true);
        Optativa5->setChecked(false);
        Optativa4 = new QPushButton(ventanaAlumnoLCC);
        Optativa4->setObjectName(QStringLiteral("Optativa4"));
        Optativa4->setGeometry(QRect(870, 570, 61, 37));
        Optativa4->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        Optativa4->setIcon(icon);
        Optativa4->setIconSize(QSize(61, 37));
        Optativa4->setCheckable(true);
        Optativa4->setChecked(false);
        Optativa1 = new QPushButton(ventanaAlumnoLCC);
        Optativa1->setObjectName(QStringLiteral("Optativa1"));
        Optativa1->setGeometry(QRect(710, 500, 61, 37));
        Optativa1->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        Optativa1->setIcon(icon);
        Optativa1->setIconSize(QSize(61, 37));
        Optativa1->setCheckable(true);
        Optativa1->setChecked(false);
        CircuitosLogicos = new QPushButton(ventanaAlumnoLCC);
        CircuitosLogicos->setObjectName(QStringLiteral("CircuitosLogicos"));
        CircuitosLogicos->setGeometry(QRect(630, 220, 61, 37));
        CircuitosLogicos->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        CircuitosLogicos->setIcon(icon);
        CircuitosLogicos->setIconSize(QSize(61, 37));
        CircuitosLogicos->setCheckable(true);
        CircuitosLogicos->setChecked(false);
        CircuitosElectricos = new QPushButton(ventanaAlumnoLCC);
        CircuitosElectricos->setObjectName(QStringLiteral("CircuitosElectricos"));
        CircuitosElectricos->setGeometry(QRect(390, 220, 61, 37));
        CircuitosElectricos->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        CircuitosElectricos->setIcon(icon);
        CircuitosElectricos->setIconSize(QSize(61, 37));
        CircuitosElectricos->setCheckable(true);
        CircuitosElectricos->setChecked(false);
        AlgebraLineal = new QPushButton(ventanaAlumnoLCC);
        AlgebraLineal->setObjectName(QStringLiteral("AlgebraLineal"));
        AlgebraLineal->setGeometry(QRect(220, 220, 61, 37));
        AlgebraLineal->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        AlgebraLineal->setIcon(icon);
        AlgebraLineal->setIconSize(QSize(61, 37));
        AlgebraLineal->setCheckable(true);
        AlgebraLineal->setChecked(false);
        AlgebraSuperior = new QPushButton(ventanaAlumnoLCC);
        AlgebraSuperior->setObjectName(QStringLiteral("AlgebraSuperior"));
        AlgebraSuperior->setGeometry(QRect(140, 220, 61, 37));
        AlgebraSuperior->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        AlgebraSuperior->setIcon(icon);
        AlgebraSuperior->setIconSize(QSize(61, 37));
        AlgebraSuperior->setCheckable(true);
        AlgebraSuperior->setChecked(false);
        label->raise();
        label_8->raise();
        label_9->raise();
        label_2->raise();
        pushButton->raise();
        pushButton_2->raise();
        gridLayoutWidget->raise();
        buttRegresar->raise();
        AUXIMA->raise();
        MatematicasElementales->raise();
        CalculoDiferencial->raise();
        CalculoIntegral->raise();
        ProbabilidadyEstadistica->raise();
        LenguaExtranjeraI->raise();
        LenguaExtranjeraII->raise();
        LenguaExtranjeraIII->raise();
        LenguaExtranjeraIV->raise();
        FormacionHumanaySocial->raise();
        Dhpc->raise();
        IngenieriaDeSoftware->raise();
        AdministracionDeProyectos->raise();
        ProyectoIDI->raise();
        PracticaProfesional->raise();
        Optativa1Desit->raise();
        SeguridadEnRedes->raise();
        RedesDeComputadoras->raise();
        BasesDeDatos->raise();
        ServicioSocial->raise();
        InteligenciaArtificial->raise();
        Graficacion->raise();
        AnalisisyDisenoDeAlgoritmos->raise();
        LenguajesFormalesyAutomatas->raise();
        EstructurasDiscretas->raise();
        MetodologiaDeLaProgramacion->raise();
        ProgramacionI->raise();
        Ensamblador->raise();
        LogicaMatematica->raise();
        ProgramacionConcurrenteyParalela->raise();
        RecuperacionDeLaInformacion->raise();
        ProgramacionDistribuida->raise();
        ArquitecturaFuncionalDeComputadoras->raise();
        Computabilidad->raise();
        SistemasOperativosII->raise();
        FundamentosDeLenguajesDeProgramacion->raise();
        SistemasOperativosI->raise();
        EstructuraDeDatos->raise();
        ProgramacionII->raise();
        Optativa2->raise();
        Optativa3->raise();
        Optativa5->raise();
        Optativa4->raise();
        Optativa1->raise();
        CircuitosLogicos->raise();
        CircuitosElectricos->raise();
        AlgebraLineal->raise();
        AlgebraSuperior->raise();

        retranslateUi(ventanaAlumnoLCC);
        QObject::connect(buttRegresar, SIGNAL(clicked()), ventanaAlumnoLCC, SLOT(close()));

        QMetaObject::connectSlotsByName(ventanaAlumnoLCC);
    } // setupUi

    void retranslateUi(QDialog *ventanaAlumnoLCC)
    {
        ventanaAlumnoLCC->setWindowTitle(QApplication::translate("ventanaAlumnoLCC", "Dialog", 0));
        label->setText(QString());
        label_2->setText(QString());
        pushButton->setText(QApplication::translate("ventanaAlumnoLCC", "Guardar", 0));
        pushButton_2->setText(QApplication::translate("ventanaAlumnoLCC", "Ver Proyecci\303\263n", 0));
        label_NombreMatri->setText(QApplication::translate("ventanaAlumnoLCC", "201664822", 0));
        label_4->setText(QApplication::translate("ventanaAlumnoLCC", "Matr\303\255cula", 0));
        label_5->setText(QApplication::translate("ventanaAlumnoLCC", "Nombre", 0));
        label_NombreAlm->setText(QApplication::translate("ventanaAlumnoLCC", "Juana In\303\251s de Asbaje y Ram\303\255rez de Santillana", 0));
        label_6->setText(QApplication::translate("ventanaAlumnoLCC", "Porcentaje", 0));
        label_7->setText(QApplication::translate("ventanaAlumnoLCC", "100%", 0));
        label_8->setText(QString());
        label_9->setText(QString());
        buttRegresar->setText(QString());
        AUXIMA->setText(QString());
        MatematicasElementales->setText(QString());
        CalculoDiferencial->setText(QString());
        CalculoIntegral->setText(QString());
        ProbabilidadyEstadistica->setText(QString());
        LenguaExtranjeraI->setText(QString());
        LenguaExtranjeraII->setText(QString());
        LenguaExtranjeraIII->setText(QString());
        LenguaExtranjeraIV->setText(QString());
        FormacionHumanaySocial->setText(QString());
        Dhpc->setText(QString());
        IngenieriaDeSoftware->setText(QString());
        AdministracionDeProyectos->setText(QString());
        ProyectoIDI->setText(QString());
        PracticaProfesional->setText(QString());
        Optativa1Desit->setText(QString());
        SeguridadEnRedes->setText(QString());
        RedesDeComputadoras->setText(QString());
        BasesDeDatos->setText(QString());
        ServicioSocial->setText(QString());
        InteligenciaArtificial->setText(QString());
        Graficacion->setText(QString());
        AnalisisyDisenoDeAlgoritmos->setText(QString());
        LenguajesFormalesyAutomatas->setText(QString());
        EstructurasDiscretas->setText(QString());
        MetodologiaDeLaProgramacion->setText(QString());
        ProgramacionI->setText(QString());
        Ensamblador->setText(QString());
        LogicaMatematica->setText(QString());
        ProgramacionConcurrenteyParalela->setText(QString());
        RecuperacionDeLaInformacion->setText(QString());
        ProgramacionDistribuida->setText(QString());
        ArquitecturaFuncionalDeComputadoras->setText(QString());
        Computabilidad->setText(QString());
        SistemasOperativosII->setText(QString());
        FundamentosDeLenguajesDeProgramacion->setText(QString());
        SistemasOperativosI->setText(QString());
        EstructuraDeDatos->setText(QString());
        ProgramacionII->setText(QString());
        Optativa2->setText(QString());
        Optativa3->setText(QString());
        Optativa5->setText(QString());
        Optativa4->setText(QString());
        Optativa1->setText(QString());
        CircuitosLogicos->setText(QString());
        CircuitosElectricos->setText(QString());
        AlgebraLineal->setText(QString());
        AlgebraSuperior->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ventanaAlumnoLCC: public Ui_ventanaAlumnoLCC {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VENTANAALUMNOLCC_H
