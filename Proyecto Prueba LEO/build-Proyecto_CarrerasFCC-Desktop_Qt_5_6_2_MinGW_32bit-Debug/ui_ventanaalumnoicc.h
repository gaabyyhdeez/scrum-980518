/********************************************************************************
** Form generated from reading UI file 'ventanaalumnoicc.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VENTANAALUMNOICC_H
#define UI_VENTANAALUMNOICC_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ventanaAlumnoICC
{
public:
    QLabel *label;
    QLabel *label_2;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_NombreMatri;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_NombreAlm;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QPushButton *buttRegresar;
    QLabel *AUXIMA;
    QPushButton *Matematicas;
    QPushButton *CalculoDiferencial;
    QPushButton *CalculoIntegral;
    QPushButton *EcuacionesDiferenciales;
    QPushButton *ProbabilidadyEstadistica;
    QPushButton *ModeloDeRedes;
    QPushButton *RedesInalambricas;
    QPushButton *TeoriaDeControl;
    QPushButton *AlgebraSuperior;
    QPushButton *FisicaI;
    QPushButton *FisicaII;
    QPushButton *CircuitosElectricos;
    QPushButton *CircuitosElectronicos;
    QPushButton *DisenoDigital;
    QPushButton *MineriaDeDatos;
    QPushButton *AdministracionDeRedes;
    QPushButton *IntercomunicacionySeguridadEnRedes;
    QPushButton *AlgebraLinealConElementosDeGeometriaAnalitica;
    QPushButton *Ma1_19;
    QPushButton *Ma1_20;
    QPushButton *MatematicasDiscretas;
    QPushButton *BaseDeDatosParaIngenieria;
    QPushButton *ArquitecturaDeComputo;
    QPushButton *SistemasEmpotrados;
    QPushButton *MetodologiaDeLaProgramacion;
    QPushButton *ProgramacionII;
    QPushButton *ProgramacionI;
    QPushButton *Graficacion;
    QPushButton *SistemasOperativosI;
    QLabel *label_3;
    QPushButton *SistemasOperativosII;
    QPushButton *TecnicasDeInteligenciaArtificial;
    QPushButton *OptativaDesit;
    QPushButton *Ensamblador;
    QPushButton *AdministracionDeProyectos;
    QPushButton *ProgramacionDistribuidaAplicada;
    QPushButton *AnalisisyDiseoDeAlgoritmos;
    QPushButton *EstructurasDeDatos;
    QPushButton *ProgramacionConcurrenteyParalela;
    QPushButton *ServicioSocial;
    QPushButton *PracticaProfesional;
    QPushButton *DesarrolloDeAplicacionesMoviles;
    QPushButton *IngenieriaDeSoftware;
    QPushButton *DesarrolloDeAplicacionesWeb;
    QLabel *label_10;
    QPushButton *OptativaII;
    QPushButton *ProyectoIDI;
    QPushButton *OptativaI;
    QPushButton *LenguaExtranjeraI;
    QPushButton *Dhpc;
    QPushButton *LenguaExtranjeraIII;
    QPushButton *FormacionHumanaySocial;
    QPushButton *LenguaExtranjeraII;
    QPushButton *LenguaExtranjeraIV;

    void setupUi(QDialog *ventanaAlumnoICC)
    {
        if (ventanaAlumnoICC->objectName().isEmpty())
            ventanaAlumnoICC->setObjectName(QStringLiteral("ventanaAlumnoICC"));
        ventanaAlumnoICC->resize(1366, 748);
        ventanaAlumnoICC->setStyleSheet(QLatin1String("QDialog#ventanaAlumnoICC\n"
"{\n"
"                background-color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"QPushButton\n"
"\n"
"{\n"
"        border-width: 1.5px;\n"
"        border-color: rgb(82, 150, 170);\n"
"        border-radius:5px;\n"
"        padding: 0 8px;\n"
"\n"
"/*background-color: rgb(255, 255, 255);*/\n"
"        font: 10pt \"Yu Gothic\";\n"
"\n"
"        color:rgb(82, 150, 170)\n"
"}"));
        label = new QLabel(ventanaAlumnoICC);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(-40, 20, 1041, 671));
        label->setPixmap(QPixmap(QString::fromUtf8(":/Mapa_Carrera_ICC-001.jpg")));
        label->setScaledContents(true);
        label_2 = new QLabel(ventanaAlumnoICC);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(1070, 50, 181, 181));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/1200px-Escudobuappositivo2.png")));
        label_2->setScaledContents(true);
        pushButton = new QPushButton(ventanaAlumnoICC);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(1080, 540, 151, 41));
        QFont font;
        font.setFamily(QStringLiteral("Yu Gothic"));
        font.setPointSize(10);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        pushButton->setFont(font);
        pushButton->setStyleSheet(QLatin1String("background-color: rgb(0, 59, 92);\n"
"border-radius: 10px;\n"
"color: white;"));
        pushButton_2 = new QPushButton(ventanaAlumnoICC);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(1080, 610, 151, 41));
        pushButton_2->setFont(font);
        pushButton_2->setStyleSheet(QLatin1String("background-color: rgb(0, 59, 92);\n"
"border-radius: 10px;\n"
"color: white;"));
        gridLayoutWidget = new QWidget(ventanaAlumnoICC);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(970, 240, 371, 251));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_NombreMatri = new QLabel(gridLayoutWidget);
        label_NombreMatri->setObjectName(QStringLiteral("label_NombreMatri"));
        QFont font1;
        font1.setFamily(QStringLiteral("Segoe UI"));
        font1.setPointSize(13);
        font1.setBold(false);
        font1.setItalic(false);
        font1.setWeight(50);
        label_NombreMatri->setFont(font1);
        label_NombreMatri->setStyleSheet(QStringLiteral(""));
        label_NombreMatri->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_NombreMatri, 1, 0, 1, 1);

        label_4 = new QLabel(gridLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        QFont font2;
        font2.setFamily(QStringLiteral("Segoe UI"));
        font2.setPointSize(11);
        font2.setBold(true);
        font2.setItalic(false);
        font2.setWeight(75);
        label_4->setFont(font2);
        label_4->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        gridLayout->addWidget(label_4, 0, 0, 1, 1);

        label_5 = new QLabel(gridLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        QFont font3;
        font3.setFamily(QStringLiteral("Segoe UI"));
        font3.setPointSize(11);
        font3.setBold(true);
        font3.setWeight(75);
        label_5->setFont(font3);
        label_5->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        gridLayout->addWidget(label_5, 2, 0, 1, 1);

        label_NombreAlm = new QLabel(gridLayoutWidget);
        label_NombreAlm->setObjectName(QStringLiteral("label_NombreAlm"));
        QFont font4;
        font4.setFamily(QStringLiteral("Segoe UI"));
        font4.setPointSize(13);
        font4.setBold(false);
        font4.setWeight(50);
        label_NombreAlm->setFont(font4);
        label_NombreAlm->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_NombreAlm, 3, 0, 1, 1);

        label_6 = new QLabel(gridLayoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        QFont font5;
        font5.setPointSize(11);
        font5.setBold(true);
        font5.setWeight(75);
        label_6->setFont(font5);
        label_6->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        gridLayout->addWidget(label_6, 4, 0, 1, 1);

        label_7 = new QLabel(gridLayoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setFont(font4);
        label_7->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_7, 5, 0, 1, 1);

        label_8 = new QLabel(ventanaAlumnoICC);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(970, 20, 371, 671));
        label_8->setStyleSheet(QLatin1String("background-color: rgb(235, 235, 235);\n"
"border-radius: 10px;\n"
"color: white;alternate-background-color: rgb(222, 222, 222);"));
        label_9 = new QLabel(ventanaAlumnoICC);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(970, 20, 371, 671));
        label_9->setStyleSheet(QLatin1String("border-radius: 10px;\n"
"border: 2px solid #003b5c;"));
        buttRegresar = new QPushButton(ventanaAlumnoICC);
        buttRegresar->setObjectName(QStringLiteral("buttRegresar"));
        buttRegresar->setGeometry(QRect(10, 10, 41, 23));
        buttRegresar->setStyleSheet(QLatin1String("background-color: none;\n"
"border-image:url(:/west-arrow (1).png);"));
        AUXIMA = new QLabel(ventanaAlumnoICC);
        AUXIMA->setObjectName(QStringLiteral("AUXIMA"));
        AUXIMA->setGeometry(QRect(-40, 20, 1041, 671));
        AUXIMA->setPixmap(QPixmap(QString::fromUtf8("Im\303\241genes/Mapa_Carrera_ICC-001.jpg")));
        AUXIMA->setScaledContents(true);
        Matematicas = new QPushButton(ventanaAlumnoICC);
        Matematicas->setObjectName(QStringLiteral("Matematicas"));
        Matematicas->setGeometry(QRect(120, 150, 71, 37));
        QIcon icon;
        icon.addFile(QStringLiteral("../marcado/transparente.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon.addFile(QStringLiteral("../marcado/PALOMA.png"), QSize(), QIcon::Normal, QIcon::On);
        icon.addFile(QStringLiteral("../marcado/transparente.png"), QSize(), QIcon::Disabled, QIcon::Off);
        icon.addFile(QStringLiteral("../marcado/transparente.png"), QSize(), QIcon::Active, QIcon::Off);
        Matematicas->setIcon(icon);
        Matematicas->setIconSize(QSize(71, 37));
        Matematicas->setCheckable(true);
        Matematicas->setChecked(false);
        CalculoDiferencial = new QPushButton(ventanaAlumnoICC);
        CalculoDiferencial->setObjectName(QStringLiteral("CalculoDiferencial"));
        CalculoDiferencial->setGeometry(QRect(200, 150, 71, 37));
        CalculoDiferencial->setIcon(icon);
        CalculoDiferencial->setIconSize(QSize(71, 37));
        CalculoDiferencial->setCheckable(true);
        CalculoDiferencial->setChecked(false);
        CalculoIntegral = new QPushButton(ventanaAlumnoICC);
        CalculoIntegral->setObjectName(QStringLiteral("CalculoIntegral"));
        CalculoIntegral->setGeometry(QRect(280, 150, 71, 37));
        CalculoIntegral->setIcon(icon);
        CalculoIntegral->setIconSize(QSize(71, 37));
        CalculoIntegral->setCheckable(true);
        CalculoIntegral->setChecked(false);
        EcuacionesDiferenciales = new QPushButton(ventanaAlumnoICC);
        EcuacionesDiferenciales->setObjectName(QStringLiteral("EcuacionesDiferenciales"));
        EcuacionesDiferenciales->setGeometry(QRect(360, 150, 71, 37));
        EcuacionesDiferenciales->setIcon(icon);
        EcuacionesDiferenciales->setIconSize(QSize(71, 37));
        EcuacionesDiferenciales->setCheckable(true);
        EcuacionesDiferenciales->setChecked(false);
        ProbabilidadyEstadistica = new QPushButton(ventanaAlumnoICC);
        ProbabilidadyEstadistica->setObjectName(QStringLiteral("ProbabilidadyEstadistica"));
        ProbabilidadyEstadistica->setGeometry(QRect(470, 150, 71, 37));
        ProbabilidadyEstadistica->setIcon(icon);
        ProbabilidadyEstadistica->setIconSize(QSize(71, 37));
        ProbabilidadyEstadistica->setCheckable(true);
        ProbabilidadyEstadistica->setChecked(false);
        ModeloDeRedes = new QPushButton(ventanaAlumnoICC);
        ModeloDeRedes->setObjectName(QStringLiteral("ModeloDeRedes"));
        ModeloDeRedes->setGeometry(QRect(550, 150, 71, 37));
        ModeloDeRedes->setIcon(icon);
        ModeloDeRedes->setIconSize(QSize(71, 37));
        ModeloDeRedes->setCheckable(true);
        ModeloDeRedes->setChecked(false);
        RedesInalambricas = new QPushButton(ventanaAlumnoICC);
        RedesInalambricas->setObjectName(QStringLiteral("RedesInalambricas"));
        RedesInalambricas->setGeometry(QRect(630, 150, 71, 37));
        RedesInalambricas->setIcon(icon);
        RedesInalambricas->setIconSize(QSize(71, 37));
        RedesInalambricas->setCheckable(true);
        RedesInalambricas->setChecked(false);
        TeoriaDeControl = new QPushButton(ventanaAlumnoICC);
        TeoriaDeControl->setObjectName(QStringLiteral("TeoriaDeControl"));
        TeoriaDeControl->setGeometry(QRect(710, 150, 71, 37));
        TeoriaDeControl->setIcon(icon);
        TeoriaDeControl->setIconSize(QSize(71, 37));
        TeoriaDeControl->setCheckable(true);
        TeoriaDeControl->setChecked(false);
        AlgebraSuperior = new QPushButton(ventanaAlumnoICC);
        AlgebraSuperior->setObjectName(QStringLiteral("AlgebraSuperior"));
        AlgebraSuperior->setGeometry(QRect(120, 200, 71, 37));
        AlgebraSuperior->setIcon(icon);
        AlgebraSuperior->setIconSize(QSize(71, 37));
        AlgebraSuperior->setCheckable(true);
        AlgebraSuperior->setChecked(false);
        FisicaI = new QPushButton(ventanaAlumnoICC);
        FisicaI->setObjectName(QStringLiteral("FisicaI"));
        FisicaI->setGeometry(QRect(200, 200, 71, 41));
        FisicaI->setIcon(icon);
        FisicaI->setIconSize(QSize(71, 37));
        FisicaI->setCheckable(true);
        FisicaI->setChecked(false);
        FisicaII = new QPushButton(ventanaAlumnoICC);
        FisicaII->setObjectName(QStringLiteral("FisicaII"));
        FisicaII->setGeometry(QRect(280, 200, 71, 37));
        FisicaII->setIcon(icon);
        FisicaII->setIconSize(QSize(71, 37));
        FisicaII->setCheckable(true);
        FisicaII->setChecked(false);
        CircuitosElectricos = new QPushButton(ventanaAlumnoICC);
        CircuitosElectricos->setObjectName(QStringLiteral("CircuitosElectricos"));
        CircuitosElectricos->setGeometry(QRect(360, 200, 71, 37));
        CircuitosElectricos->setIcon(icon);
        CircuitosElectricos->setIconSize(QSize(71, 37));
        CircuitosElectricos->setCheckable(true);
        CircuitosElectricos->setChecked(false);
        CircuitosElectronicos = new QPushButton(ventanaAlumnoICC);
        CircuitosElectronicos->setObjectName(QStringLiteral("CircuitosElectronicos"));
        CircuitosElectronicos->setGeometry(QRect(470, 200, 71, 37));
        CircuitosElectronicos->setIcon(icon);
        CircuitosElectronicos->setIconSize(QSize(71, 37));
        CircuitosElectronicos->setCheckable(true);
        CircuitosElectronicos->setChecked(false);
        DisenoDigital = new QPushButton(ventanaAlumnoICC);
        DisenoDigital->setObjectName(QStringLiteral("DisenoDigital"));
        DisenoDigital->setGeometry(QRect(550, 200, 71, 37));
        DisenoDigital->setIcon(icon);
        DisenoDigital->setIconSize(QSize(71, 37));
        DisenoDigital->setCheckable(true);
        DisenoDigital->setChecked(false);
        MineriaDeDatos = new QPushButton(ventanaAlumnoICC);
        MineriaDeDatos->setObjectName(QStringLiteral("MineriaDeDatos"));
        MineriaDeDatos->setGeometry(QRect(630, 200, 71, 37));
        MineriaDeDatos->setIcon(icon);
        MineriaDeDatos->setIconSize(QSize(71, 37));
        MineriaDeDatos->setCheckable(true);
        MineriaDeDatos->setChecked(false);
        AdministracionDeRedes = new QPushButton(ventanaAlumnoICC);
        AdministracionDeRedes->setObjectName(QStringLiteral("AdministracionDeRedes"));
        AdministracionDeRedes->setGeometry(QRect(710, 200, 71, 37));
        AdministracionDeRedes->setIcon(icon);
        AdministracionDeRedes->setIconSize(QSize(71, 37));
        AdministracionDeRedes->setCheckable(true);
        AdministracionDeRedes->setChecked(false);
        IntercomunicacionySeguridadEnRedes = new QPushButton(ventanaAlumnoICC);
        IntercomunicacionySeguridadEnRedes->setObjectName(QStringLiteral("IntercomunicacionySeguridadEnRedes"));
        IntercomunicacionySeguridadEnRedes->setGeometry(QRect(790, 200, 71, 37));
        IntercomunicacionySeguridadEnRedes->setIcon(icon);
        IntercomunicacionySeguridadEnRedes->setIconSize(QSize(71, 37));
        IntercomunicacionySeguridadEnRedes->setCheckable(true);
        IntercomunicacionySeguridadEnRedes->setChecked(false);
        AlgebraLinealConElementosDeGeometriaAnalitica = new QPushButton(ventanaAlumnoICC);
        AlgebraLinealConElementosDeGeometriaAnalitica->setObjectName(QStringLiteral("AlgebraLinealConElementosDeGeometriaAnalitica"));
        AlgebraLinealConElementosDeGeometriaAnalitica->setGeometry(QRect(200, 260, 71, 37));
        AlgebraLinealConElementosDeGeometriaAnalitica->setIcon(icon);
        AlgebraLinealConElementosDeGeometriaAnalitica->setIconSize(QSize(71, 37));
        AlgebraLinealConElementosDeGeometriaAnalitica->setCheckable(true);
        AlgebraLinealConElementosDeGeometriaAnalitica->setChecked(false);
        Ma1_19 = new QPushButton(ventanaAlumnoICC);
        Ma1_19->setObjectName(QStringLiteral("Ma1_19"));
        Ma1_19->setGeometry(QRect(910, 720, 71, 37));
        Ma1_19->setIcon(icon);
        Ma1_19->setIconSize(QSize(71, 37));
        Ma1_19->setCheckable(true);
        Ma1_19->setChecked(false);
        Ma1_20 = new QPushButton(ventanaAlumnoICC);
        Ma1_20->setObjectName(QStringLiteral("Ma1_20"));
        Ma1_20->setGeometry(QRect(250, 430, 71, 37));
        Ma1_20->setIcon(icon);
        Ma1_20->setIconSize(QSize(71, 37));
        Ma1_20->setCheckable(true);
        Ma1_20->setChecked(false);
        MatematicasDiscretas = new QPushButton(ventanaAlumnoICC);
        MatematicasDiscretas->setObjectName(QStringLiteral("MatematicasDiscretas"));
        MatematicasDiscretas->setGeometry(QRect(280, 260, 71, 37));
        MatematicasDiscretas->setIcon(icon);
        MatematicasDiscretas->setIconSize(QSize(71, 37));
        MatematicasDiscretas->setCheckable(true);
        MatematicasDiscretas->setChecked(false);
        BaseDeDatosParaIngenieria = new QPushButton(ventanaAlumnoICC);
        BaseDeDatosParaIngenieria->setObjectName(QStringLiteral("BaseDeDatosParaIngenieria"));
        BaseDeDatosParaIngenieria->setGeometry(QRect(550, 260, 71, 37));
        BaseDeDatosParaIngenieria->setIcon(icon);
        BaseDeDatosParaIngenieria->setIconSize(QSize(71, 37));
        BaseDeDatosParaIngenieria->setCheckable(true);
        BaseDeDatosParaIngenieria->setChecked(false);
        ArquitecturaDeComputo = new QPushButton(ventanaAlumnoICC);
        ArquitecturaDeComputo->setObjectName(QStringLiteral("ArquitecturaDeComputo"));
        ArquitecturaDeComputo->setGeometry(QRect(630, 260, 71, 37));
        ArquitecturaDeComputo->setIcon(icon);
        ArquitecturaDeComputo->setIconSize(QSize(71, 37));
        ArquitecturaDeComputo->setCheckable(true);
        ArquitecturaDeComputo->setChecked(false);
        SistemasEmpotrados = new QPushButton(ventanaAlumnoICC);
        SistemasEmpotrados->setObjectName(QStringLiteral("SistemasEmpotrados"));
        SistemasEmpotrados->setGeometry(QRect(790, 260, 71, 37));
        SistemasEmpotrados->setIcon(icon);
        SistemasEmpotrados->setIconSize(QSize(71, 37));
        SistemasEmpotrados->setCheckable(true);
        SistemasEmpotrados->setChecked(false);
        MetodologiaDeLaProgramacion = new QPushButton(ventanaAlumnoICC);
        MetodologiaDeLaProgramacion->setObjectName(QStringLiteral("MetodologiaDeLaProgramacion"));
        MetodologiaDeLaProgramacion->setGeometry(QRect(120, 320, 71, 37));
        MetodologiaDeLaProgramacion->setIcon(icon);
        MetodologiaDeLaProgramacion->setIconSize(QSize(71, 37));
        MetodologiaDeLaProgramacion->setCheckable(true);
        MetodologiaDeLaProgramacion->setChecked(false);
        ProgramacionII = new QPushButton(ventanaAlumnoICC);
        ProgramacionII->setObjectName(QStringLiteral("ProgramacionII"));
        ProgramacionII->setGeometry(QRect(280, 320, 71, 37));
        ProgramacionII->setIcon(icon);
        ProgramacionII->setIconSize(QSize(71, 37));
        ProgramacionII->setCheckable(true);
        ProgramacionII->setChecked(false);
        ProgramacionI = new QPushButton(ventanaAlumnoICC);
        ProgramacionI->setObjectName(QStringLiteral("ProgramacionI"));
        ProgramacionI->setGeometry(QRect(200, 320, 71, 37));
        ProgramacionI->setIcon(icon);
        ProgramacionI->setIconSize(QSize(71, 37));
        ProgramacionI->setCheckable(true);
        ProgramacionI->setChecked(false);
        Graficacion = new QPushButton(ventanaAlumnoICC);
        Graficacion->setObjectName(QStringLiteral("Graficacion"));
        Graficacion->setGeometry(QRect(360, 320, 71, 37));
        Graficacion->setIcon(icon);
        Graficacion->setIconSize(QSize(71, 37));
        Graficacion->setCheckable(true);
        Graficacion->setChecked(false);
        SistemasOperativosI = new QPushButton(ventanaAlumnoICC);
        SistemasOperativosI->setObjectName(QStringLiteral("SistemasOperativosI"));
        SistemasOperativosI->setGeometry(QRect(470, 320, 71, 37));
        SistemasOperativosI->setIcon(icon);
        SistemasOperativosI->setIconSize(QSize(71, 37));
        SistemasOperativosI->setCheckable(true);
        SistemasOperativosI->setChecked(false);
        label_3 = new QLabel(ventanaAlumnoICC);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(-40, 20, 1041, 671));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/Mapa_Carrera_ICC-001.jpg")));
        label_3->setScaledContents(true);
        SistemasOperativosII = new QPushButton(ventanaAlumnoICC);
        SistemasOperativosII->setObjectName(QStringLiteral("SistemasOperativosII"));
        SistemasOperativosII->setGeometry(QRect(550, 320, 71, 37));
        SistemasOperativosII->setIcon(icon);
        SistemasOperativosII->setIconSize(QSize(71, 37));
        SistemasOperativosII->setCheckable(true);
        SistemasOperativosII->setChecked(false);
        TecnicasDeInteligenciaArtificial = new QPushButton(ventanaAlumnoICC);
        TecnicasDeInteligenciaArtificial->setObjectName(QStringLiteral("TecnicasDeInteligenciaArtificial"));
        TecnicasDeInteligenciaArtificial->setGeometry(QRect(710, 320, 71, 37));
        TecnicasDeInteligenciaArtificial->setIcon(icon);
        TecnicasDeInteligenciaArtificial->setIconSize(QSize(71, 37));
        TecnicasDeInteligenciaArtificial->setCheckable(true);
        TecnicasDeInteligenciaArtificial->setChecked(false);
        OptativaDesit = new QPushButton(ventanaAlumnoICC);
        OptativaDesit->setObjectName(QStringLiteral("OptativaDesit"));
        OptativaDesit->setGeometry(QRect(870, 320, 71, 37));
        OptativaDesit->setIcon(icon);
        OptativaDesit->setIconSize(QSize(71, 37));
        OptativaDesit->setCheckable(true);
        OptativaDesit->setChecked(false);
        Ensamblador = new QPushButton(ventanaAlumnoICC);
        Ensamblador->setObjectName(QStringLiteral("Ensamblador"));
        Ensamblador->setGeometry(QRect(280, 380, 71, 37));
        Ensamblador->setIcon(icon);
        Ensamblador->setIconSize(QSize(71, 37));
        Ensamblador->setCheckable(true);
        Ensamblador->setChecked(false);
        AdministracionDeProyectos = new QPushButton(ventanaAlumnoICC);
        AdministracionDeProyectos->setObjectName(QStringLiteral("AdministracionDeProyectos"));
        AdministracionDeProyectos->setGeometry(QRect(550, 380, 71, 37));
        AdministracionDeProyectos->setIcon(icon);
        AdministracionDeProyectos->setIconSize(QSize(71, 37));
        AdministracionDeProyectos->setCheckable(true);
        AdministracionDeProyectos->setChecked(false);
        ProgramacionDistribuidaAplicada = new QPushButton(ventanaAlumnoICC);
        ProgramacionDistribuidaAplicada->setObjectName(QStringLiteral("ProgramacionDistribuidaAplicada"));
        ProgramacionDistribuidaAplicada->setGeometry(QRect(710, 380, 71, 37));
        ProgramacionDistribuidaAplicada->setIcon(icon);
        ProgramacionDistribuidaAplicada->setIconSize(QSize(71, 37));
        ProgramacionDistribuidaAplicada->setCheckable(true);
        ProgramacionDistribuidaAplicada->setChecked(false);
        AnalisisyDiseoDeAlgoritmos = new QPushButton(ventanaAlumnoICC);
        AnalisisyDiseoDeAlgoritmos->setObjectName(QStringLiteral("AnalisisyDiseoDeAlgoritmos"));
        AnalisisyDiseoDeAlgoritmos->setGeometry(QRect(470, 370, 71, 51));
        AnalisisyDiseoDeAlgoritmos->setIcon(icon);
        AnalisisyDiseoDeAlgoritmos->setIconSize(QSize(71, 37));
        AnalisisyDiseoDeAlgoritmos->setCheckable(true);
        AnalisisyDiseoDeAlgoritmos->setChecked(false);
        EstructurasDeDatos = new QPushButton(ventanaAlumnoICC);
        EstructurasDeDatos->setObjectName(QStringLiteral("EstructurasDeDatos"));
        EstructurasDeDatos->setGeometry(QRect(360, 380, 71, 37));
        EstructurasDeDatos->setIcon(icon);
        EstructurasDeDatos->setIconSize(QSize(71, 37));
        EstructurasDeDatos->setCheckable(true);
        EstructurasDeDatos->setChecked(false);
        ProgramacionConcurrenteyParalela = new QPushButton(ventanaAlumnoICC);
        ProgramacionConcurrenteyParalela->setObjectName(QStringLiteral("ProgramacionConcurrenteyParalela"));
        ProgramacionConcurrenteyParalela->setGeometry(QRect(630, 380, 71, 37));
        ProgramacionConcurrenteyParalela->setIcon(icon);
        ProgramacionConcurrenteyParalela->setIconSize(QSize(71, 37));
        ProgramacionConcurrenteyParalela->setCheckable(true);
        ProgramacionConcurrenteyParalela->setChecked(false);
        ServicioSocial = new QPushButton(ventanaAlumnoICC);
        ServicioSocial->setObjectName(QStringLiteral("ServicioSocial"));
        ServicioSocial->setGeometry(QRect(790, 380, 71, 37));
        ServicioSocial->setIcon(icon);
        ServicioSocial->setIconSize(QSize(71, 37));
        ServicioSocial->setCheckable(true);
        ServicioSocial->setChecked(false);
        PracticaProfesional = new QPushButton(ventanaAlumnoICC);
        PracticaProfesional->setObjectName(QStringLiteral("PracticaProfesional"));
        PracticaProfesional->setGeometry(QRect(870, 380, 71, 37));
        PracticaProfesional->setIcon(icon);
        PracticaProfesional->setIconSize(QSize(71, 37));
        PracticaProfesional->setCheckable(true);
        PracticaProfesional->setChecked(false);
        DesarrolloDeAplicacionesMoviles = new QPushButton(ventanaAlumnoICC);
        DesarrolloDeAplicacionesMoviles->setObjectName(QStringLiteral("DesarrolloDeAplicacionesMoviles"));
        DesarrolloDeAplicacionesMoviles->setGeometry(QRect(710, 440, 71, 37));
        DesarrolloDeAplicacionesMoviles->setIcon(icon);
        DesarrolloDeAplicacionesMoviles->setIconSize(QSize(71, 37));
        DesarrolloDeAplicacionesMoviles->setCheckable(true);
        DesarrolloDeAplicacionesMoviles->setChecked(false);
        IngenieriaDeSoftware = new QPushButton(ventanaAlumnoICC);
        IngenieriaDeSoftware->setObjectName(QStringLiteral("IngenieriaDeSoftware"));
        IngenieriaDeSoftware->setGeometry(QRect(470, 440, 71, 37));
        IngenieriaDeSoftware->setIcon(icon);
        IngenieriaDeSoftware->setIconSize(QSize(71, 37));
        IngenieriaDeSoftware->setCheckable(true);
        IngenieriaDeSoftware->setChecked(false);
        DesarrolloDeAplicacionesWeb = new QPushButton(ventanaAlumnoICC);
        DesarrolloDeAplicacionesWeb->setObjectName(QStringLiteral("DesarrolloDeAplicacionesWeb"));
        DesarrolloDeAplicacionesWeb->setGeometry(QRect(630, 440, 71, 37));
        DesarrolloDeAplicacionesWeb->setIcon(icon);
        DesarrolloDeAplicacionesWeb->setIconSize(QSize(71, 37));
        DesarrolloDeAplicacionesWeb->setCheckable(true);
        DesarrolloDeAplicacionesWeb->setChecked(false);
        label_10 = new QLabel(ventanaAlumnoICC);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(-20, 40, 1041, 671));
        label_10->setPixmap(QPixmap(QString::fromUtf8(":/Mapa_Carrera_ICC-001.jpg")));
        label_10->setScaledContents(true);
        OptativaII = new QPushButton(ventanaAlumnoICC);
        OptativaII->setObjectName(QStringLiteral("OptativaII"));
        OptativaII->setGeometry(QRect(790, 500, 71, 37));
        OptativaII->setIcon(icon);
        OptativaII->setIconSize(QSize(71, 37));
        OptativaII->setCheckable(true);
        OptativaII->setChecked(false);
        ProyectoIDI = new QPushButton(ventanaAlumnoICC);
        ProyectoIDI->setObjectName(QStringLiteral("ProyectoIDI"));
        ProyectoIDI->setGeometry(QRect(870, 500, 71, 37));
        ProyectoIDI->setIcon(icon);
        ProyectoIDI->setIconSize(QSize(71, 37));
        ProyectoIDI->setCheckable(true);
        ProyectoIDI->setChecked(false);
        OptativaI = new QPushButton(ventanaAlumnoICC);
        OptativaI->setObjectName(QStringLiteral("OptativaI"));
        OptativaI->setGeometry(QRect(710, 500, 71, 37));
        OptativaI->setIcon(icon);
        OptativaI->setIconSize(QSize(71, 37));
        OptativaI->setCheckable(true);
        OptativaI->setChecked(false);
        LenguaExtranjeraI = new QPushButton(ventanaAlumnoICC);
        LenguaExtranjeraI->setObjectName(QStringLiteral("LenguaExtranjeraI"));
        LenguaExtranjeraI->setGeometry(QRect(120, 550, 71, 37));
        LenguaExtranjeraI->setIcon(icon);
        LenguaExtranjeraI->setIconSize(QSize(71, 37));
        LenguaExtranjeraI->setCheckable(true);
        LenguaExtranjeraI->setChecked(false);
        Dhpc = new QPushButton(ventanaAlumnoICC);
        Dhpc->setObjectName(QStringLiteral("Dhpc"));
        Dhpc->setGeometry(QRect(200, 600, 71, 37));
        Dhpc->setIcon(icon);
        Dhpc->setIconSize(QSize(71, 37));
        Dhpc->setCheckable(true);
        Dhpc->setChecked(false);
        LenguaExtranjeraIII = new QPushButton(ventanaAlumnoICC);
        LenguaExtranjeraIII->setObjectName(QStringLiteral("LenguaExtranjeraIII"));
        LenguaExtranjeraIII->setGeometry(QRect(280, 550, 71, 37));
        LenguaExtranjeraIII->setIcon(icon);
        LenguaExtranjeraIII->setIconSize(QSize(71, 37));
        LenguaExtranjeraIII->setCheckable(true);
        LenguaExtranjeraIII->setChecked(false);
        FormacionHumanaySocial = new QPushButton(ventanaAlumnoICC);
        FormacionHumanaySocial->setObjectName(QStringLiteral("FormacionHumanaySocial"));
        FormacionHumanaySocial->setGeometry(QRect(120, 600, 71, 37));
        FormacionHumanaySocial->setIcon(icon);
        FormacionHumanaySocial->setIconSize(QSize(71, 37));
        FormacionHumanaySocial->setCheckable(true);
        FormacionHumanaySocial->setChecked(false);
        LenguaExtranjeraII = new QPushButton(ventanaAlumnoICC);
        LenguaExtranjeraII->setObjectName(QStringLiteral("LenguaExtranjeraII"));
        LenguaExtranjeraII->setGeometry(QRect(200, 550, 71, 37));
        LenguaExtranjeraII->setIcon(icon);
        LenguaExtranjeraII->setIconSize(QSize(71, 37));
        LenguaExtranjeraII->setCheckable(true);
        LenguaExtranjeraII->setChecked(false);
        LenguaExtranjeraIV = new QPushButton(ventanaAlumnoICC);
        LenguaExtranjeraIV->setObjectName(QStringLiteral("LenguaExtranjeraIV"));
        LenguaExtranjeraIV->setGeometry(QRect(360, 550, 71, 37));
        LenguaExtranjeraIV->setIcon(icon);
        LenguaExtranjeraIV->setIconSize(QSize(71, 37));
        LenguaExtranjeraIV->setCheckable(true);
        LenguaExtranjeraIV->setChecked(false);
        label_10->raise();
        label_3->raise();
        AUXIMA->raise();
        label->raise();
        label_8->raise();
        label_9->raise();
        label_2->raise();
        pushButton->raise();
        pushButton_2->raise();
        gridLayoutWidget->raise();
        buttRegresar->raise();
        Matematicas->raise();
        CalculoDiferencial->raise();
        CalculoIntegral->raise();
        EcuacionesDiferenciales->raise();
        ProbabilidadyEstadistica->raise();
        ModeloDeRedes->raise();
        RedesInalambricas->raise();
        TeoriaDeControl->raise();
        AlgebraSuperior->raise();
        FisicaI->raise();
        FisicaII->raise();
        CircuitosElectricos->raise();
        CircuitosElectronicos->raise();
        DisenoDigital->raise();
        MineriaDeDatos->raise();
        AdministracionDeRedes->raise();
        IntercomunicacionySeguridadEnRedes->raise();
        AlgebraLinealConElementosDeGeometriaAnalitica->raise();
        Ma1_19->raise();
        Ma1_20->raise();
        MatematicasDiscretas->raise();
        BaseDeDatosParaIngenieria->raise();
        ArquitecturaDeComputo->raise();
        SistemasEmpotrados->raise();
        MetodologiaDeLaProgramacion->raise();
        ProgramacionII->raise();
        ProgramacionI->raise();
        Graficacion->raise();
        SistemasOperativosI->raise();
        SistemasOperativosII->raise();
        TecnicasDeInteligenciaArtificial->raise();
        OptativaDesit->raise();
        Ensamblador->raise();
        AdministracionDeProyectos->raise();
        ProgramacionDistribuidaAplicada->raise();
        AnalisisyDiseoDeAlgoritmos->raise();
        EstructurasDeDatos->raise();
        ProgramacionConcurrenteyParalela->raise();
        ServicioSocial->raise();
        PracticaProfesional->raise();
        DesarrolloDeAplicacionesMoviles->raise();
        IngenieriaDeSoftware->raise();
        DesarrolloDeAplicacionesWeb->raise();
        OptativaII->raise();
        ProyectoIDI->raise();
        OptativaI->raise();
        LenguaExtranjeraI->raise();
        Dhpc->raise();
        LenguaExtranjeraIII->raise();
        FormacionHumanaySocial->raise();
        LenguaExtranjeraII->raise();
        LenguaExtranjeraIV->raise();

        retranslateUi(ventanaAlumnoICC);
        QObject::connect(buttRegresar, SIGNAL(clicked()), ventanaAlumnoICC, SLOT(close()));

        QMetaObject::connectSlotsByName(ventanaAlumnoICC);
    } // setupUi

    void retranslateUi(QDialog *ventanaAlumnoICC)
    {
        ventanaAlumnoICC->setWindowTitle(QApplication::translate("ventanaAlumnoICC", "Dialog", 0));
        label->setText(QString());
        label_2->setText(QString());
        pushButton->setText(QApplication::translate("ventanaAlumnoICC", "Guardar", 0));
        pushButton_2->setText(QApplication::translate("ventanaAlumnoICC", "Ver Proyecci\303\263n", 0));
        label_NombreMatri->setText(QApplication::translate("ventanaAlumnoICC", "201664822", 0));
        label_4->setText(QApplication::translate("ventanaAlumnoICC", "Matr\303\255cula", 0));
        label_5->setText(QApplication::translate("ventanaAlumnoICC", "Nombre", 0));
        label_NombreAlm->setText(QApplication::translate("ventanaAlumnoICC", "Juana In\303\251s de Asbaje y Ram\303\255rez de Santillana", 0));
        label_6->setText(QApplication::translate("ventanaAlumnoICC", "Porcentaje", 0));
        label_7->setText(QApplication::translate("ventanaAlumnoICC", "100%", 0));
        label_8->setText(QString());
        label_9->setText(QString());
        buttRegresar->setText(QString());
        AUXIMA->setText(QString());
        Matematicas->setText(QString());
        CalculoDiferencial->setText(QString());
        CalculoIntegral->setText(QString());
        EcuacionesDiferenciales->setText(QString());
        ProbabilidadyEstadistica->setText(QString());
        ModeloDeRedes->setText(QString());
        RedesInalambricas->setText(QString());
        TeoriaDeControl->setText(QString());
        AlgebraSuperior->setText(QString());
        FisicaI->setText(QString());
        FisicaII->setText(QString());
        CircuitosElectricos->setText(QString());
        CircuitosElectronicos->setText(QString());
        DisenoDigital->setText(QString());
        MineriaDeDatos->setText(QString());
        AdministracionDeRedes->setText(QString());
        IntercomunicacionySeguridadEnRedes->setText(QString());
        AlgebraLinealConElementosDeGeometriaAnalitica->setText(QString());
        Ma1_19->setText(QString());
        Ma1_20->setText(QString());
        MatematicasDiscretas->setText(QString());
        BaseDeDatosParaIngenieria->setText(QString());
        ArquitecturaDeComputo->setText(QString());
        SistemasEmpotrados->setText(QString());
        MetodologiaDeLaProgramacion->setText(QString());
        ProgramacionII->setText(QString());
        ProgramacionI->setText(QString());
        Graficacion->setText(QString());
        SistemasOperativosI->setText(QString());
        label_3->setText(QString());
        SistemasOperativosII->setText(QString());
        TecnicasDeInteligenciaArtificial->setText(QString());
        OptativaDesit->setText(QString());
        Ensamblador->setText(QString());
        AdministracionDeProyectos->setText(QString());
        ProgramacionDistribuidaAplicada->setText(QString());
        AnalisisyDiseoDeAlgoritmos->setText(QString());
        EstructurasDeDatos->setText(QString());
        ProgramacionConcurrenteyParalela->setText(QString());
        ServicioSocial->setText(QString());
        PracticaProfesional->setText(QString());
        DesarrolloDeAplicacionesMoviles->setText(QString());
        IngenieriaDeSoftware->setText(QString());
        DesarrolloDeAplicacionesWeb->setText(QString());
        label_10->setText(QString());
        OptativaII->setText(QString());
        ProyectoIDI->setText(QString());
        OptativaI->setText(QString());
        LenguaExtranjeraI->setText(QString());
        Dhpc->setText(QString());
        LenguaExtranjeraIII->setText(QString());
        FormacionHumanaySocial->setText(QString());
        LenguaExtranjeraII->setText(QString());
        LenguaExtranjeraIV->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ventanaAlumnoICC: public Ui_ventanaAlumnoICC {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VENTANAALUMNOICC_H
