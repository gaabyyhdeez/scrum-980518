/********************************************************************************
** Form generated from reading UI file 'ventanamodificacontrasenaalm.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VENTANAMODIFICACONTRASENAALM_H
#define UI_VENTANAMODIFICACONTRASENAALM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_ventanaModificaContrasenaAlm
{
public:
    QLabel *label;
    QPushButton *buttGuardar;
    QLabel *label_2;
    QLineEdit *lineNuevaContra;
    QLabel *label_3;
    QLineEdit *lineNuevaNueva;
    QLabel *label_4;
    QPushButton *buttRegresar;
    QLabel *label_7;

    void setupUi(QDialog *ventanaModificaContrasenaAlm)
    {
        if (ventanaModificaContrasenaAlm->objectName().isEmpty())
            ventanaModificaContrasenaAlm->setObjectName(QStringLiteral("ventanaModificaContrasenaAlm"));
        ventanaModificaContrasenaAlm->resize(489, 251);
        ventanaModificaContrasenaAlm->setStyleSheet(QLatin1String("QPushButton{\n"
"color:white;\n"
"border-radius: 10px;\n"
"background-color:rgb(0, 59, 92) ;\n"
"}\n"
"\n"
" QPushButton:pressed \n"
"{\n"
"     background-color: rgb(167, 191, 255);     \n"
" }"));
        label = new QLabel(ventanaModificaContrasenaAlm);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 50, 171, 31));
        QFont font;
        font.setFamily(QStringLiteral("Segoe UI"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label->setStyleSheet(QStringLiteral("color: rgb(14, 60, 91);"));
        buttGuardar = new QPushButton(ventanaModificaContrasenaAlm);
        buttGuardar->setObjectName(QStringLiteral("buttGuardar"));
        buttGuardar->setGeometry(QRect(360, 190, 91, 31));
        QFont font1;
        font1.setFamily(QStringLiteral("Segoe UI"));
        font1.setPointSize(11);
        font1.setBold(true);
        font1.setWeight(75);
        buttGuardar->setFont(font1);
        label_2 = new QLabel(ventanaModificaContrasenaAlm);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(50, 90, 279, 26));
        QFont font2;
        font2.setFamily(QStringLiteral("Segoe UI"));
        font2.setPointSize(11);
        font2.setBold(false);
        font2.setWeight(50);
        label_2->setFont(font2);
        label_2->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        lineNuevaContra = new QLineEdit(ventanaModificaContrasenaAlm);
        lineNuevaContra->setObjectName(QStringLiteral("lineNuevaContra"));
        lineNuevaContra->setGeometry(QRect(50, 120, 271, 31));
        QFont font3;
        font3.setFamily(QStringLiteral("Segoe UI"));
        font3.setPointSize(11);
        lineNuevaContra->setFont(font3);
        lineNuevaContra->setStyleSheet(QStringLiteral("background-color: rgb(232, 232, 232);"));
        lineNuevaContra->setEchoMode(QLineEdit::Password);
        lineNuevaContra->setClearButtonEnabled(true);
        label_3 = new QLabel(ventanaModificaContrasenaAlm);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(50, 170, 279, 16));
        label_3->setFont(font2);
        label_3->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        lineNuevaNueva = new QLineEdit(ventanaModificaContrasenaAlm);
        lineNuevaNueva->setObjectName(QStringLiteral("lineNuevaNueva"));
        lineNuevaNueva->setGeometry(QRect(50, 190, 271, 31));
        lineNuevaNueva->setFont(font3);
        lineNuevaNueva->setStyleSheet(QStringLiteral("background-color: rgb(232, 232, 232);"));
        lineNuevaNueva->setEchoMode(QLineEdit::Password);
        lineNuevaNueva->setClearButtonEnabled(true);
        label_4 = new QLabel(ventanaModificaContrasenaAlm);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(0, 0, 491, 41));
        label_4->setStyleSheet(QStringLiteral("background-color: rgb(170, 170, 170);"));
        buttRegresar = new QPushButton(ventanaModificaContrasenaAlm);
        buttRegresar->setObjectName(QStringLiteral("buttRegresar"));
        buttRegresar->setGeometry(QRect(10, 10, 41, 23));
        buttRegresar->setStyleSheet(QLatin1String("background-color: none;\n"
"border-image:url(:/west-arrow (1).png);"));
        label_7 = new QLabel(ventanaModificaContrasenaAlm);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(320, -10, 161, 51));
        label_7->setPixmap(QPixmap(QString::fromUtf8(":/FCC.png")));
        label_7->setScaledContents(true);
        label_4->raise();
        label->raise();
        buttGuardar->raise();
        label_2->raise();
        lineNuevaContra->raise();
        label_3->raise();
        lineNuevaNueva->raise();
        buttRegresar->raise();
        label_7->raise();
        QWidget::setTabOrder(lineNuevaContra, lineNuevaNueva);
        QWidget::setTabOrder(lineNuevaNueva, buttGuardar);
        QWidget::setTabOrder(buttGuardar, buttRegresar);

        retranslateUi(ventanaModificaContrasenaAlm);
        QObject::connect(buttRegresar, SIGNAL(clicked()), ventanaModificaContrasenaAlm, SLOT(close()));

        QMetaObject::connectSlotsByName(ventanaModificaContrasenaAlm);
    } // setupUi

    void retranslateUi(QDialog *ventanaModificaContrasenaAlm)
    {
        ventanaModificaContrasenaAlm->setWindowTitle(QApplication::translate("ventanaModificaContrasenaAlm", "Dialog", 0));
        label->setText(QApplication::translate("ventanaModificaContrasenaAlm", "Cambio de Contrase\303\261a", 0));
        buttGuardar->setText(QApplication::translate("ventanaModificaContrasenaAlm", "Guardar", 0));
        label_2->setText(QApplication::translate("ventanaModificaContrasenaAlm", "Introduce tu nueva contrase\303\261a:", 0));
        lineNuevaContra->setPlaceholderText(QString());
        label_3->setText(QApplication::translate("ventanaModificaContrasenaAlm", "Confirma tu nueva contrase\303\261a:", 0));
        label_4->setText(QString());
        buttRegresar->setText(QString());
        label_7->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ventanaModificaContrasenaAlm: public Ui_ventanaModificaContrasenaAlm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VENTANAMODIFICACONTRASENAALM_H
