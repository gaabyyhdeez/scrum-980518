/********************************************************************************
** Form generated from reading UI file 'ventanaadmin.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VENTANAADMIN_H
#define UI_VENTANAADMIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ventanaAdmin
{
public:
    QTabWidget *tabWidget;
    QWidget *tab;
    QLabel *label_4;
    QPushButton *butRegresar;
    QLabel *label_7;
    QLabel *label_10;
    QLabel *label;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QTableView *tablaAlumnos;
    QLineEdit *lineNuevaContra;
    QLabel *label_2;
    QWidget *tab_2;
    QLabel *label_5;
    QPushButton *pushButton_2;
    QLabel *label_6;
    QLabel *label_9;

    void setupUi(QDialog *ventanaAdmin)
    {
        if (ventanaAdmin->objectName().isEmpty())
            ventanaAdmin->setObjectName(QStringLiteral("ventanaAdmin"));
        ventanaAdmin->resize(1366, 748);
        ventanaAdmin->setStyleSheet(QLatin1String("QDialog\n"
"{\n"
"        background-color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"QPushButton\n"
"{\n"
"	background-color: rgb(0, 59, 92);\n"
"	border-radius: 10px;\n"
"	color: white;\n"
"}\n"
"\n"
" QPushButton:pressed \n"
"{\n"
"     background-color: rgb(167, 191, 255);     \n"
" }"));
        tabWidget = new QTabWidget(ventanaAdmin);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(20, 10, 1321, 711));
        tabWidget->setLayoutDirection(Qt::LeftToRight);
        tabWidget->setTabPosition(QTabWidget::North);
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        label_4 = new QLabel(tab);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(0, -10, 1321, 51));
        label_4->setStyleSheet(QStringLiteral("background-color: rgb(170, 170, 170);"));
        butRegresar = new QPushButton(tab);
        butRegresar->setObjectName(QStringLiteral("butRegresar"));
        butRegresar->setGeometry(QRect(10, 10, 41, 23));
        butRegresar->setStyleSheet(QLatin1String("background-color: none;\n"
"border-image:url(:/west-arrow (1).png);"));
        label_7 = new QLabel(tab);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(1030, 20, 241, 81));
        label_7->setPixmap(QPixmap(QString::fromUtf8(":/FCC.png")));
        label_7->setScaledContents(true);
        label_10 = new QLabel(tab);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(410, 90, 871, 51));
        QFont font;
        font.setPointSize(30);
        label_10->setFont(font);
        label = new QLabel(tab);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(40, 80, 361, 71));
        QFont font1;
        font1.setFamily(QStringLiteral("Segoe UI"));
        font1.setPointSize(24);
        font1.setBold(true);
        font1.setWeight(75);
        label->setFont(font1);
        label->setStyleSheet(QStringLiteral("color: rgb(14, 60, 91);"));
        pushButton_3 = new QPushButton(tab);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(780, 210, 111, 31));
        QFont font2;
        font2.setPointSize(11);
        font2.setBold(true);
        font2.setWeight(75);
        pushButton_3->setFont(font2);
        pushButton_4 = new QPushButton(tab);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(540, 600, 231, 41));
        QFont font3;
        font3.setFamily(QStringLiteral("Segoe UI"));
        font3.setPointSize(11);
        font3.setBold(true);
        font3.setWeight(75);
        pushButton_4->setFont(font3);
        tablaAlumnos = new QTableView(tab);
        tablaAlumnos->setObjectName(QStringLiteral("tablaAlumnos"));
        tablaAlumnos->setGeometry(QRect(470, 280, 371, 301));
        tablaAlumnos->setStyleSheet(QStringLiteral("font: 10pt \"MS Shell Dlg 2\";"));
        lineNuevaContra = new QLineEdit(tab);
        lineNuevaContra->setObjectName(QStringLiteral("lineNuevaContra"));
        lineNuevaContra->setGeometry(QRect(500, 210, 271, 31));
        QFont font4;
        font4.setFamily(QStringLiteral("Segoe UI"));
        font4.setPointSize(11);
        lineNuevaContra->setFont(font4);
        lineNuevaContra->setStyleSheet(QStringLiteral("background-color: rgb(232, 232, 232);"));
        lineNuevaContra->setEchoMode(QLineEdit::Normal);
        lineNuevaContra->setClearButtonEnabled(true);
        label_2 = new QLabel(tab);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(430, 210, 71, 31));
        label_2->setFont(font3);
        tabWidget->addTab(tab, QString());
        label_10->raise();
        label_4->raise();
        butRegresar->raise();
        label->raise();
        label_7->raise();
        pushButton_3->raise();
        pushButton_4->raise();
        tablaAlumnos->raise();
        lineNuevaContra->raise();
        label_2->raise();
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        label_5 = new QLabel(tab_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(-70, -10, 1391, 51));
        label_5->setStyleSheet(QStringLiteral("background-color: rgb(170, 170, 170);"));
        pushButton_2 = new QPushButton(tab_2);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(10, 10, 41, 23));
        pushButton_2->setStyleSheet(QLatin1String("background-color: none;\n"
"border-image:url(:/west-arrow (1).png);"));
        label_6 = new QLabel(tab_2);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(1030, 30, 241, 81));
        label_6->setPixmap(QPixmap(QString::fromUtf8(":/FCC.png")));
        label_6->setScaledContents(true);
        label_9 = new QLabel(tab_2);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(20, 70, 821, 51));
        label_9->setFont(font);
        tabWidget->addTab(tab_2, QString());

        retranslateUi(ventanaAdmin);
        QObject::connect(butRegresar, SIGNAL(clicked()), ventanaAdmin, SLOT(close()));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(ventanaAdmin);
    } // setupUi

    void retranslateUi(QDialog *ventanaAdmin)
    {
        ventanaAdmin->setWindowTitle(QApplication::translate("ventanaAdmin", "Dialog", 0));
        label_4->setText(QString());
        butRegresar->setText(QString());
        label_7->setText(QString());
        label_10->setText(QApplication::translate("ventanaAdmin", "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -", 0));
        label->setText(QApplication::translate("ventanaAdmin", "Restablecer Contrase\303\261a", 0));
        pushButton_3->setText(QApplication::translate("ventanaAdmin", "Buscar", 0));
        pushButton_4->setText(QApplication::translate("ventanaAdmin", "Restablecer Contrase\303\261a", 0));
        lineNuevaContra->setPlaceholderText(QString());
        label_2->setText(QApplication::translate("ventanaAdmin", "Alumno:", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("ventanaAdmin", "Restablecer Contrase\303\261a", 0));
        label_5->setText(QString());
        pushButton_2->setText(QString());
        label_6->setText(QString());
        label_9->setText(QApplication::translate("ventanaAdmin", "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("ventanaAdmin", "Generar Reporte", 0));
    } // retranslateUi

};

namespace Ui {
    class ventanaAdmin: public Ui_ventanaAdmin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VENTANAADMIN_H
