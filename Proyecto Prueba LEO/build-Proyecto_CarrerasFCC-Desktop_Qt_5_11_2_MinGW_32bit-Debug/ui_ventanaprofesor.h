/********************************************************************************
** Form generated from reading UI file 'ventanaprofesor.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VENTANAPROFESOR_H
#define UI_VENTANAPROFESOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ventanaProfesor
{
public:
    QLabel *label_2;
    QLabel *label;
    QLabel *label_4;
    QPushButton *pushButton;
    QLabel *label_10;
    QLabel *label_7;
    QTableView *tablaTutorados;
    QLabel *label_11;
    QLabel *label_13;
    QLabel *label_3;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label_8;
    QLabel *labIDTutor;
    QLabel *label_5;
    QLabel *label_NombreTutor;
    QLabel *label_9;
    QLabel *labSeccion;

    void setupUi(QDialog *ventanaProfesor)
    {
        if (ventanaProfesor->objectName().isEmpty())
            ventanaProfesor->setObjectName(QStringLiteral("ventanaProfesor"));
        ventanaProfesor->resize(1366, 748);
        ventanaProfesor->setStyleSheet(QLatin1String("QPushButton{\n"
"color:white;\n"
"border-radius: 10px;\n"
"background-color:rgb(0, 59, 92) ;\n"
"}\n"
"\n"
" QPushButton:pressed\n"
"{\n"
"     background-color: rgb(167, 191, 255);\n"
" }"));
        label_2 = new QLabel(ventanaProfesor);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(840, 250, 171, 41));
        QFont font;
        font.setFamily(QStringLiteral("Segoe UI"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label_2->setFont(font);
        label = new QLabel(ventanaProfesor);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 100, 141, 31));
        QFont font1;
        font1.setFamily(QStringLiteral("Segoe UI"));
        font1.setPointSize(24);
        font1.setBold(true);
        font1.setItalic(false);
        font1.setWeight(75);
        label->setFont(font1);
        label->setStyleSheet(QStringLiteral("color: rgb(14, 60, 91);"));
        label->setAlignment(Qt::AlignCenter);
        label_4 = new QLabel(ventanaProfesor);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(0, -10, 1591, 51));
        label_4->setStyleSheet(QStringLiteral("background-color: rgb(170, 170, 170);"));
        pushButton = new QPushButton(ventanaProfesor);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(10, 10, 41, 23));
        pushButton->setStyleSheet(QLatin1String("background-color: none;\n"
"border-image:url(:/west-arrow (1).png);"));
        label_10 = new QLabel(ventanaProfesor);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(150, 90, 1171, 51));
        QFont font2;
        font2.setPointSize(30);
        label_10->setFont(font2);
        label_7 = new QLabel(ventanaProfesor);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(1080, 20, 241, 81));
        label_7->setPixmap(QPixmap(QString::fromUtf8(":/FCC.png")));
        label_7->setScaledContents(true);
        tablaTutorados = new QTableView(ventanaProfesor);
        tablaTutorados->setObjectName(QStringLiteral("tablaTutorados"));
        tablaTutorados->setGeometry(QRect(750, 310, 371, 321));
        tablaTutorados->setStyleSheet(QLatin1String("font: 10pt \"MS Shell Dlg 2\";\n"
""));
        label_11 = new QLabel(ventanaProfesor);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(270, 270, 371, 361));
        label_11->setStyleSheet(QLatin1String("background-color: rgb(235, 235, 235);\n"
"border-radius: 10px;\n"
"color: white;alternate-background-color: rgb(222, 222, 222);"));
        label_13 = new QLabel(ventanaProfesor);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(270, 270, 371, 361));
        label_13->setStyleSheet(QLatin1String("border-radius: 10px;\n"
"border: 2px solid #003b5c;"));
        label_3 = new QLabel(ventanaProfesor);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(360, 290, 211, 51));
        QFont font3;
        font3.setFamily(QStringLiteral("Segoe UI"));
        font3.setPointSize(15);
        font3.setBold(true);
        font3.setWeight(75);
        label_3->setFont(font3);
        label_3->setStyleSheet(QStringLiteral("color: rgb(0, 59, 92) "));
        layoutWidget = new QWidget(ventanaProfesor);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(270, 360, 371, 241));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_8 = new QLabel(layoutWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        QFont font4;
        font4.setFamily(QStringLiteral("Segoe UI"));
        font4.setPointSize(11);
        font4.setBold(true);
        font4.setItalic(false);
        font4.setWeight(75);
        label_8->setFont(font4);
        label_8->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        verticalLayout->addWidget(label_8);

        labIDTutor = new QLabel(layoutWidget);
        labIDTutor->setObjectName(QStringLiteral("labIDTutor"));
        QFont font5;
        font5.setFamily(QStringLiteral("Segoe UI"));
        font5.setPointSize(13);
        font5.setBold(false);
        font5.setItalic(false);
        font5.setWeight(50);
        labIDTutor->setFont(font5);
        labIDTutor->setStyleSheet(QStringLiteral(""));
        labIDTutor->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(labIDTutor);

        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        QFont font6;
        font6.setFamily(QStringLiteral("Segoe UI"));
        font6.setPointSize(11);
        font6.setBold(true);
        font6.setWeight(75);
        label_5->setFont(font6);
        label_5->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        verticalLayout->addWidget(label_5);

        label_NombreTutor = new QLabel(layoutWidget);
        label_NombreTutor->setObjectName(QStringLiteral("label_NombreTutor"));
        QFont font7;
        font7.setFamily(QStringLiteral("Segoe UI"));
        font7.setPointSize(13);
        font7.setBold(false);
        font7.setWeight(50);
        label_NombreTutor->setFont(font7);
        label_NombreTutor->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_NombreTutor);

        label_9 = new QLabel(layoutWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setFont(font6);
        label_9->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        verticalLayout->addWidget(label_9);

        labSeccion = new QLabel(layoutWidget);
        labSeccion->setObjectName(QStringLiteral("labSeccion"));
        labSeccion->setFont(font7);
        labSeccion->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(labSeccion);

        label_11->raise();
        layoutWidget->raise();
        label_2->raise();
        label->raise();
        label_4->raise();
        pushButton->raise();
        label_10->raise();
        label_7->raise();
        tablaTutorados->raise();
        label_3->raise();
        label_13->raise();

        retranslateUi(ventanaProfesor);
        QObject::connect(pushButton, SIGNAL(clicked()), ventanaProfesor, SLOT(close()));

        QMetaObject::connectSlotsByName(ventanaProfesor);
    } // setupUi

    void retranslateUi(QDialog *ventanaProfesor)
    {
        ventanaProfesor->setWindowTitle(QApplication::translate("ventanaProfesor", "Dialog", nullptr));
        label_2->setText(QApplication::translate("ventanaProfesor", "Buscador de Alumnos", nullptr));
        label->setText(QApplication::translate("ventanaProfesor", "Tutor", nullptr));
        label_4->setText(QString());
        pushButton->setText(QString());
        label_10->setText(QApplication::translate("ventanaProfesor", "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -", nullptr));
        label_7->setText(QString());
        label_11->setText(QString());
        label_13->setText(QString());
        label_3->setText(QApplication::translate("ventanaProfesor", "Informaci\303\263n Personal", nullptr));
        label_8->setText(QApplication::translate("ventanaProfesor", "ID", nullptr));
        labIDTutor->setText(QApplication::translate("ventanaProfesor", "201664822", nullptr));
        label_5->setText(QApplication::translate("ventanaProfesor", "Nombre", nullptr));
        label_NombreTutor->setText(QApplication::translate("ventanaProfesor", "Juana In\303\251s de Asbaje y Ram\303\255rez de Santillana", nullptr));
        label_9->setText(QApplication::translate("ventanaProfesor", "Secci\303\263n", nullptr));
        labSeccion->setText(QApplication::translate("ventanaProfesor", "Juana In\303\251s de Asbaje y Ram\303\255rez de Santillana", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ventanaProfesor: public Ui_ventanaProfesor {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VENTANAPROFESOR_H
