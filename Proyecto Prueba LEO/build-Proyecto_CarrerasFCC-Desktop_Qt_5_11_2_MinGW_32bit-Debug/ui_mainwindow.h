/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLineEdit *lineUsuario;
    QLabel *label_4;
    QLabel *label_5;
    QLineEdit *lineContrasenia;
    QPushButton *butIngresar;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QRadioButton *rBut_Alumno;
    QRadioButton *rBut_Profesor;
    QRadioButton *rBut_Coordinador;
    QRadioButton *rBut_Admin;
    QLabel *label_6;
    QLabel *label_7;
    QPushButton *Salir;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1366, 748);
        QFont font;
        font.setFamily(QStringLiteral("Segoe UI"));
        font.setBold(true);
        font.setWeight(75);
        MainWindow->setFont(font);
        MainWindow->setStyleSheet(QLatin1String("QMainWindow\n"
"{\n"
"		background: url(:/PicsArt_10-15-09.04.54.jpg);\n"
"}\n"
"\n"
"QPushButton#Salir\n"
"{\n"
"\n"
"	border-style: inset;\n"
"\n"
"\n"
"}\n"
""));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(680, 320, 491, 361));
        label->setStyleSheet(QLatin1String("border-radius: 25px;\n"
"border-image:url(:/background_white_transparent_1.png);"));
        label->setScaledContents(true);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(760, 470, 41, 41));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/loginUser.png")));
        label_2->setScaledContents(true);
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(760, 530, 41, 41));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/password2.png")));
        label_3->setScaledContents(true);
        lineUsuario = new QLineEdit(centralWidget);
        lineUsuario->setObjectName(QStringLiteral("lineUsuario"));
        lineUsuario->setGeometry(QRect(810, 470, 271, 41));
        QFont font1;
        font1.setFamily(QStringLiteral("Segoe UI"));
        font1.setPointSize(11);
        font1.setBold(true);
        font1.setWeight(75);
        lineUsuario->setFont(font1);
        lineUsuario->setStyleSheet(QLatin1String("background-color: transparent;\n"
"border: transparent;"));
        lineUsuario->setClearButtonEnabled(true);
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(730, 460, 391, 61));
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/Twitter_Device_Mobile_Floating_forWEB.png")));
        label_4->setScaledContents(true);
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(730, 520, 391, 61));
        label_5->setPixmap(QPixmap(QString::fromUtf8(":/Twitter_Device_Mobile_Floating_forWEB.png")));
        label_5->setScaledContents(true);
        lineContrasenia = new QLineEdit(centralWidget);
        lineContrasenia->setObjectName(QStringLiteral("lineContrasenia"));
        lineContrasenia->setGeometry(QRect(810, 530, 271, 41));
        lineContrasenia->setFont(font1);
        lineContrasenia->setStyleSheet(QLatin1String("background-color: transparent;\n"
"border: transparent;"));
        lineContrasenia->setEchoMode(QLineEdit::Password);
        lineContrasenia->setClearButtonEnabled(true);
        butIngresar = new QPushButton(centralWidget);
        butIngresar->setObjectName(QStringLiteral("butIngresar"));
        butIngresar->setGeometry(QRect(770, 600, 311, 41));
        QFont font2;
        font2.setFamily(QStringLiteral("Segoe UI"));
        font2.setPointSize(13);
        font2.setBold(true);
        font2.setWeight(75);
        butIngresar->setFont(font2);
        butIngresar->setStyleSheet(QLatin1String("QPushButton\n"
"{\n"
"	background-color: rgb(0, 59, 92);\n"
"	border-radius: 10px;\n"
"	color: white;\n"
"}\n"
"\n"
" QPushButton:pressed \n"
"{\n"
"	border-style:transparent;\n"
"     background-color: rgb(167, 191, 255);     \n"
" }"));
        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(710, 420, 455, 41));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        rBut_Alumno = new QRadioButton(horizontalLayoutWidget);
        rBut_Alumno->setObjectName(QStringLiteral("rBut_Alumno"));
        QFont font3;
        font3.setFamily(QStringLiteral("Segoe UI"));
        font3.setPointSize(10);
        font3.setBold(true);
        font3.setWeight(75);
        rBut_Alumno->setFont(font3);
        rBut_Alumno->setChecked(true);

        horizontalLayout->addWidget(rBut_Alumno);

        rBut_Profesor = new QRadioButton(horizontalLayoutWidget);
        rBut_Profesor->setObjectName(QStringLiteral("rBut_Profesor"));
        QFont font4;
        font4.setFamily(QStringLiteral("Segoe UI"));
        font4.setPointSize(10);
        font4.setBold(true);
        font4.setItalic(false);
        font4.setWeight(75);
        rBut_Profesor->setFont(font4);

        horizontalLayout->addWidget(rBut_Profesor);

        rBut_Coordinador = new QRadioButton(horizontalLayoutWidget);
        rBut_Coordinador->setObjectName(QStringLiteral("rBut_Coordinador"));
        rBut_Coordinador->setFont(font3);

        horizontalLayout->addWidget(rBut_Coordinador);

        rBut_Admin = new QRadioButton(horizontalLayoutWidget);
        rBut_Admin->setObjectName(QStringLiteral("rBut_Admin"));
        QFont font5;
        font5.setFamily(QStringLiteral("Segoe UI"));
        font5.setPointSize(10);
        font5.setBold(true);
        font5.setItalic(false);
        font5.setWeight(75);
        font5.setStrikeOut(false);
        rBut_Admin->setFont(font5);

        horizontalLayout->addWidget(rBut_Admin);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(770, 340, 361, 51));
        QFont font6;
        font6.setFamily(QStringLiteral("Segoe UI"));
        font6.setPointSize(28);
        font6.setBold(true);
        font6.setWeight(75);
        label_6->setFont(font6);
        label_6->setStyleSheet(QLatin1String("color: rgb(0, 59, 92);\n"
""));
        label_6->setAlignment(Qt::AlignCenter);
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(0, 0, 1366, 748));
        label_7->setPixmap(QPixmap(QString::fromUtf8(":/PicsArt_10-16-03.43.12.jpg")));
        label_7->setScaledContents(true);
        Salir = new QPushButton(centralWidget);
        Salir->setObjectName(QStringLiteral("Salir"));
        Salir->setGeometry(QRect(1250, 30, 71, 61));
        Salir->setStyleSheet(QStringLiteral(""));
        QIcon icon;
        icon.addFile(QStringLiteral("../../../../NUEVO QT/MMM/imagen/SALIR.png"), QSize(), QIcon::Normal, QIcon::Off);
        Salir->setIcon(icon);
        Salir->setIconSize(QSize(70, 60));
        MainWindow->setCentralWidget(centralWidget);
        label_7->raise();
        label->raise();
        label_4->raise();
        lineUsuario->raise();
        label_2->raise();
        label_5->raise();
        label_3->raise();
        lineContrasenia->raise();
        butIngresar->raise();
        horizontalLayoutWidget->raise();
        label_6->raise();
        Salir->raise();

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label->setText(QString());
        label_2->setText(QString());
        label_3->setText(QString());
        label_4->setText(QString());
        label_5->setText(QString());
        butIngresar->setText(QApplication::translate("MainWindow", "Ingresar", nullptr));
        rBut_Alumno->setText(QApplication::translate("MainWindow", "Alumno", nullptr));
        rBut_Profesor->setText(QApplication::translate("MainWindow", "Tutor", nullptr));
        rBut_Coordinador->setText(QApplication::translate("MainWindow", "Coordinador", nullptr));
        rBut_Admin->setText(QApplication::translate("MainWindow", "Administrador", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "AUTENTICARSE", nullptr));
        label_7->setText(QString());
        Salir->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
