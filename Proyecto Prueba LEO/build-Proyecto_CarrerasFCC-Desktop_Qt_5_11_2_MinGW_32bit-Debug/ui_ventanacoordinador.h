/********************************************************************************
** Form generated from reading UI file 'ventanacoordinador.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VENTANACOORDINADOR_H
#define UI_VENTANACOORDINADOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>

QT_BEGIN_NAMESPACE

class Ui_ventanaCoordinador
{
public:
    QLabel *label_2;
    QLabel *label;
    QLineEdit *lineIDGrupo;
    QPushButton *buttBuscarGrupo;
    QLabel *label_6;
    QPushButton *buttBuscar2;
    QLabel *label_3;
    QLineEdit *lineIDProfesor;
    QLabel *label_7;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_9;
    QPushButton *pushButton;
    QTableView *tablaGrupos;
    QTableView *tablaProfesores;
    QPushButton *buttAsignar;

    void setupUi(QDialog *ventanaCoordinador)
    {
        if (ventanaCoordinador->objectName().isEmpty())
            ventanaCoordinador->setObjectName(QStringLiteral("ventanaCoordinador"));
        ventanaCoordinador->resize(1366, 748);
        ventanaCoordinador->setStyleSheet(QLatin1String("QPushButton{\n"
"color:white;\n"
"border-radius: 10px;\n"
"background-color:rgb(0, 59, 92) ;\n"
"}\n"
"\n"
" QPushButton:pressed\n"
"{\n"
"     background-color: rgb(167, 191, 255);\n"
" }"));
        label_2 = new QLabel(ventanaCoordinador);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(270, 210, 141, 41));
        QFont font;
        font.setFamily(QStringLiteral("Segoe UI"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label_2->setFont(font);
        label_2->setStyleSheet(QStringLiteral(""));
        label = new QLabel(ventanaCoordinador);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(50, 80, 201, 71));
        QFont font1;
        font1.setFamily(QStringLiteral("Segoe UI"));
        font1.setPointSize(24);
        font1.setBold(true);
        font1.setItalic(false);
        font1.setWeight(75);
        label->setFont(font1);
        label->setStyleSheet(QStringLiteral("color: rgb(14, 60, 91);"));
        label->setAlignment(Qt::AlignCenter);
        lineIDGrupo = new QLineEdit(ventanaCoordinador);
        lineIDGrupo->setObjectName(QStringLiteral("lineIDGrupo"));
        lineIDGrupo->setGeometry(QRect(260, 290, 201, 31));
        QFont font2;
        font2.setFamily(QStringLiteral("Segoe UI"));
        font2.setPointSize(11);
        lineIDGrupo->setFont(font2);
        lineIDGrupo->setStyleSheet(QStringLiteral("background-color: rgb(232, 232, 232);"));
        buttBuscarGrupo = new QPushButton(ventanaCoordinador);
        buttBuscarGrupo->setObjectName(QStringLiteral("buttBuscarGrupo"));
        buttBuscarGrupo->setGeometry(QRect(470, 290, 111, 31));
        QFont font3;
        font3.setFamily(QStringLiteral("Segoe UI"));
        font3.setPointSize(11);
        font3.setBold(true);
        font3.setWeight(75);
        buttBuscarGrupo->setFont(font3);
        buttBuscarGrupo->setStyleSheet(QStringLiteral(""));
        label_6 = new QLabel(ventanaCoordinador);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(110, 290, 141, 31));
        label_6->setFont(font3);
        label_6->setStyleSheet(QStringLiteral(""));
        label_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        buttBuscar2 = new QPushButton(ventanaCoordinador);
        buttBuscar2->setObjectName(QStringLiteral("buttBuscar2"));
        buttBuscar2->setGeometry(QRect(1100, 290, 111, 31));
        buttBuscar2->setFont(font3);
        buttBuscar2->setStyleSheet(QStringLiteral(""));
        label_3 = new QLabel(ventanaCoordinador);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(930, 210, 161, 41));
        label_3->setFont(font);
        label_3->setStyleSheet(QStringLiteral(""));
        lineIDProfesor = new QLineEdit(ventanaCoordinador);
        lineIDProfesor->setObjectName(QStringLiteral("lineIDProfesor"));
        lineIDProfesor->setGeometry(QRect(900, 290, 191, 31));
        lineIDProfesor->setFont(font2);
        lineIDProfesor->setStyleSheet(QStringLiteral("background-color: rgb(232, 232, 232);"));
        label_7 = new QLabel(ventanaCoordinador);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(800, 290, 91, 31));
        label_7->setFont(font3);
        label_7->setStyleSheet(QStringLiteral(""));
        label_7->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_4 = new QLabel(ventanaCoordinador);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(-10, -10, 1411, 51));
        label_4->setStyleSheet(QStringLiteral("background-color: rgb(170, 170, 170);"));
        label_5 = new QLabel(ventanaCoordinador);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(1080, 20, 241, 81));
        label_5->setPixmap(QPixmap(QString::fromUtf8(":/FCC.png")));
        label_5->setScaledContents(true);
        label_9 = new QLabel(ventanaCoordinador);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(270, 90, 1101, 51));
        QFont font4;
        font4.setPointSize(30);
        label_9->setFont(font4);
        pushButton = new QPushButton(ventanaCoordinador);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(10, 10, 41, 23));
        pushButton->setStyleSheet(QLatin1String("background-color: none;\n"
"border-image:url(:/west-arrow (1).png);"));
        tablaGrupos = new QTableView(ventanaCoordinador);
        tablaGrupos->setObjectName(QStringLiteral("tablaGrupos"));
        tablaGrupos->setGeometry(QRect(210, 340, 291, 271));
        tablaGrupos->setStyleSheet(QStringLiteral("font: 10pt \"MS Shell Dlg 2\";"));
        tablaProfesores = new QTableView(ventanaCoordinador);
        tablaProfesores->setObjectName(QStringLiteral("tablaProfesores"));
        tablaProfesores->setGeometry(QRect(790, 340, 471, 271));
        tablaProfesores->setStyleSheet(QStringLiteral("font: 10pt \"MS Shell Dlg 2\";"));
        buttAsignar = new QPushButton(ventanaCoordinador);
        buttAsignar->setObjectName(QStringLiteral("buttAsignar"));
        buttAsignar->setGeometry(QRect(560, 630, 111, 31));
        buttAsignar->setFont(font3);
        buttAsignar->setStyleSheet(QStringLiteral(""));
        label_4->raise();
        label_2->raise();
        label->raise();
        lineIDGrupo->raise();
        buttBuscarGrupo->raise();
        label_6->raise();
        buttBuscar2->raise();
        label_3->raise();
        lineIDProfesor->raise();
        label_7->raise();
        label_5->raise();
        label_9->raise();
        pushButton->raise();
        tablaGrupos->raise();
        tablaProfesores->raise();
        buttAsignar->raise();

        retranslateUi(ventanaCoordinador);
        QObject::connect(pushButton, SIGNAL(clicked()), ventanaCoordinador, SLOT(close()));

        QMetaObject::connectSlotsByName(ventanaCoordinador);
    } // setupUi

    void retranslateUi(QDialog *ventanaCoordinador)
    {
        ventanaCoordinador->setWindowTitle(QApplication::translate("ventanaCoordinador", "Dialog", nullptr));
        label_2->setText(QApplication::translate("ventanaCoordinador", "Buscador Secci\303\263n", nullptr));
        label->setText(QApplication::translate("ventanaCoordinador", "Coordinador", nullptr));
        buttBuscarGrupo->setText(QApplication::translate("ventanaCoordinador", "Buscar", nullptr));
        label_6->setText(QApplication::translate("ventanaCoordinador", "Nombre de Secci\303\263n:", nullptr));
        buttBuscar2->setText(QApplication::translate("ventanaCoordinador", "Buscar", nullptr));
        label_3->setText(QApplication::translate("ventanaCoordinador", "Buscador Profesores", nullptr));
        label_7->setText(QApplication::translate("ventanaCoordinador", "ID Profesor:", nullptr));
        label_4->setText(QString());
        label_5->setText(QString());
        label_9->setText(QApplication::translate("ventanaCoordinador", "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -", nullptr));
        pushButton->setText(QString());
        buttAsignar->setText(QApplication::translate("ventanaCoordinador", "Asignar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ventanaCoordinador: public Ui_ventanaCoordinador {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VENTANACOORDINADOR_H
