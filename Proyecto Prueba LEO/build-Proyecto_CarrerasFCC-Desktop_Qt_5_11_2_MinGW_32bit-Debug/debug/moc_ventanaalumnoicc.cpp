/****************************************************************************
** Meta object code from reading C++ file 'ventanaalumnoicc.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Proyecto_CarrerasFCC/ventanaalumnoicc.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ventanaalumnoicc.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ventanaAlumnoICC_t {
    QByteArrayData data[36];
    char stringdata0[1013];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ventanaAlumnoICC_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ventanaAlumnoICC_t qt_meta_stringdata_ventanaAlumnoICC = {
    {
QT_MOC_LITERAL(0, 0, 16), // "ventanaAlumnoICC"
QT_MOC_LITERAL(1, 17, 23), // "on_buttRegresar_clicked"
QT_MOC_LITERAL(2, 41, 0), // ""
QT_MOC_LITERAL(3, 42, 22), // "on_Matematicas_clicked"
QT_MOC_LITERAL(4, 65, 7), // "checked"
QT_MOC_LITERAL(5, 73, 29), // "on_CalculoDiferencial_clicked"
QT_MOC_LITERAL(6, 103, 18), // "on_FisicaI_clicked"
QT_MOC_LITERAL(7, 122, 26), // "on_CalculoIntegral_clicked"
QT_MOC_LITERAL(8, 149, 26), // "on_AlgebraSuperior_clicked"
QT_MOC_LITERAL(9, 176, 38), // "on_MetodologiaDeLaProgramacio..."
QT_MOC_LITERAL(10, 215, 28), // "on_LenguaExtranjeraI_clicked"
QT_MOC_LITERAL(11, 244, 56), // "on_AlgebraLinealConElementosD..."
QT_MOC_LITERAL(12, 301, 24), // "on_ProgramacionI_clicked"
QT_MOC_LITERAL(13, 326, 29), // "on_LenguaExtranjeraII_clicked"
QT_MOC_LITERAL(14, 356, 19), // "on_FisicaII_clicked"
QT_MOC_LITERAL(15, 376, 31), // "on_MatematicasDiscretas_clicked"
QT_MOC_LITERAL(16, 408, 25), // "on_ProgramacionII_clicked"
QT_MOC_LITERAL(17, 434, 22), // "on_Ensamblador_clicked"
QT_MOC_LITERAL(18, 457, 30), // "on_LenguaExtranjeraIII_clicked"
QT_MOC_LITERAL(19, 488, 34), // "on_EcuacionesDiferenciales_cl..."
QT_MOC_LITERAL(20, 523, 30), // "on_CircuitosElectricos_clicked"
QT_MOC_LITERAL(21, 554, 22), // "on_Graficacion_clicked"
QT_MOC_LITERAL(22, 577, 29), // "on_EstructurasDeDatos_clicked"
QT_MOC_LITERAL(23, 607, 29), // "on_LenguaExtranjeraIV_clicked"
QT_MOC_LITERAL(24, 637, 35), // "on_ProbabilidadyEstadistica_c..."
QT_MOC_LITERAL(25, 673, 32), // "on_CircuitosElectronicos_clicked"
QT_MOC_LITERAL(26, 706, 30), // "on_SistemasOperativosI_clicked"
QT_MOC_LITERAL(27, 737, 37), // "on_AnalisisyDiseoDeAlgoritmos..."
QT_MOC_LITERAL(28, 775, 33), // "on_FormacionHumanaySocial_cli..."
QT_MOC_LITERAL(29, 809, 31), // "on_IngenieriaDeSoftware_clicked"
QT_MOC_LITERAL(30, 841, 15), // "on_Dhpc_clicked"
QT_MOC_LITERAL(31, 857, 24), // "on_ModeloDeRedes_clicked"
QT_MOC_LITERAL(32, 882, 24), // "on_DisenoDigital_clicked"
QT_MOC_LITERAL(33, 907, 36), // "on_BaseDeDatosParaIngenieria_..."
QT_MOC_LITERAL(34, 944, 31), // "on_SistemasOperativosII_clicked"
QT_MOC_LITERAL(35, 976, 36) // "on_AdministracionDeProyectos_..."

    },
    "ventanaAlumnoICC\0on_buttRegresar_clicked\0"
    "\0on_Matematicas_clicked\0checked\0"
    "on_CalculoDiferencial_clicked\0"
    "on_FisicaI_clicked\0on_CalculoIntegral_clicked\0"
    "on_AlgebraSuperior_clicked\0"
    "on_MetodologiaDeLaProgramacion_clicked\0"
    "on_LenguaExtranjeraI_clicked\0"
    "on_AlgebraLinealConElementosDeGeometriaAnalitica_clicked\0"
    "on_ProgramacionI_clicked\0"
    "on_LenguaExtranjeraII_clicked\0"
    "on_FisicaII_clicked\0on_MatematicasDiscretas_clicked\0"
    "on_ProgramacionII_clicked\0"
    "on_Ensamblador_clicked\0"
    "on_LenguaExtranjeraIII_clicked\0"
    "on_EcuacionesDiferenciales_clicked\0"
    "on_CircuitosElectricos_clicked\0"
    "on_Graficacion_clicked\0"
    "on_EstructurasDeDatos_clicked\0"
    "on_LenguaExtranjeraIV_clicked\0"
    "on_ProbabilidadyEstadistica_clicked\0"
    "on_CircuitosElectronicos_clicked\0"
    "on_SistemasOperativosI_clicked\0"
    "on_AnalisisyDiseoDeAlgoritmos_clicked\0"
    "on_FormacionHumanaySocial_clicked\0"
    "on_IngenieriaDeSoftware_clicked\0"
    "on_Dhpc_clicked\0on_ModeloDeRedes_clicked\0"
    "on_DisenoDigital_clicked\0"
    "on_BaseDeDatosParaIngenieria_clicked\0"
    "on_SistemasOperativosII_clicked\0"
    "on_AdministracionDeProyectos_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ventanaAlumnoICC[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      33,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  179,    2, 0x08 /* Private */,
       3,    1,  180,    2, 0x08 /* Private */,
       5,    1,  183,    2, 0x08 /* Private */,
       6,    1,  186,    2, 0x08 /* Private */,
       7,    1,  189,    2, 0x08 /* Private */,
       8,    1,  192,    2, 0x08 /* Private */,
       9,    1,  195,    2, 0x08 /* Private */,
      10,    1,  198,    2, 0x08 /* Private */,
      11,    1,  201,    2, 0x08 /* Private */,
      12,    1,  204,    2, 0x08 /* Private */,
      13,    1,  207,    2, 0x08 /* Private */,
      14,    1,  210,    2, 0x08 /* Private */,
      15,    1,  213,    2, 0x08 /* Private */,
      16,    1,  216,    2, 0x08 /* Private */,
      17,    1,  219,    2, 0x08 /* Private */,
      18,    1,  222,    2, 0x08 /* Private */,
      19,    1,  225,    2, 0x08 /* Private */,
      20,    1,  228,    2, 0x08 /* Private */,
      21,    1,  231,    2, 0x08 /* Private */,
      22,    1,  234,    2, 0x08 /* Private */,
      23,    1,  237,    2, 0x08 /* Private */,
      24,    1,  240,    2, 0x08 /* Private */,
      25,    1,  243,    2, 0x08 /* Private */,
      26,    1,  246,    2, 0x08 /* Private */,
      27,    1,  249,    2, 0x08 /* Private */,
      28,    1,  252,    2, 0x08 /* Private */,
      29,    1,  255,    2, 0x08 /* Private */,
      30,    1,  258,    2, 0x08 /* Private */,
      31,    1,  261,    2, 0x08 /* Private */,
      32,    1,  264,    2, 0x08 /* Private */,
      33,    1,  267,    2, 0x08 /* Private */,
      34,    1,  270,    2, 0x08 /* Private */,
      35,    1,  273,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,

       0        // eod
};

void ventanaAlumnoICC::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ventanaAlumnoICC *_t = static_cast<ventanaAlumnoICC *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_buttRegresar_clicked(); break;
        case 1: _t->on_Matematicas_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->on_CalculoDiferencial_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->on_FisicaI_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->on_CalculoIntegral_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->on_AlgebraSuperior_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->on_MetodologiaDeLaProgramacion_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->on_LenguaExtranjeraI_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->on_AlgebraLinealConElementosDeGeometriaAnalitica_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->on_ProgramacionI_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->on_LenguaExtranjeraII_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->on_FisicaII_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->on_MatematicasDiscretas_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: _t->on_ProgramacionII_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->on_Ensamblador_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: _t->on_LenguaExtranjeraIII_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: _t->on_EcuacionesDiferenciales_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 17: _t->on_CircuitosElectricos_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->on_Graficacion_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 19: _t->on_EstructurasDeDatos_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 20: _t->on_LenguaExtranjeraIV_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 21: _t->on_ProbabilidadyEstadistica_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 22: _t->on_CircuitosElectronicos_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 23: _t->on_SistemasOperativosI_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 24: _t->on_AnalisisyDiseoDeAlgoritmos_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: _t->on_FormacionHumanaySocial_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 26: _t->on_IngenieriaDeSoftware_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 27: _t->on_Dhpc_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 28: _t->on_ModeloDeRedes_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 29: _t->on_DisenoDigital_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 30: _t->on_BaseDeDatosParaIngenieria_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 31: _t->on_SistemasOperativosII_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 32: _t->on_AdministracionDeProyectos_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ventanaAlumnoICC::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ventanaAlumnoICC.data,
      qt_meta_data_ventanaAlumnoICC,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ventanaAlumnoICC::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ventanaAlumnoICC::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ventanaAlumnoICC.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int ventanaAlumnoICC::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 33)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 33;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 33)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 33;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
