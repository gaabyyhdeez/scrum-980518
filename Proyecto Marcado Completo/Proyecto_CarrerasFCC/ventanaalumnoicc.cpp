#include "ventanaalumnoicc.h"
#include "ui_ventanaalumnoicc.h"
#include <QMessageBox>

ventanaAlumnoICC::ventanaAlumnoICC(QWidget *parent, int matricula) :
    QDialog(parent),
    ui(new Ui::ventanaAlumnoICC)
{   
    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose,true);

    matricula2 = matricula;
    qDebug() << matricula2;

    QSqlQuery datosAlumno;
    datosAlumno.prepare("SELECT matricula, nombre FROM Alumno WHERE matricula="+QString::number(matricula2)+"");
    datosAlumno.exec();
    datosAlumno.next();

    qDebug() << datosAlumno.result();
    ui->label_NombreMatri->setText(datosAlumno.value(0).toString());
    ui->label_NombreAlm->setText(datosAlumno.value(1).toString());

    //primer semestre
    ui->Matematicas->setVisible(true);
    ui->AlgebraSuperior->setVisible(true);
    ui->MetodologiaDeLaProgramacion->setVisible(true);
    ui->LenguaExtranjeraI->setVisible(true);
    ui->FormacionHumanaySocial->setVisible(true);
    //segundo semestre
    ui->CalculoDiferencial->setVisible(false);
    ui->FisicaI->setVisible(false);
    ui->AlgebraLinealConElementosDeGeometriaAnalitica->setVisible(false);
    ui->ProgramacionI->setVisible(false);
    ui->LenguaExtranjeraII->setVisible(false);
    ui->Dhpc->setVisible(true);
    //tercer semestre
    ui->CalculoIntegral->setVisible(false);
    ui->FisicaII->setVisible(false);
    ui->MatematicasDiscretas->setVisible(false);
    ui->ProgramacionII->setVisible(false);
    ui->Ensamblador->setVisible(false);
    ui->LenguaExtranjeraIII->setVisible(false);
    //cuarto semestre
    ui->EcuacionesDiferenciales->setVisible(false);
    ui->CircuitosElectricos->setVisible(false);
    ui->Graficacion->setVisible(false);
    ui->EstructurasDeDatos->setVisible(false);
    ui->LenguaExtranjeraIV->setVisible(false);
    //quinto semestre
    ui->ProbabilidadyEstadistica->setVisible(false);
    ui->CircuitosElectronicos->setVisible(false);
    ui->SistemasOperativosI->setVisible(false);
    ui->AnalisisyDiseoDeAlgoritmos->setVisible(false);
    ui->IngenieriaDeSoftware->setVisible(false);
    //sexto semestre
    ui->ModeloDeRedes->setVisible(false);
    ui->DisenoDigital->setVisible(false);
    ui->BaseDeDatosParaIngenieria->setVisible(false);
    ui->SistemasOperativosII->setVisible(false);
    ui->AdministracionDeProyectos->setVisible(true);
    //septimo semestre
    ui->RedesInalambricas->setVisible(false);
    ui->MineriaDeDatos->setVisible(false);
    ui->ArquitecturaDeComputo->setVisible(false);
    ui->ProgramacionConcurrenteyParalela->setVisible(false);
    ui->DesarrolloDeAplicacionesWeb->setVisible(false);
    //octavo semestre
    ui->TeoriaDeControl->setVisible(false);
    ui->AdministracionDeRedes->setVisible(false);
    ui->TecnicasDeInteligenciaArtificial->setVisible(false);
    ui->ProgramacionDistribuidaAplicada->setVisible(false);
    ui->DesarrolloDeAplicacionesMoviles->setVisible(false);
    ui->OptativaI->setVisible(true);
    //noveno semestre
    ui->IntercomunicacionySeguridadEnRedes->setVisible(false);
    ui->SistemasEmpotrados->setVisible(false);
    ui->ServicioSocial->setVisible(true);
    ui->OptativaII->setVisible(true);
    //decimo semestre
    ui->OptativaDesit->setVisible(true);
    ui->PracticaProfesional->setVisible(true);
    ui->ProyectoIDI->setVisible(false);
}

ventanaAlumnoICC::~ventanaAlumnoICC()
{
    delete ui;
}


void ventanaAlumnoICC::on_buttRegresar_clicked()
{

}

//Primer semestre
void ventanaAlumnoICC::on_Matematicas_clicked(bool checked)
{
    ui->CalculoDiferencial->setVisible(checked);
    ui->FisicaI->setVisible(checked);

    if(!checked){
        ui->CalculoDiferencial->setChecked(checked);
        ui->FisicaI->setChecked(checked);

        on_CalculoDiferencial_clicked(checked);
        on_FisicaI_clicked(checked);
    }
}

void ventanaAlumnoICC::on_AlgebraSuperior_clicked(bool checked)
{
    ui->AlgebraLinealConElementosDeGeometriaAnalitica->setVisible(checked);
    if(!checked){
        ui->AlgebraLinealConElementosDeGeometriaAnalitica->setChecked(checked);
        on_AlgebraLinealConElementosDeGeometriaAnalitica_clicked(checked);
    }
}

void ventanaAlumnoICC::on_MetodologiaDeLaProgramacion_clicked(bool checked)
{
    ui->ProgramacionI->setVisible(checked);
    if(!checked){
        ui->ProgramacionI->setChecked(checked);
        on_ProgramacionI_clicked(checked);
    }
}

void ventanaAlumnoICC::on_LenguaExtranjeraI_clicked(bool checked)
{
    ui->LenguaExtranjeraII->setVisible(checked);
    if(!checked){
        ui->LenguaExtranjeraII->setChecked(checked);
        on_LenguaExtranjeraII_clicked(checked);
    }
}

void ventanaAlumnoICC::on_FormacionHumanaySocial_clicked(bool checked)
{
    //No tiene materia consecuente.
}

//Segundo Semestre

void ventanaAlumnoICC::on_CalculoDiferencial_clicked(bool checked)
{
    ui->CalculoIntegral->setVisible(checked);
    if(!checked){
        ui->CalculoIntegral->setChecked(checked);
        on_CalculoIntegral_clicked(checked);
    }
}

void ventanaAlumnoICC::on_FisicaI_clicked(bool checked)
{
    ui->FisicaII->setVisible(checked);
    if(!checked){
        ui->FisicaII->setChecked(checked);
        on_FisicaII_clicked(checked);
    }
}

void ventanaAlumnoICC::on_AlgebraLinealConElementosDeGeometriaAnalitica_clicked(bool checked)
{
    if(ui->ProgramacionII->isChecked())
        ui->Graficacion->setVisible(checked);

    ui->MatematicasDiscretas->setVisible(checked);

    if (!checked){
        ui->MatematicasDiscretas->setChecked(checked);
        ui->Graficacion->setChecked(checked);

        //on_MatematicasDiscretas_clicked(checked);
        //on_Graficacion_clicked(checked);
    }
}

void ventanaAlumnoICC::on_ProgramacionI_clicked(bool checked)
{
    ui->ProgramacionII->setVisible(checked);
    ui->Ensamblador->setVisible(checked);

    if(!checked){
        ui->ProgramacionII->setChecked(checked);
        ui->Ensamblador->setChecked(checked);

        on_ProgramacionII_clicked(checked);
        on_Ensamblador_clicked(checked);
    }
}

void ventanaAlumnoICC::on_LenguaExtranjeraII_clicked(bool checked)
{
    ui->LenguaExtranjeraIII->setVisible(checked);
    if(!checked){
        ui->LenguaExtranjeraIII->setChecked(checked);
        on_LenguaExtranjeraIII_clicked(checked);
    }
}

void ventanaAlumnoICC::on_Dhpc_clicked(bool checked)
{
    //No tiene materia consecuente.
}

//Tercer Semestre

void ventanaAlumnoICC::on_CalculoIntegral_clicked(bool checked)
{
    ui->EcuacionesDiferenciales->setVisible(checked);
    ui->ProbabilidadyEstadistica->setVisible(checked);

    if(!checked){
        ui->EcuacionesDiferenciales->setChecked(checked);
        ui->ProbabilidadyEstadistica->setChecked(checked);

        on_EcuacionesDiferenciales_clicked(checked);
        on_ProbabilidadyEstadistica_clicked(checked);
    }
}

void ventanaAlumnoICC::on_FisicaII_clicked(bool checked)
{
    ui->CircuitosElectricos->setVisible(checked);
    if(!checked){
        ui->CircuitosElectricos->setChecked(checked);
        on_CircuitosElectricos_clicked(checked);
    }
}

void ventanaAlumnoICC::on_MatematicasDiscretas_clicked(bool checked)
{
    //No tiene materia consecuente.
}

void ventanaAlumnoICC::on_ProgramacionII_clicked(bool checked)
{
    if(ui->AlgebraLinealConElementosDeGeometriaAnalitica->isChecked())
        ui->Graficacion->setVisible(checked);

    ui->EstructurasDeDatos->setVisible(checked);
    ui->IngenieriaDeSoftware->setVisible(checked);

    if(!checked){

        ui->Graficacion->setChecked(checked);
        ui->EstructurasDeDatos->setChecked(checked);
        ui->IngenieriaDeSoftware->setChecked(checked);

        on_EstructurasDeDatos_clicked(checked);
    }
}

void ventanaAlumnoICC::on_Ensamblador_clicked(bool checked)
{
    if(ui->EstructurasDeDatos->isChecked())
        ui->SistemasOperativosI->setVisible(checked);
    if(!checked){
        ui->SistemasOperativosI->setChecked(checked);
        on_SistemasOperativosI_clicked(checked);
    }
}

void ventanaAlumnoICC::on_LenguaExtranjeraIII_clicked(bool checked)
{
    ui->LenguaExtranjeraIV->setVisible(checked);
    if(!checked)
        ui->LenguaExtranjeraIV->setChecked(checked);
}

//Cuarto Semestre

void ventanaAlumnoICC::on_EcuacionesDiferenciales_clicked(bool checked)
{
    ui->TeoriaDeControl->setVisible(checked);
    if(!checked)
        ui->TeoriaDeControl->setChecked(checked);
}

void ventanaAlumnoICC::on_CircuitosElectricos_clicked(bool checked)
{
    ui->CircuitosElectronicos->setVisible(checked);
    if(!checked){
        ui->CircuitosElectronicos->setChecked(checked);
        on_CircuitosElectronicos_clicked(checked);
    }
}

void ventanaAlumnoICC::on_Graficacion_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_EstructurasDeDatos_clicked(bool checked)
{
    if(ui->Ensamblador->isChecked())
        ui->SistemasOperativosI->setVisible(checked);

    ui->AnalisisyDiseoDeAlgoritmos->setVisible(checked);
    ui->BaseDeDatosParaIngenieria->setVisible(checked);

    if(!checked){
        ui->AnalisisyDiseoDeAlgoritmos->setChecked(checked);
        ui->SistemasOperativosI->setChecked(checked);
        ui->BaseDeDatosParaIngenieria->setChecked(checked);

        on_AnalisisyDiseoDeAlgoritmos_clicked(checked);
        on_SistemasOperativosI_clicked(checked);
    }
}

void ventanaAlumnoICC::on_LenguaExtranjeraIV_clicked(bool checked)
{
    //No tiene materia consecuente.
}

//Quinto Semestre

void ventanaAlumnoICC::on_ProbabilidadyEstadistica_clicked(bool checked)
{
    ui->ModeloDeRedes->setVisible(checked);
    ui->MineriaDeDatos->setVisible(checked);
    if(!checked){
        ui->ModeloDeRedes->setChecked(checked);
        ui->MineriaDeDatos->setChecked(checked);
        on_ModeloDeRedes_clicked(checked);
    }
}

void ventanaAlumnoICC::on_CircuitosElectronicos_clicked(bool checked)
{
    ui->DisenoDigital->setVisible(checked);
    if(!checked){
        ui->DisenoDigital->setChecked(checked);
        on_DisenoDigital_clicked(checked);
    }
}

void ventanaAlumnoICC::on_SistemasOperativosI_clicked(bool checked)
{
    ui->SistemasOperativosII->setVisible(checked);
    if(!checked)
        ui->SistemasOperativosII->setChecked(checked);
}

void ventanaAlumnoICC::on_AnalisisyDiseoDeAlgoritmos_clicked(bool checked)
{
    ui->ProgramacionConcurrenteyParalela->setVisible(checked);
    ui->TecnicasDeInteligenciaArtificial->setVisible(checked);
    if(!checked){
        ui->ProgramacionConcurrenteyParalela->setChecked(checked);
        ui->TecnicasDeInteligenciaArtificial->setChecked(checked);
        on_ProgramacionConcurrenteyParalela_clicked(checked);
    }
}

void ventanaAlumnoICC::on_IngenieriaDeSoftware_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

//Sexto Semestre

void ventanaAlumnoICC::on_ModeloDeRedes_clicked(bool checked)
{    
    ui->RedesInalambricas->setVisible(checked);
    if(!checked){
        ui->RedesInalambricas->setChecked(checked);
        on_RedesInalambricas_clicked(checked);
    }
}

void ventanaAlumnoICC::on_DisenoDigital_clicked(bool checked)
{
    ui->ArquitecturaDeComputo->setVisible(checked);
    if (!checked){
        ui->ArquitecturaDeComputo->setChecked(checked);
        on_ArquitecturaDeComputo_clicked(checked);
    }
}

void ventanaAlumnoICC::on_BaseDeDatosParaIngenieria_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_SistemasOperativosII_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_AdministracionDeProyectos_clicked(bool checked)
{
    if(ui->OptativaII->isChecked())
        ui->ProyectoIDI->setVisible(checked);

    ui->DesarrolloDeAplicacionesWeb->setVisible(checked);
    if(!checked){
        ui->DesarrolloDeAplicacionesWeb->setChecked(checked);
        ui->ProyectoIDI->setChecked(checked);
        on_DesarrolloDeAplicacionesWeb_clicked(checked);
    }

}

/// ------------- Séptimo Semestre.

void ventanaAlumnoICC::on_RedesInalambricas_clicked(bool checked)
{
    ui->AdministracionDeRedes->setVisible(checked);
    if (!checked){
        ui->AdministracionDeRedes->setChecked(checked);
        on_AdministracionDeRedes_clicked(checked);
    }
}

void ventanaAlumnoICC::on_MineriaDeDatos_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_ArquitecturaDeComputo_clicked(bool checked)
{
    ui->SistemasEmpotrados->setVisible(checked);
    if(!checked)
        ui->SistemasEmpotrados->setChecked(false);
}

void ventanaAlumnoICC::on_ProgramacionConcurrenteyParalela_clicked(bool checked)
{
    ui->ProgramacionDistribuidaAplicada->setVisible(checked);
    if(!checked)
        ui->ProgramacionDistribuidaAplicada->setChecked(checked);
}

void ventanaAlumnoICC::on_DesarrolloDeAplicacionesWeb_clicked(bool checked)
{
    ui->DesarrolloDeAplicacionesMoviles->setVisible(checked);
    if(!checked)
        ui->DesarrolloDeAplicacionesMoviles->setChecked(checked);
}


/// ------------- Octavo Semestre.


void ventanaAlumnoICC::on_TeoriaDeControl_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_AdministracionDeRedes_clicked(bool checked)
{
    ui->IntercomunicacionySeguridadEnRedes->setVisible(checked);
    if (!checked)
        ui->IntercomunicacionySeguridadEnRedes->setChecked(checked);
}

void ventanaAlumnoICC::on_TecnicasDeInteligenciaArtificial_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_ProgramacionDistribuidaAplicada_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_DesarrolloDeAplicacionesMoviles_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_OptativaI_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

/// -------------  Noveno Semestre.

void ventanaAlumnoICC::on_IntercomunicacionySeguridadEnRedes_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_SistemasEmpotrados_clicked(bool checked)
{
    //No tiene materias consecuentes.
}


void ventanaAlumnoICC::on_ServicioSocial_clicked(bool checked)
{
    //No tiene materias consecuentes.
}

void ventanaAlumnoICC::on_OptativaII_clicked(bool checked)
{
    if(ui->AdministracionDeProyectos->isChecked())
        ui->ProyectoIDI->setVisible(checked);
    if (!checked)
        ui->ProyectoIDI->setChecked(checked);
}


/// -------------  Décimo Semestre.


void ventanaAlumnoICC::on_OptativaDesit_clicked(bool checked)
{
    //No tiene materias consecuentes.
}


void ventanaAlumnoICC::on_PracticaProfesional_clicked(bool checked)
{
    //No tiene materias consecuentes.
}


void ventanaAlumnoICC::on_ProyectoIDI_clicked(bool checked)
{
    //No tiene materias consecuentes.
}


