#include "ventanacoordinador.h"
#include "ui_ventanacoordinador.h"

ventanaCoordinador::ventanaCoordinador(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ventanaCoordinador)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose,true);

    ///Tablas - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    //Tabla Tutores
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery * query = new QSqlQuery();
    query->prepare("SELECT idTutor AS ID, nombre AS 'Nombre del Tutor' FROM Tutor WHERE verificado = 0");
    query->exec();
    modal->setQuery(*query);
    ui->tablaProfesores->setModel(modal);
    ui->tablaProfesores->resizeColumnsToContents();

    //Tabla Grupos
    QSqlQueryModel * modalG = new QSqlQueryModel();
    QSqlQuery * queryG = new QSqlQuery();
    queryG->prepare("SELECT nombre AS 'Nombre de la Sección', agno as 'Generación' FROM Seccion WHERE idTutor is NULL");
    queryG->exec();
    modalG->setQuery(*queryG);
    ui->tablaGrupos->setModel(modalG);
    ui->tablaGrupos->resizeColumnsToContents();

    ///Autocompletado - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    //Profesores

    QSqlQuery queryIDTutores;
    QString idTutorString = "";
    QStringList CompletionList;

    queryIDTutores.prepare("SELECT idTutor FROM Tutor");
    if(queryIDTutores.exec( )){
        while(queryIDTutores.next()){
            idTutorString = queryIDTutores.value(0).toString();
            CompletionList << idTutorString;
        }
    }

    StringCompleter = new QCompleter(CompletionList, this);
    StringCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    ui->lineIDProfesor->setCompleter(StringCompleter);

    //Grupos

    QSqlQuery queryNombreSeccion;
    QString nameSeccionString = "";
    QStringList CompletionListGrupo;

    queryNombreSeccion.prepare("SELECT nombre FROM Seccion");
    if(queryNombreSeccion.exec( )){
        while(queryNombreSeccion.next()){
            nameSeccionString = queryNombreSeccion.value(0).toString();
            CompletionListGrupo << nameSeccionString;
        }
    }

    StringCompleterGrupo = new QCompleter(CompletionListGrupo, this);
    StringCompleterGrupo->setCaseSensitivity(Qt::CaseInsensitive);
    ui->lineIDGrupo->setCompleter(StringCompleterGrupo);
}

ventanaCoordinador::~ventanaCoordinador()
{
    delete ui;
}

void ventanaCoordinador::on_buttBuscarGrupo_clicked()
{
    //Busca Grupos
    QSqlQueryModel * modalG = new QSqlQueryModel();
    QSqlQuery * queryG = new QSqlQuery(), queryAux;
    QString nombSeccion = ui->lineIDGrupo->text();

    queryAux.prepare("SELECT idTutor FROM Seccion WHERE nombre = '"+ nombSeccion +"'");
    queryAux.exec();
    while(queryAux.next()){

        if( (queryAux.value(0).toString().isEmpty() )) {

            queryG->prepare("SELECT nombre AS 'Nombre de la Sección', agno as 'Generación' FROM Seccion WHERE nombre = '"+nombSeccion+"' ");
            queryG->exec();
            modalG->setQuery(*queryG);
            ui->tablaGrupos->setModel(modalG);
            ui->tablaGrupos->resizeColumnsToContents();
        }
        else
        {
            QMessageBox::warning(this, "ERROR", "El grupo ya tiene un tutor asignado!");
            ui->lineIDGrupo->clear();

            //Tabla Grupos
            QSqlQueryModel * modalG = new QSqlQueryModel();
            QSqlQuery * queryG = new QSqlQuery();
            queryG->prepare("SELECT nombre AS 'Nombre de la Sección', agno as 'Generación' FROM Seccion WHERE idTutor is NULL");
            queryG->exec();
            modalG->setQuery(*queryG);
            ui->tablaGrupos->setModel(modalG);
            ui->tablaGrupos->resizeColumnsToContents();
        }
    }
}


void ventanaCoordinador::on_buttBuscar2_clicked()
{
    //Busca Tutores
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery * query = new QSqlQuery(), queryAux;
    QString idTutor = ui->lineIDProfesor->text();    

    queryAux.prepare("SELECT verificado FROM Tutor WHERE idTutor = '"+idTutor +"' ");
    queryAux.exec();
    while(queryAux.next()){

        if( (queryAux.value(0).toInt()) == 0){

            query->prepare("SELECT idTutor AS ID, nombre AS 'Nombre del Tutor' FROM Tutor WHERE idTutor = '"+idTutor +"' ");
            query->exec();
            modal->setQuery(*query);
            ui->tablaProfesores->setModel(modal);
            ui->tablaProfesores->resizeColumnsToContents();
        }
        else
        {
            QMessageBox::warning(this, "ERROR", "El tutor ya tiene un grupo asignado!");
            ui->lineIDProfesor->clear();

            //Tabla Tutores
            QSqlQueryModel * modal = new QSqlQueryModel();
            QSqlQuery * query = new QSqlQuery();
            query->prepare("SELECT idTutor AS ID, nombre AS 'Nombre del Tutor' FROM Tutor WHERE verificado = 0");
            query->exec();
            modal->setQuery(*query);
            ui->tablaProfesores->setModel(modal);
            ui->tablaProfesores->resizeColumnsToContents();

        }
    }
}

void ventanaCoordinador::on_lineIDGrupo_textEdited(const QString &arg1)
{
    //Tabla Grupos
    QSqlQueryModel * modalG = new QSqlQueryModel();
    QSqlQuery * queryG = new QSqlQuery();
    queryG->prepare("SELECT nombre AS 'Nombre de la Sección', agno as 'Generación' FROM Seccion WHERE idTutor is NULL");
    queryG->exec();
    modalG->setQuery(*queryG);
    ui->tablaGrupos->setModel(modalG);
    ui->tablaGrupos->resizeColumnsToContents();
}

void ventanaCoordinador::on_lineIDProfesor_textEdited(const QString &arg1)
{
    //Tabla Tutores
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery * query = new QSqlQuery();
    query->prepare("SELECT idTutor AS ID, nombre AS 'Nombre del Tutor' FROM Tutor WHERE verificado = 0");
    query->exec();
    modal->setQuery(*query);
    ui->tablaProfesores->setModel(modal);
    ui->tablaProfesores->resizeColumnsToContents();

}

void ventanaCoordinador::on_pushButton_clicked()
{
    /*
    MainWindow * ventPrincipal = new MainWindow();
    ventPrincipal->show();
    ventPrincipal->setVisible(true);

    close();*/

}

void ventanaCoordinador::on_buttAsignar_clicked()
{
    //Normal
    QMessageBox msgBox;
    msgBox.setText("Asignar sección a tutor.");
    ///Cambiar
    msgBox.setInformativeText("¿Desea guardar los cambios?");
    /// -----
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    //Normal

    ///Agregar
    msgBox.setButtonText(QMessageBox::Save,"Guardar");
    msgBox.setButtonText(QMessageBox::Discard,"Descartar");
    msgBox.setButtonText(QMessageBox::Cancel,"Cancelar");
    ///Agregar
    int ret = msgBox.exec();



    if (ret == QMessageBox::Save){

        QSqlQuery insercionDatos;
        QString idTutorString = ui->lineIDProfesor->text(), nombreSeccion = ui->lineIDGrupo->text();

        if(idTutorString.length())
        insercionDatos.prepare("UPDATE Tutor SET verificado = 1 WHERE idTutor = '"+idTutorString +"' ");
        insercionDatos.exec();

        insercionDatos.prepare("UPDATE Seccion SET idTutor = '"+idTutorString +"' WHERE nombre = '"+nombreSeccion +"' ");
        insercionDatos.exec();


        QMessageBox msgBox;
        msgBox.setText("¡El tutor ha sido asignado!.");
        msgBox.exec();


        //Tabla Tutores
        QSqlQueryModel * modal = new QSqlQueryModel();
        QSqlQuery * query = new QSqlQuery();
        query->prepare("SELECT idTutor AS ID, nombre AS 'Nombre del Tutor' FROM Tutor WHERE verificado = 0");
        query->exec();
        modal->setQuery(*query);
        ui->tablaProfesores->setModel(modal);
        ui->tablaProfesores->resizeColumnsToContents();

        //Tabla Grupos
        QSqlQueryModel * modalG = new QSqlQueryModel();
        QSqlQuery * queryG = new QSqlQuery();
        queryG->prepare("SELECT nombre AS 'Nombre de la Sección', agno as 'Generación' FROM Seccion WHERE idTutor is NULL");
        queryG->exec();
        modalG->setQuery(*queryG);
        ui->tablaGrupos->setModel(modalG);
        ui->tablaGrupos->resizeColumnsToContents();


        ui->lineIDGrupo->clear();
        ui->lineIDProfesor->clear();
    }



}
