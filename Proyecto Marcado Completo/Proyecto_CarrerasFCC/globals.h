#ifndef GLOBALS_H
#define GLOBALS_H

#include <QtGlobal>
#include "mainwindow.h"
#include <QtSql/QSql>
#include <QtSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlTableModel>
#include <QDebug>
#include <QMessageBox>
#include "ventanaalumnoicc.h"
#include "ventanaalumnolcc.h"
#include "ventanaprofesor.h"
#include "ventanacoordinador.h"
#include "ventanaadmin.h"
#include "ventanamodificacontrasenaalm.h"
#include "ventanacoordinador.h"

//Predicciones (Autocompletado)
#include <QCompleter>
#include <QDirModel>
QT_BEGIN_NAMESPACE

QT_END_NAMESPACE

extern bool conectada;
extern QSqlDatabase sDataBase;

#endif // GLOBALS_H
