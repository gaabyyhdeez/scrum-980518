#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "globals.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_butIngresar_clicked();
    void modificaContrasenia();

    void on_rBut_Coordinador_clicked();

    void on_rBut_Admin_clicked();

    void on_rBut_Profesor_clicked();

    void on_rBut_Alumno_clicked();

    void on_Salir_clicked();

private:
    Ui::MainWindow *ui;
    int matricula=0;
    int idTutor=0;
};

#endif // MAINWINDOW_H
