#ifndef VENTANAADMIN_H
#define VENTANAADMIN_H

#include <QDialog>
#include "globals.h"

namespace Ui {
class ventanaAdmin;
}

class ventanaAdmin : public QDialog
{
    Q_OBJECT

public:
    explicit ventanaAdmin(QWidget *parent = 0);
    ~ventanaAdmin();

private slots:
    void on_butRegresar_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_lineNuevaContra_textEdited(const QString &arg1);

private:
    Ui::ventanaAdmin *ui;
    //Autocompletado
    QCompleter * StringCompleter;

};

#endif // VENTANAADMIN_H
