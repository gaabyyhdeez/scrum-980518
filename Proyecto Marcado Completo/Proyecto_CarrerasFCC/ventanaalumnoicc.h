#ifndef VENTANAALUMNOICC_H
#define VENTANAALUMNOICC_H

#include <QDialog>
#include "globals.h"

namespace Ui {
class ventanaAlumnoICC;
}

class ventanaAlumnoICC : public QDialog
{
    Q_OBJECT

public:
    explicit ventanaAlumnoICC(QWidget *parent = 0, int matricula=0);
    ~ventanaAlumnoICC();

private slots:


    void on_buttRegresar_clicked();

    void on_Matematicas_clicked(bool checked);

    void on_CalculoDiferencial_clicked(bool checked);

    void on_FisicaI_clicked(bool checked);

    void on_CalculoIntegral_clicked(bool checked);

    void on_AlgebraSuperior_clicked(bool checked);

    void on_MetodologiaDeLaProgramacion_clicked(bool checked);

    void on_LenguaExtranjeraI_clicked(bool checked);

    void on_AlgebraLinealConElementosDeGeometriaAnalitica_clicked(bool checked);

    void on_ProgramacionI_clicked(bool checked);

    void on_LenguaExtranjeraII_clicked(bool checked);

    void on_FisicaII_clicked(bool checked);

    void on_MatematicasDiscretas_clicked(bool checked);

    void on_ProgramacionII_clicked(bool checked);

    void on_Ensamblador_clicked(bool checked);

    void on_LenguaExtranjeraIII_clicked(bool checked);

    void on_EcuacionesDiferenciales_clicked(bool checked);

    void on_CircuitosElectricos_clicked(bool checked);

    void on_Graficacion_clicked(bool checked);

    void on_EstructurasDeDatos_clicked(bool checked);

    void on_LenguaExtranjeraIV_clicked(bool checked);

    void on_ProbabilidadyEstadistica_clicked(bool checked);

    void on_CircuitosElectronicos_clicked(bool checked);

    void on_SistemasOperativosI_clicked(bool checked);

    void on_AnalisisyDiseoDeAlgoritmos_clicked(bool checked);

    void on_FormacionHumanaySocial_clicked(bool checked);

    void on_IngenieriaDeSoftware_clicked(bool checked);

    void on_Dhpc_clicked(bool checked);

    //Sexto semestre

    void on_ModeloDeRedes_clicked(bool checked);

    void on_DisenoDigital_clicked(bool checked);

    void on_BaseDeDatosParaIngenieria_clicked(bool checked);

    void on_SistemasOperativosII_clicked(bool checked);

    void on_AdministracionDeProyectos_clicked(bool checked);

    //Séptimo semestre

    void on_RedesInalambricas_clicked(bool checked);

    void on_MineriaDeDatos_clicked(bool checked);

    void on_ArquitecturaDeComputo_clicked(bool checked);

    void on_ProgramacionConcurrenteyParalela_clicked(bool checked);

    void on_DesarrolloDeAplicacionesWeb_clicked(bool checked);

    //Octavo semestre

    void on_TeoriaDeControl_clicked(bool checked);

    void on_AdministracionDeRedes_clicked(bool checked);

    void on_TecnicasDeInteligenciaArtificial_clicked(bool checked);

    void on_ProgramacionDistribuidaAplicada_clicked(bool checked);

    void on_DesarrolloDeAplicacionesMoviles_clicked(bool checked);

    //Noveno semestre

    void on_OptativaI_clicked(bool checked);

    void on_IntercomunicacionySeguridadEnRedes_clicked(bool checked);

    void on_SistemasEmpotrados_clicked(bool checked);

    void on_ServicioSocial_clicked(bool checked);

    void on_OptativaII_clicked(bool checked);

    //Décimo semestre

    void on_OptativaDesit_clicked(bool checked);

    void on_PracticaProfesional_clicked(bool checked);

    void on_ProyectoIDI_clicked(bool checked);




private:
    Ui::ventanaAlumnoICC *ui;
    int matricula2=0;
};

#endif // VENTANAALUMNOICC_H
