#include "ventanamodificacontrasenaalm.h"
#include "ui_ventanamodificacontrasenaalm.h"

ventanaModificaContrasenaAlm::ventanaModificaContrasenaAlm(QWidget *parent, int matricula) :
    QDialog(parent),
    ui(new Ui::ventanaModificaContrasenaAlm)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose,true);
    matricula2 = matricula;

    qDebug() << matricula;
}

ventanaModificaContrasenaAlm::~ventanaModificaContrasenaAlm()
{
    delete ui;
}

void ventanaModificaContrasenaAlm::on_buttGuardar_clicked()
{
    //Ya no
    /*QMessageBox::StandardButton repl;
          repl = QMessageBox::question(this, "Cambiar", "¿Confirmar Nueva Contraseña?",
                                        QMessageBox::Yes|QMessageBox::No);*/
    //No

    //Remplazar
    QMessageBox mBoxConfirmar(QMessageBox::Question,"Cambiar","¿Desea confirmar la nueva contraseña?",QMessageBox::Yes | QMessageBox::No);

    mBoxConfirmar.setButtonText(QMessageBox::Yes, "Sí");
    mBoxConfirmar.setButtonText(QMessageBox::No, "No");
    //Remplezar

    //En este primer if cambiar nombre variable
          if (mBoxConfirmar.exec() == QMessageBox::Yes)
          {

              if((!ui->lineNuevaContra->text().isEmpty()) && (!ui->lineNuevaNueva->text().isEmpty()))
              {
                  QSqlQuery modificaContra;
                  if((ui->lineNuevaContra->text()) == QString::number(matricula2))
                  {
                      QMessageBox::about(this,"Aviso","No puedes tener tu contraseña como matricula");
                  }else{
                      if((ui->lineNuevaContra->text()) == ui->lineNuevaNueva->text())
                      {
                          modificaContra.prepare("UPDATE Alumno SET contrasegna = '" + ui->lineNuevaNueva->text() + "', verificado=1 WHERE matricula="+QString::number(matricula2)+"");

                          if(!modificaContra.exec())
                          {
                              qDebug() << "Error" << endl;
                          }

                          QMessageBox::information(this,"Aviso","Contraseña modificada");

                          this->close();

                      }else{
                          QMessageBox::warning(this,"ERROR", "Los campos no coinciden");
                      }
                  }


              }else{
                   QMessageBox::warning(this,"Datos incompletos","Faltan campos por llenar");
              }
              QMessageBox::about(this, "Aceptado","Contraseña Actualizada Exitosamente.");
                this->close();
           }

          else

          {
              QMessageBox::about(this, "Negado","Contraseña no Actualizada.");
              this->close();
          }

}
