#include "ventanaalumnolcc.h"
#include "ui_ventanaalumnolcc.h"

ventanaAlumnoLCC::ventanaAlumnoLCC(QWidget *parent, int matricula) :
    QDialog(parent),
    ui(new Ui::ventanaAlumnoLCC)
{
    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose,true);

    matricula2 = matricula;
    qDebug() << matricula2;

    QSqlQuery datosAlumno;
    datosAlumno.prepare("SELECT matricula, nombre FROM Alumno WHERE matricula="+QString::number(matricula2)+"");
    datosAlumno.exec();
    datosAlumno.next();

    qDebug() << datosAlumno.result();
    ui->label_NombreMatri->setText(datosAlumno.value(0).toString());
    ui->label_NombreAlm->setText(datosAlumno.value(1).toString());
}

ventanaAlumnoLCC::~ventanaAlumnoLCC()
{
    delete ui;
}

